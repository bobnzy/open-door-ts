// index.ts
// 获取应用实例
const app = getApp<IAppOption>();

import Api, { HttpResponseToPage } from '../../services/api';

Page({
  data: {
  },
  // 事件处理函数
  // 请出申请权限表单缓存
  clearForm() {
    wx.removeStorageSync('_placeForm')
  },
  onLoad() {
    // tabBar 右上角显示红点
    wx.showTabBarRedDot({
      index: 1
    })
    this.testApi();
  },
  onShow() {
    this.clearForm()
  },
  getUserInfo(e: any) {
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true,
    })
  },

  testApi() {
    Api.post('/getData', { status: 200, i: 323 }).then((res: HttpResponseToPage) => {
      console.log(res.items);
    }).catch(function (reason) {
      console.log('失败：', reason);
    });
  }
})
