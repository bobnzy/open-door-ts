"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../../utils/navigate");
Page({
    data: {
        isLogin: false,
        hadRight: false,
        rightMsg: [
            '亲，有两种途径获得开门权限',
            '1、点击“申请开门权限”在线发起申请',
            '2、联系物业管理处开通权限'
        ],
        beforeLogin: "登录后可一键开门",
        comAddr: "常用开门：3栋1单元1楼",
        afterLogin: "开通开门权限后可一键开门"
    },
    open: function () {
        wx.showModal({
            title: '提示',
            content: '登录后可开门，是否登录？',
            success: function (res) {
                if (res.confirm) {
                    navigate_1.default.goToPanel('/pages/apply/apply-place/apply-place');
                }
                else if (res.cancel) {
                    console.log('用户点击取消');
                }
            }
        });
    },
    clickLoginBtn: function () {
        this.setData({
            isLogin: true
        });
    },
    clickRightBtn: function () {
        this.setData({
            hadRight: true
        });
        navigate_1.default.goToPanel('/pages/apply/apply-place/apply-place');
    },
    onLoad: function (_options) {
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3Blbi1kb29yLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3Blbi1kb29yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsdURBQWtEO0FBRWxELElBQUksQ0FBQztJQUtILElBQUksRUFBRTtRQUNKLE9BQU8sRUFBRSxLQUFLO1FBQ2QsUUFBUSxFQUFFLEtBQUs7UUFDZixRQUFRLEVBQUU7WUFDUixlQUFlO1lBQ2Ysb0JBQW9CO1lBQ3BCLGVBQWU7U0FDaEI7UUFFRCxXQUFXLEVBQUUsVUFBVTtRQUN2QixPQUFPLEVBQUUsY0FBYztRQUN2QixVQUFVLEVBQUUsY0FBYztLQUMzQjtJQUVELElBQUk7UUFDRixFQUFFLENBQUMsU0FBUyxDQUFDO1lBQ1gsS0FBSyxFQUFFLElBQUk7WUFDWCxPQUFPLEVBQUUsY0FBYztZQUN2QixPQUFPLFlBQUMsR0FBRztnQkFDVCxJQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQUU7b0JBQ2Ysa0JBQVEsQ0FBQyxTQUFTLENBQ2hCLHNDQUFzQyxDQUN2QyxDQUFBO2lCQUNGO3FCQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sRUFBRTtvQkFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtpQkFDdEI7WUFDSCxDQUFDO1NBQ0YsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUdELGFBQWEsRUFBYjtRQUNFLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDWCxPQUFPLEVBQUUsSUFBSTtTQUNkLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFDRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNYLFFBQVEsRUFBRSxJQUFJO1NBQ2YsQ0FBQyxDQUFBO1FBQ0Ysa0JBQVEsQ0FBQyxTQUFTLENBQ2hCLHNDQUFzQyxDQUN2QyxDQUFBO0lBQ0gsQ0FBQztJQUlELE1BQU0sRUFBRSxVQUFVLFFBQVE7SUFFMUIsQ0FBQztJQUtELE9BQU8sRUFBRTtJQUVULENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELFFBQVEsRUFBRTtJQUVWLENBQUM7SUFLRCxpQkFBaUIsRUFBRTtJQUVuQixDQUFDO0lBS0QsYUFBYSxFQUFFO0lBRWYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyDpppbpobXlvIDpl6jnu4Tku7ZcclxuXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSc7XHJcblxyXG5QYWdlKHtcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i55qE5Yid5aeL5pWw5o2uXHJcbiAgICovXHJcbiAgZGF0YToge1xyXG4gICAgaXNMb2dpbjogZmFsc2UsICAgICAgIC8v5piv5ZCm55m75b2V54q25oCBXHJcbiAgICBoYWRSaWdodDogZmFsc2UsICAgICAvL+aYr+WQpuW8gOmAmuW8gOmXqOadg+mZkFxyXG4gICAgcmlnaHRNc2c6IFtcclxuICAgICAgJ+S6su+8jOacieS4pOenjemAlOW+hOiOt+W+l+W8gOmXqOadg+mZkCcsXHJcbiAgICAgICcx44CB54K55Ye74oCc55Sz6K+35byA6Zeo5p2D6ZmQ4oCd5Zyo57q/5Y+R6LW355Sz6K+3JyxcclxuICAgICAgJzLjgIHogZTns7vniankuJrnrqHnkIblpITlvIDpgJrmnYPpmZAnXHJcbiAgICBdLCAgICAgICAgLy/nlLPor7flvIDpl6jmnYPpmZDkv6Hmga/mj5DnpLpcclxuICAgIC8vIHJpZ2h0TXNnOmDkurLvvIzmnInkuKTnp43pgJTlvoTojrflvpflvIDpl6jmnYPpmZBcXG4x44CB54K55Ye74oCc55Sz6K+35byA6Zeo5p2D6ZmQ4oCd5Zyo57q/5Y+R6LW355Sz6K+3XFxuMuOAgeiBlOezu+eJqeS4mueuoeeQhuWkhOW8gOmAmuadg+mZkCjmraXpqqTor7TmmI4pYCxcclxuICAgIGJlZm9yZUxvZ2luOiBcIueZu+W9leWQjuWPr+S4gOmUruW8gOmXqFwiLFxyXG4gICAgY29tQWRkcjogXCLluLjnlKjlvIDpl6jvvJoz5qCLMeWNleWFgzHmpbxcIixcclxuICAgIGFmdGVyTG9naW46IFwi5byA6YCa5byA6Zeo5p2D6ZmQ5ZCO5Y+v5LiA6ZSu5byA6ZeoXCJcclxuICB9LFxyXG4gIC8vIOeCueWHu+W8gOmXqOaMiemSrui/m+WFpeeUs+ivt+adg+mZkOeahOmhtemdolxyXG4gIG9wZW4oKSB7XHJcbiAgICB3eC5zaG93TW9kYWwoe1xyXG4gICAgICB0aXRsZTogJ+aPkOekuicsXHJcbiAgICAgIGNvbnRlbnQ6ICfnmbvlvZXlkI7lj6/lvIDpl6jvvIzmmK/lkKbnmbvlvZXvvJ8nLFxyXG4gICAgICBzdWNjZXNzKHJlcykge1xyXG4gICAgICAgIGlmIChyZXMuY29uZmlybSkge1xyXG4gICAgICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsKFxyXG4gICAgICAgICAgICAnL3BhZ2VzL2FwcGx5L2FwcGx5LXBsYWNlL2FwcGx5LXBsYWNlJ1xyXG4gICAgICAgICAgKVxyXG4gICAgICAgIH0gZWxzZSBpZiAocmVzLmNhbmNlbCkge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coJ+eUqOaIt+eCueWHu+WPlua2iCcpXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG4gIC8vIOeZu+W9leaMiemSri3nu4Tku7bliIfmjaJcclxuICBjbGlja0xvZ2luQnRuKCk6IHZvaWQge1xyXG4gICAgdGhpcy5zZXREYXRhKHtcclxuICAgICAgaXNMb2dpbjogdHJ1ZVxyXG4gICAgfSlcclxuICB9LFxyXG4gIGNsaWNrUmlnaHRCdG4oKSB7XHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICBoYWRSaWdodDogdHJ1ZVxyXG4gICAgfSlcclxuICAgIG5hdmlnYXRlLmdvVG9QYW5lbChcclxuICAgICAgJy9wYWdlcy9hcHBseS9hcHBseS1wbGFjZS9hcHBseS1wbGFjZSdcclxuICAgIClcclxuICB9LFxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yqg6L29XHJcbiAgICovXHJcbiAgb25Mb2FkOiBmdW5jdGlvbiAoX29wdGlvbnMpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLliJ3mrKHmuLLmn5PlrozmiJBcclxuICAgKi9cclxuICBvblJlYWR5OiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5pi+56S6XHJcbiAgICovXHJcbiAgb25TaG93OiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i6ZqQ6JePXHJcbiAgICovXHJcbiAgb25IaWRlOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Y246L29XHJcbiAgICovXHJcbiAgb25VbmxvYWQ6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i55u45YWz5LqL5Lu25aSE55CG5Ye95pWwLS3nm5HlkKznlKjmiLfkuIvmi4nliqjkvZxcclxuICAgKi9cclxuICBvblB1bGxEb3duUmVmcmVzaDogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLkuIrmi4nop6blupXkuovku7bnmoTlpITnkIblh73mlbBcclxuICAgKi9cclxuICBvblJlYWNoQm90dG9tOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUqOaIt+eCueWHu+WPs+S4iuinkuWIhuS6q1xyXG4gICAqL1xyXG4gIG9uU2hhcmVBcHBNZXNzYWdlOiBmdW5jdGlvbiAoKTogYW55IHtcclxuXHJcbiAgfVxyXG59KSJdfQ==