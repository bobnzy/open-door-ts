// 首页开门组件

import navigate from '../../../../utils/navigate';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin: false,       //是否登录状态
    hadRight: false,     //是否开通开门权限
    rightMsg: [
      '亲，有两种途径获得开门权限',
      '1、点击“申请开门权限”在线发起申请',
      '2、联系物业管理处开通权限'
    ],        //申请开门权限信息提示
    // rightMsg:`亲，有两种途径获得开门权限\n1、点击“申请开门权限”在线发起申请\n2、联系物业管理处开通权限(步骤说明)`,
    beforeLogin: "登录后可一键开门",
    comAddr: "常用开门：3栋1单元1楼",
    afterLogin: "开通开门权限后可一键开门"
  },
  // 点击开门按钮进入申请权限的页面
  open() {
    wx.showModal({
      title: '提示',
      content: '登录后可开门，是否登录？',
      success(res) {
        if (res.confirm) {
          navigate.goToPanel(
            '/pages/apply/apply-place/apply-place'
          )
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  // 登录按钮-组件切换
  clickLoginBtn(): void {
    this.setData({
      isLogin: true
    })
  },
  clickRightBtn() {
    this.setData({
      hadRight: true
    })
    navigate.goToPanel(
      '/pages/apply/apply-place/apply-place'
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})