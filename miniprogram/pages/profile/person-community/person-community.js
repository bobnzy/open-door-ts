"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Component({
    properties: {},
    data: {},
    methods: {
        goToAuthority: function () {
            navigate_1.default.goToPanel('/pages/apply/apply-place/apply-place');
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLWNvbW11bml0eS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBlcnNvbi1jb21tdW5pdHkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSxvREFBOEM7QUFDOUMsU0FBUyxDQUFDO0lBSVIsVUFBVSxFQUFFLEVBRVg7SUFLRCxJQUFJLEVBQUUsRUFFTDtJQUtELE9BQU8sRUFBRTtRQUVQLGFBQWE7WUFDWCxrQkFBUSxDQUFDLFNBQVMsQ0FBQyxzQ0FBc0MsQ0FBQyxDQUFBO1FBQzVELENBQUM7S0FDRjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDlip/og73mnI3liqEt5oiR55qE5bCP5Yy66aG16Z2iXHJcbiAqL1xyXG5pbXBvcnQgbmF2aWdhdGUgZnJvbSAnLi4vLi4vLi4vdXRpbHMvbmF2aWdhdGUnXHJcbkNvbXBvbmVudCh7XHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5bGe5oCn5YiX6KGoXHJcbiAgICovXHJcbiAgcHJvcGVydGllczoge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOaWueazleWIl+ihqFxyXG4gICAqL1xyXG4gIG1ldGhvZHM6IHtcclxuICAgIC8vIOi3s+i9rOWIsOeUs+ivt+aOiOadg1xyXG4gICAgZ29Ub0F1dGhvcml0eSgpe1xyXG4gICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoJy9wYWdlcy9hcHBseS9hcHBseS1wbGFjZS9hcHBseS1wbGFjZScpXHJcbiAgICB9XHJcbiAgfVxyXG59KVxyXG4iXX0=