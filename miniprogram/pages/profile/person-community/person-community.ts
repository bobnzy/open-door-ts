/**
 * 功能服务-我的小区页面
 */
import navigate from '../../../utils/navigate'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转到申请授权
    goToAuthority(){
      navigate.goToPanel('/pages/apply/apply-place/apply-place')
    }
  }
})
