/**
 * 个人页面的首页
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 菜单数据
    menuList: [
      {
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIgSg.png",
        title: "我的订单",
        url: ""
      },
      {
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzI6fS.png",
        title: "店长中心",
        url: ""
      },
      {
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIyY8.png",
        title: "优惠卷",
        url: ""
      },
      {
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIsFf.png",
        title: "消息通知",
        url: ""
      }
    ],
    // 功能服务数据
    funItems: [
      {
        id: 0,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dz5xJS.png",
        title: "我的小区",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 1,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIFZq.png",
        title: "申请记录",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 2,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzI9Mj.png",
        title: "物业缴费",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 3,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzISzQ.png",
        title: "邀请家人",
        url: "",
        badgeType: 'string',
        value: 'qqq'
      }, {
        id: 4,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dz5zRg.png",
        title: "访客授权",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 5,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dz5XIf.png",
        title: "门卡管理",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 6,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIAoV.png",
        title: "联系物业",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 7,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dz5vi8.png",
        title: "绑定车牌",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 8,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIPLn.png",
        title: "开门教程",
        url: "",
        badgeType: null,
        value: ''
      }, {
        id: 9,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzICss.png",
        title: "用户反馈",
        url: "",
        badgeType: null,
        value: ''
      },
      {
        id: 10,
        iconUrl: "https://s1.ax1x.com/2020/09/02/dzIkd0.png",
        title: "亲邻有福利",
        url: "",
        badgeType: 'number',
        value: 99
      }
    ]
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage(opts): WechatMiniprogram.Page.ICustomShareContent {
    console.log(opts.target)
    return {}
  }
})