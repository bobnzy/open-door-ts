"use strict";
Page({
    data: {
        menuList: [
            {
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIgSg.png",
                title: "我的订单",
                url: ""
            },
            {
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzI6fS.png",
                title: "店长中心",
                url: ""
            },
            {
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIyY8.png",
                title: "优惠卷",
                url: ""
            },
            {
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIsFf.png",
                title: "消息通知",
                url: ""
            }
        ],
        funItems: [
            {
                id: 0,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dz5xJS.png",
                title: "我的小区",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 1,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIFZq.png",
                title: "申请记录",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 2,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzI9Mj.png",
                title: "物业缴费",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 3,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzISzQ.png",
                title: "邀请家人",
                url: "",
                badgeType: 'string',
                value: 'qqq'
            }, {
                id: 4,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dz5zRg.png",
                title: "访客授权",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 5,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dz5XIf.png",
                title: "门卡管理",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 6,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIAoV.png",
                title: "联系物业",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 7,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dz5vi8.png",
                title: "绑定车牌",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 8,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIPLn.png",
                title: "开门教程",
                url: "",
                badgeType: null,
                value: ''
            }, {
                id: 9,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzICss.png",
                title: "用户反馈",
                url: "",
                badgeType: null,
                value: ''
            },
            {
                id: 10,
                iconUrl: "https://s1.ax1x.com/2020/09/02/dzIkd0.png",
                title: "亲邻有福利",
                url: "",
                badgeType: 'number',
                value: 99
            }
        ]
    },
    onLoad: function () {
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function (opts) {
        console.log(opts.target);
        return {};
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJpbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBR0EsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBRUosUUFBUSxFQUFFO1lBQ1I7Z0JBQ0UsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7YUFDUjtZQUNEO2dCQUNFLE9BQU8sRUFBRSwyQ0FBMkM7Z0JBQ3BELEtBQUssRUFBRSxNQUFNO2dCQUNiLEdBQUcsRUFBRSxFQUFFO2FBQ1I7WUFDRDtnQkFDRSxPQUFPLEVBQUUsMkNBQTJDO2dCQUNwRCxLQUFLLEVBQUUsS0FBSztnQkFDWixHQUFHLEVBQUUsRUFBRTthQUNSO1lBQ0Q7Z0JBQ0UsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7YUFDUjtTQUNGO1FBRUQsUUFBUSxFQUFFO1lBQ1I7Z0JBQ0UsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsS0FBSyxFQUFFLEVBQUU7YUFDVixFQUFFO2dCQUNELEVBQUUsRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSwyQ0FBMkM7Z0JBQ3BELEtBQUssRUFBRSxNQUFNO2dCQUNiLEdBQUcsRUFBRSxFQUFFO2dCQUNQLFNBQVMsRUFBRSxJQUFJO2dCQUNmLEtBQUssRUFBRSxFQUFFO2FBQ1YsRUFBRTtnQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsMkNBQTJDO2dCQUNwRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixHQUFHLEVBQUUsRUFBRTtnQkFDUCxTQUFTLEVBQUUsSUFBSTtnQkFDZixLQUFLLEVBQUUsRUFBRTthQUNWLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsU0FBUyxFQUFFLFFBQVE7Z0JBQ25CLEtBQUssRUFBRSxLQUFLO2FBQ2IsRUFBRTtnQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsMkNBQTJDO2dCQUNwRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixHQUFHLEVBQUUsRUFBRTtnQkFDUCxTQUFTLEVBQUUsSUFBSTtnQkFDZixLQUFLLEVBQUUsRUFBRTthQUNWLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsS0FBSyxFQUFFLEVBQUU7YUFDVixFQUFFO2dCQUNELEVBQUUsRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSwyQ0FBMkM7Z0JBQ3BELEtBQUssRUFBRSxNQUFNO2dCQUNiLEdBQUcsRUFBRSxFQUFFO2dCQUNQLFNBQVMsRUFBRSxJQUFJO2dCQUNmLEtBQUssRUFBRSxFQUFFO2FBQ1YsRUFBRTtnQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDTCxPQUFPLEVBQUUsMkNBQTJDO2dCQUNwRCxLQUFLLEVBQUUsTUFBTTtnQkFDYixHQUFHLEVBQUUsRUFBRTtnQkFDUCxTQUFTLEVBQUUsSUFBSTtnQkFDZixLQUFLLEVBQUUsRUFBRTthQUNWLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsT0FBTyxFQUFFLDJDQUEyQztnQkFDcEQsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsS0FBSyxFQUFFLEVBQUU7YUFDVixFQUFFO2dCQUNELEVBQUUsRUFBRSxDQUFDO2dCQUNMLE9BQU8sRUFBRSwyQ0FBMkM7Z0JBQ3BELEtBQUssRUFBRSxNQUFNO2dCQUNiLEdBQUcsRUFBRSxFQUFFO2dCQUNQLFNBQVMsRUFBRSxJQUFJO2dCQUNmLEtBQUssRUFBRSxFQUFFO2FBQ1Y7WUFDRDtnQkFDRSxFQUFFLEVBQUUsRUFBRTtnQkFDTixPQUFPLEVBQUUsMkNBQTJDO2dCQUNwRCxLQUFLLEVBQUUsT0FBTztnQkFDZCxHQUFHLEVBQUUsRUFBRTtnQkFDUCxTQUFTLEVBQUUsUUFBUTtnQkFDbkIsS0FBSyxFQUFFLEVBQUU7YUFDVjtTQUNGO0tBQ0Y7SUFLRCxNQUFNO0lBRU4sQ0FBQztJQUtELE9BQU87SUFFUCxDQUFDO0lBS0QsTUFBTTtJQUVOLENBQUM7SUFLRCxNQUFNO0lBRU4sQ0FBQztJQUtELFFBQVE7SUFFUixDQUFDO0lBS0QsaUJBQWlCO0lBRWpCLENBQUM7SUFLRCxhQUFhO0lBRWIsQ0FBQztJQUtELGlCQUFpQixFQUFqQixVQUFrQixJQUFJO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1FBQ3hCLE9BQU8sRUFBRSxDQUFBO0lBQ1gsQ0FBQztDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDkuKrkurrpobXpnaLnmoTpppbpobVcclxuICovXHJcblBhZ2Uoe1xyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcbiAgICAvLyDoj5zljZXmlbDmja5cclxuICAgIG1lbnVMaXN0OiBbXHJcbiAgICAgIHtcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9keklnU2cucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi5oiR55qE6K6i5Y2VXCIsXHJcbiAgICAgICAgdXJsOiBcIlwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9kekk2ZlMucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi5bqX6ZW/5Lit5b+DXCIsXHJcbiAgICAgICAgdXJsOiBcIlwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9kekl5WTgucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi5LyY5oOg5Y23XCIsXHJcbiAgICAgICAgdXJsOiBcIlwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9keklzRmYucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi5raI5oGv6YCa55+lXCIsXHJcbiAgICAgICAgdXJsOiBcIlwiXHJcbiAgICAgIH1cclxuICAgIF0sXHJcbiAgICAvLyDlip/og73mnI3liqHmlbDmja5cclxuICAgIGZ1bkl0ZW1zOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBpZDogMCxcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9kejV4SlMucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi5oiR55qE5bCP5Yy6XCIsXHJcbiAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgIGJhZGdlVHlwZTogbnVsbCxcclxuICAgICAgICB2YWx1ZTogJydcclxuICAgICAgfSwge1xyXG4gICAgICAgIGlkOiAxLFxyXG4gICAgICAgIGljb25Vcmw6IFwiaHR0cHM6Ly9zMS5heDF4LmNvbS8yMDIwLzA5LzAyL2R6SUZacS5wbmdcIixcclxuICAgICAgICB0aXRsZTogXCLnlLPor7forrDlvZVcIixcclxuICAgICAgICB1cmw6IFwiXCIsXHJcbiAgICAgICAgYmFkZ2VUeXBlOiBudWxsLFxyXG4gICAgICAgIHZhbHVlOiAnJ1xyXG4gICAgICB9LCB7XHJcbiAgICAgICAgaWQ6IDIsXHJcbiAgICAgICAgaWNvblVybDogXCJodHRwczovL3MxLmF4MXguY29tLzIwMjAvMDkvMDIvZHpJOU1qLnBuZ1wiLFxyXG4gICAgICAgIHRpdGxlOiBcIueJqeS4mue8tOi0uVwiLFxyXG4gICAgICAgIHVybDogXCJcIixcclxuICAgICAgICBiYWRnZVR5cGU6IG51bGwsXHJcbiAgICAgICAgdmFsdWU6ICcnXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBpZDogMyxcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9keklTelEucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi6YKA6K+35a625Lq6XCIsXHJcbiAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgIGJhZGdlVHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgdmFsdWU6ICdxcXEnXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBpZDogNCxcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9kejV6UmcucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi6K6/5a6i5o6I5p2DXCIsXHJcbiAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgIGJhZGdlVHlwZTogbnVsbCxcclxuICAgICAgICB2YWx1ZTogJydcclxuICAgICAgfSwge1xyXG4gICAgICAgIGlkOiA1LFxyXG4gICAgICAgIGljb25Vcmw6IFwiaHR0cHM6Ly9zMS5heDF4LmNvbS8yMDIwLzA5LzAyL2R6NVhJZi5wbmdcIixcclxuICAgICAgICB0aXRsZTogXCLpl6jljaHnrqHnkIZcIixcclxuICAgICAgICB1cmw6IFwiXCIsXHJcbiAgICAgICAgYmFkZ2VUeXBlOiBudWxsLFxyXG4gICAgICAgIHZhbHVlOiAnJ1xyXG4gICAgICB9LCB7XHJcbiAgICAgICAgaWQ6IDYsXHJcbiAgICAgICAgaWNvblVybDogXCJodHRwczovL3MxLmF4MXguY29tLzIwMjAvMDkvMDIvZHpJQW9WLnBuZ1wiLFxyXG4gICAgICAgIHRpdGxlOiBcIuiBlOezu+eJqeS4mlwiLFxyXG4gICAgICAgIHVybDogXCJcIixcclxuICAgICAgICBiYWRnZVR5cGU6IG51bGwsXHJcbiAgICAgICAgdmFsdWU6ICcnXHJcbiAgICAgIH0sIHtcclxuICAgICAgICBpZDogNyxcclxuICAgICAgICBpY29uVXJsOiBcImh0dHBzOi8vczEuYXgxeC5jb20vMjAyMC8wOS8wMi9kejV2aTgucG5nXCIsXHJcbiAgICAgICAgdGl0bGU6IFwi57uR5a6a6L2m54mMXCIsXHJcbiAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgIGJhZGdlVHlwZTogbnVsbCxcclxuICAgICAgICB2YWx1ZTogJydcclxuICAgICAgfSwge1xyXG4gICAgICAgIGlkOiA4LFxyXG4gICAgICAgIGljb25Vcmw6IFwiaHR0cHM6Ly9zMS5heDF4LmNvbS8yMDIwLzA5LzAyL2R6SVBMbi5wbmdcIixcclxuICAgICAgICB0aXRsZTogXCLlvIDpl6jmlZnnqItcIixcclxuICAgICAgICB1cmw6IFwiXCIsXHJcbiAgICAgICAgYmFkZ2VUeXBlOiBudWxsLFxyXG4gICAgICAgIHZhbHVlOiAnJ1xyXG4gICAgICB9LCB7XHJcbiAgICAgICAgaWQ6IDksXHJcbiAgICAgICAgaWNvblVybDogXCJodHRwczovL3MxLmF4MXguY29tLzIwMjAvMDkvMDIvZHpJQ3NzLnBuZ1wiLFxyXG4gICAgICAgIHRpdGxlOiBcIueUqOaIt+WPjemmiFwiLFxyXG4gICAgICAgIHVybDogXCJcIixcclxuICAgICAgICBiYWRnZVR5cGU6IG51bGwsXHJcbiAgICAgICAgdmFsdWU6ICcnXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBpZDogMTAsXHJcbiAgICAgICAgaWNvblVybDogXCJodHRwczovL3MxLmF4MXguY29tLzIwMjAvMDkvMDIvZHpJa2QwLnBuZ1wiLFxyXG4gICAgICAgIHRpdGxlOiBcIuS6sumCu+acieemj+WIqVwiLFxyXG4gICAgICAgIHVybDogXCJcIixcclxuICAgICAgICBiYWRnZVR5cGU6ICdudW1iZXInLFxyXG4gICAgICAgIHZhbHVlOiA5OVxyXG4gICAgICB9XHJcbiAgICBdXHJcbiAgfSxcclxuICBcclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWKoOi9vVxyXG4gICAqL1xyXG4gIG9uTG9hZCgpIHtcclxuICAgXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLliJ3mrKHmuLLmn5PlrozmiJBcclxuICAgKi9cclxuICBvblJlYWR5KCkge1xyXG4gICAgXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLmmL7npLpcclxuICAgKi9cclxuICBvblNob3coKSB7XHJcbiAgICBcclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZSgpIHtcclxuICAgIFxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Y246L29XHJcbiAgICovXHJcbiAgb25VbmxvYWQoKSB7XHJcbiAgICBcclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnm7jlhbPkuovku7blpITnkIblh73mlbAtLeebkeWQrOeUqOaIt+S4i+aLieWKqOS9nFxyXG4gICAqL1xyXG4gIG9uUHVsbERvd25SZWZyZXNoKCkge1xyXG4gICAgXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbSgpIHtcclxuICAgIFxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUqOaIt+eCueWHu+WPs+S4iuinkuWIhuS6q1xyXG4gICAqL1xyXG4gIG9uU2hhcmVBcHBNZXNzYWdlKG9wdHMpOiBXZWNoYXRNaW5pcHJvZ3JhbS5QYWdlLklDdXN0b21TaGFyZUNvbnRlbnQge1xyXG4gICAgY29uc29sZS5sb2cob3B0cy50YXJnZXQpXHJcbiAgICByZXR1cm4ge31cclxuICB9XHJcbn0pIl19