"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Component({
    properties: {},
    data: {},
    methods: {
        goToChoose: function () {
            navigate_1.default.goToPanel('/pages/profile/login-choose/login-choose');
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tYXV0aC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvZ2luLWF1dGgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSxvREFBOEM7QUFDOUMsU0FBUyxDQUFDO0lBSVIsVUFBVSxFQUFFLEVBRVg7SUFLRCxJQUFJLEVBQUUsRUFFTDtJQUtELE9BQU8sRUFBRTtRQUVQLFVBQVU7WUFDUixrQkFBUSxDQUFDLFNBQVMsQ0FBQywwQ0FBMEMsQ0FBQyxDQUFBO1FBQ2hFLENBQUM7S0FDRjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDnmbvlvZXmjojmnYPpobXpnaJcclxuICovXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSdcclxuQ29tcG9uZW50KHtcclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTlsZ7mgKfliJfooahcclxuICAgKi9cclxuICBwcm9wZXJ0aWVzOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5pa55rOV5YiX6KGoXHJcbiAgICovXHJcbiAgbWV0aG9kczoge1xyXG4gICAgLy8g6Lez6L2s5Yiw55m75b2V5pa55byP6YCJ5oup6aG16Z2iXHJcbiAgICBnb1RvQ2hvb3NlKCl7XHJcbiAgICAgIG5hdmlnYXRlLmdvVG9QYW5lbCgnL3BhZ2VzL3Byb2ZpbGUvbG9naW4tY2hvb3NlL2xvZ2luLWNob29zZScpXHJcbiAgICB9XHJcbiAgfVxyXG59KVxyXG4iXX0=