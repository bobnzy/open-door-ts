/**
 * 登录授权页面
 */
import navigate from '../../../utils/navigate'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转到登录方式选择页面
    goToChoose(){
      navigate.goToPanel('/pages/profile/login-choose/login-choose')
    }
  }
})
