"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appChoose = getApp();
var navigate_1 = require("../../../utils/navigate");
Component({
    properties: {},
    data: {},
    methods: {
        getuserInfo: function (e) {
            appChoose.globalData.userInfo = e.detail.userInfo;
            wx.setStorage({
                key: "isLogin",
                data: true
            });
            navigate_1.default.goToPanelDirect('/pages/profile/index/index');
        },
        goToLoginPhone: function () {
            navigate_1.default.goToPanel('/pages/profile/login-phone/login-phone');
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tY2hvb3NlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4tY2hvb3NlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EsSUFBTSxTQUFTLEdBQUcsTUFBTSxFQUFFLENBQUE7QUFDMUIsb0RBQThDO0FBQzlDLFNBQVMsQ0FBQztJQUlSLFVBQVUsRUFBRSxFQUVYO0lBS0QsSUFBSSxFQUFFLEVBRUw7SUFLRCxPQUFPLEVBQUU7UUFHUCxXQUFXLFlBQUMsQ0FBQztZQUNYLFNBQVMsQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFBO1lBRWpELEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ1osR0FBRyxFQUFFLFNBQVM7Z0JBQ2QsSUFBSSxFQUFFLElBQUk7YUFDWCxDQUFDLENBQUE7WUFFRixrQkFBUSxDQUFDLGVBQWUsQ0FBQyw0QkFBNEIsQ0FBQyxDQUFBO1FBQ3hELENBQUM7UUFHRCxjQUFjO1lBQ1osa0JBQVEsQ0FBQyxTQUFTLENBQUMsd0NBQXdDLENBQUMsQ0FBQTtRQUM5RCxDQUFDO0tBQ0Y7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICog55m75b2V5o6I5p2D6YCJ5oup6aG16Z2iXHJcbiAqL1xyXG5jb25zdCBhcHBDaG9vc2UgPSBnZXRBcHAoKVxyXG5pbXBvcnQgbmF2aWdhdGUgZnJvbSAnLi4vLi4vLi4vdXRpbHMvbmF2aWdhdGUnXHJcbkNvbXBvbmVudCh7XHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5bGe5oCn5YiX6KGoXHJcbiAgICovXHJcbiAgcHJvcGVydGllczoge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOaWueazleWIl+ihqFxyXG4gICAqL1xyXG4gIG1ldGhvZHM6IHtcclxuXHJcbiAgICAvLyDojrflj5bnlKjmiLfkv6Hmga/vvIjpl67popjvvJrngrnlh7vmi5Lnu53mjInpkq7kuZ/kvJrliJvlu7rnvJPlrZjvvJ/mnKrop6PlhrPvvIlcclxuICAgIGdldHVzZXJJbmZvKGUpIHtcclxuICAgICAgYXBwQ2hvb3NlLmdsb2JhbERhdGEudXNlckluZm8gPSBlLmRldGFpbC51c2VySW5mb1xyXG4gICAgICAvLyDnmbvlvZXlkI7lu7rnq4vnvJPlrZhcclxuICAgICAgd3guc2V0U3RvcmFnZSh7XHJcbiAgICAgICAga2V5OiBcImlzTG9naW5cIixcclxuICAgICAgICBkYXRhOiB0cnVlXHJcbiAgICAgIH0pXHJcbiAgICAgIC8vIOaIkOWKn+WQjui/lOWbnuS4u+mhtemdolxyXG4gICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWxEaXJlY3QoJy9wYWdlcy9wcm9maWxlL2luZGV4L2luZGV4JylcclxuICAgIH0sXHJcblxyXG4gICAgLy8g6Lez6L2s5Yiw5omL5py655m75b2V6aG16Z2iXHJcbiAgICBnb1RvTG9naW5QaG9uZSgpIHtcclxuICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsKCcvcGFnZXMvcHJvZmlsZS9sb2dpbi1waG9uZS9sb2dpbi1waG9uZScpXHJcbiAgICB9XHJcbiAgfVxyXG59KSJdfQ==