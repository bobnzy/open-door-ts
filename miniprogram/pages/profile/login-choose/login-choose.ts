/**
 * 登录授权选择页面
 */
const appChoose = getApp()
import navigate from '../../../utils/navigate'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

    // 获取用户信息（问题：点击拒绝按钮也会创建缓存？未解决）
    getuserInfo(e) {
      appChoose.globalData.userInfo = e.detail.userInfo
      // 登录后建立缓存
      wx.setStorage({
        key: "isLogin",
        data: true
      })
      // 成功后返回主页面
      navigate.goToPanelDirect('/pages/profile/index/index')
    },

    // 跳转到手机登录页面
    goToLoginPhone() {
      navigate.goToPanel('/pages/profile/login-phone/login-phone')
    }
  }
})