/**
 * 车牌绑定页面
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    warmTips:"请绑定真实有效的车牌，普通汽车车牌只需要输入五位，新能源车车牌请输入六位！", //温馨提示
    carDisableBind:false as boolean,  // 是否禁用绑定按钮
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_option:any) {

  },
})