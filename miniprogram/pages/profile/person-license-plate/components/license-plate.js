"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var comm_1 = require("../../../../utils/comm");
Component({
    properties: {},
    data: {
        inputBoxs: [{
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }, {
                value: '',
                isActive: false,
                hasValue: false
            }],
        isNewEnergy: false,
        provinceList: [
            "京", "沪", "浙", "苏", "粤", "鲁",
            "晋", "冀", "豫", "川", "渝", "辽",
            "吉", "黑", "皖", "鄂", "津", "贵",
            "云", "桂", "琼", "青", "新", "藏",
            "蒙", "宁", "甘", "陕", "闽", "赣",
            "湘"
        ],
        letterList: [
            "A", "B", "C", "D", "E", "F",
            "G", "H", "L", "J", "K", "L",
            "M", "N", "O", "P", "Q", "R",
            "S", "T", "U", "V", "W", "X",
            "Y", "Z"
        ],
        digitalList: [
            "1", "2", "3",
            "4", "5", "6",
            "7", "8", "9",
            "0"
        ],
        boardShow: {
            carKeyboardShow: false,
            provinceBoardShow: true,
            letterBoardShow: false,
            digitalBoardShow: false,
        },
        keyboardBtns: [{
                title: "省份"
            }, {
                title: "字母"
            }, {
                title: "数字"
            }, {
                title: "清空"
            }, {
                title: "←"
            }, {
                title: "收起"
            }]
    },
    methods: {
        carTypeChange: function (e) {
            if (e.detail.value) {
                if (this.getStorageIndex() === 6)
                    this.setStorageIndex(7);
                this.data.inputBoxs.push({ value: '', isActive: false, hasValue: false });
                this.setData({
                    inputBoxs: this.data.inputBoxs
                });
            }
            else {
                if (this.getStorageIndex() === 7)
                    this.setStorageIndex(6);
                this.data.inputBoxs.pop();
                this.setData({
                    inputBoxs: this.data.inputBoxs
                });
            }
        },
        showKeyboard: function (e) {
            var index = comm_1.default.getTargetValue(e, 'index');
            this.keyboardShowHide(index);
            this.setStorageIndex(index);
            this.data.inputBoxs[index].isActive = true;
            this.setData({
                inputBoxs: this.data.inputBoxs
            });
        },
        hideKeyboard: function () {
            this.keyboardShowHide(9);
        },
        setStorageIndex: function (index) {
            wx.setStorageSync("activeInputIndex", index);
        },
        getStorageIndex: function () {
            return wx.getStorageSync('activeInputIndex');
        },
        blankEvent: function () {
        },
        carNumInput: function (e) {
            var index = comm_1.default.getTargetValue(e, 'index');
            var val = e.detail.value;
            this.setValue(index, val);
        },
        keyboardTap: function (e) {
            var index = this.getStorageIndex();
            var value = comm_1.default.getTargetValue(e, 'value');
            this.setValue(index, value);
        },
        setValue: function (index, value) {
            var inputboxs = this.data.inputBoxs;
            inputboxs[index] = { value: value, isActive: true, hasValue: true };
            if (inputboxs[index].hasValue && inputboxs[index + 1]) {
                inputboxs[index].isActive = false;
                inputboxs[index + 1].isActive = true;
                this.setStorageIndex(index + 1);
            }
            this.setData({
                inputBoxs: inputboxs
            });
        },
        carNumInputBlur: function (e) {
            var index = comm_1.default.getTargetValue(e, 'index');
            var inputboxs = this.data.inputBoxs;
            inputboxs[index].hasValue = inputboxs[index].value ? true : false;
            inputboxs[index].isActive = false;
            this.setData({
                inputBoxs: inputboxs
            });
        },
        keyboardCommand: function (e) {
            var index = comm_1.default.getTargetValue(e, 'index');
            switch (index) {
                case 0:
                    this.changeToP();
                    break;
                case 1:
                    this.changeToL();
                    break;
                case 2:
                    this.changeToD();
                    break;
                case 3:
                    this.clearInputBox();
                    break;
                case 4:
                    this.backInputBox();
                    break;
                case 5:
                    this.keyboardShowHide(9);
                    break;
                default: this.blankEvent();
            }
        },
        changeToP: function () {
            this.data.boardShow = {
                carKeyboardShow: true,
                provinceBoardShow: true,
                letterBoardShow: false,
                digitalBoardShow: false,
            };
            this.setData({
                boardShow: this.data.boardShow
            });
        },
        changeToL: function () {
            this.data.boardShow = {
                carKeyboardShow: true,
                provinceBoardShow: false,
                letterBoardShow: true,
                digitalBoardShow: false,
            };
            this.setData({
                boardShow: this.data.boardShow
            });
        },
        changeToD: function () {
            this.data.boardShow = {
                carKeyboardShow: true,
                provinceBoardShow: false,
                letterBoardShow: false,
                digitalBoardShow: true,
            };
            this.setData({
                boardShow: this.data.boardShow
            });
        },
        clearInputBox: function () {
            var inputboxs = this.data.inputBoxs;
            inputboxs.map(function (item) {
                item.value = '';
                item.isActive = false;
                item.hasValue = false;
            });
            this.setStorageIndex(0);
            this.setData({
                inputBoxs: inputboxs
            });
        },
        backInputBox: function () {
            var index = this.getStorageIndex();
            this.data.inputBoxs[index] = { value: '', isActive: true, hasValue: false };
            this.setData({
                inputBoxs: this.data.inputBoxs
            });
        },
        keyboardShowHide: function (s) {
            if (s === void 0) { s = 0; }
            var boardshow = this.data.boardShow;
            var inputboxs = this.data.inputBoxs;
            var letters = /^[A-Z]/;
            switch (s) {
                case 0:
                    this.changeToP();
                    break;
                case 1:
                    this.changeToL();
                    if (!(typeof inputboxs[1].value === "string" && letters.test(inputboxs[1].value))) {
                        wx.showToast({
                            title: '第二位必须为字母',
                            icon: 'none',
                            duration: 2000
                        });
                    }
                    break;
                case 9:
                    boardshow = {
                        carKeyboardShow: false,
                        provinceBoardShow: false,
                        letterBoardShow: false,
                        digitalBoardShow: false,
                    };
                    this.setData({
                        boardShow: boardshow
                    });
                    break;
                default:
                    if (!boardshow.carKeyboardShow) {
                        this.changeToL();
                    }
            }
        },
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGljZW5zZS1wbGF0ZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxpY2Vuc2UtcGxhdGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSwrQ0FBeUM7QUFtQnpDLFNBQVMsQ0FBQztJQUlSLFVBQVUsRUFBRSxFQUVYO0lBS0QsSUFBSSxFQUFFO1FBRUosU0FBUyxFQUFFLENBQUM7Z0JBQ1YsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsUUFBUSxFQUFFLEtBQUs7YUFDaEIsRUFBRTtnQkFDRCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxRQUFRLEVBQUUsS0FBSztnQkFDZixRQUFRLEVBQUUsS0FBSzthQUNoQixFQUFFO2dCQUNELEtBQUssRUFBRSxFQUFFO2dCQUNULFFBQVEsRUFBRSxLQUFLO2dCQUNmLFFBQVEsRUFBRSxLQUFLO2FBQ2hCLEVBQUU7Z0JBQ0QsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsUUFBUSxFQUFFLEtBQUs7YUFDaEIsRUFBRTtnQkFDRCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxRQUFRLEVBQUUsS0FBSztnQkFDZixRQUFRLEVBQUUsS0FBSzthQUNoQixFQUFFO2dCQUNELEtBQUssRUFBRSxFQUFFO2dCQUNULFFBQVEsRUFBRSxLQUFLO2dCQUNmLFFBQVEsRUFBRSxLQUFLO2FBQ2hCLEVBQUU7Z0JBQ0QsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsUUFBUSxFQUFFLEtBQUs7YUFDaEIsQ0FBeUI7UUFHMUIsV0FBVyxFQUFFLEtBQWdCO1FBRTdCLFlBQVksRUFBRTtZQUNaLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRztZQUM1QixHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDNUIsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHO1lBQzVCLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRztZQUM1QixHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDNUIsR0FBRztTQUNKO1FBQ0QsVUFBVSxFQUFFO1lBQ1YsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHO1lBQzVCLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRztZQUM1QixHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDNUIsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHO1lBQzVCLEdBQUcsRUFBRSxHQUFHO1NBQ1Q7UUFDRCxXQUFXLEVBQUU7WUFDWCxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDYixHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDYixHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUc7WUFDYixHQUFHO1NBQ0o7UUFHRCxTQUFTLEVBQUU7WUFDVCxlQUFlLEVBQUUsS0FBSztZQUN0QixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLGVBQWUsRUFBRSxLQUFLO1lBQ3RCLGdCQUFnQixFQUFFLEtBQUs7U0FDUDtRQUdsQixZQUFZLEVBQUUsQ0FBQztnQkFDYixLQUFLLEVBQUUsSUFBSTthQUNaLEVBQUU7Z0JBQ0QsS0FBSyxFQUFFLElBQUk7YUFDWixFQUFFO2dCQUNELEtBQUssRUFBRSxJQUFJO2FBQ1osRUFBRTtnQkFDRCxLQUFLLEVBQUUsSUFBSTthQUNaLEVBQUU7Z0JBQ0QsS0FBSyxFQUFFLEdBQUc7YUFDWCxFQUFFO2dCQUNELEtBQUssRUFBRSxJQUFJO2FBQ1osQ0FBdUI7S0FDekI7SUFLRCxPQUFPLEVBQUU7UUFFUCxhQUFhLEVBQUUsVUFBVSxDQUFNO1lBRTdCLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUU7Z0JBQ2xCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUM7b0JBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDekQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO2dCQUMxRSxJQUFJLENBQUMsT0FBUSxDQUFDO29CQUNaLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7aUJBQy9CLENBQUMsQ0FBQTthQUNIO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUM7b0JBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDekQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxPQUFRLENBQUM7b0JBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUztpQkFDL0IsQ0FBQyxDQUFBO2FBQ0g7UUFDSCxDQUFDO1FBR0QsWUFBWSxFQUFFLFVBQVUsQ0FBTTtZQUM1QixJQUFNLEtBQUssR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQTtZQUM3QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQTtZQUMzQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO1lBQzFDLElBQUksQ0FBQyxPQUFRLENBQUM7Z0JBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUzthQUMvQixDQUFDLENBQUE7UUFDSixDQUFDO1FBR0QsWUFBWSxFQUFFO1lBQ1osSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFBO1FBQzFCLENBQUM7UUFHRCxlQUFlLFlBQUMsS0FBYTtZQUMzQixFQUFFLENBQUMsY0FBYyxDQUFDLGtCQUFrQixFQUFFLEtBQUssQ0FBQyxDQUFBO1FBQzlDLENBQUM7UUFFRCxlQUFlLEVBQWY7WUFDRSxPQUFPLEVBQUUsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtRQUM5QyxDQUFDO1FBR0QsVUFBVTtRQUVWLENBQUM7UUFHRCxXQUFXLEVBQUUsVUFBVSxDQUFNO1lBQzNCLElBQU0sS0FBSyxHQUFHLGNBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1lBQzdDLElBQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFBO1lBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFBO1FBQzNCLENBQUM7UUFHRCxXQUFXLEVBQUUsVUFBVSxDQUFNO1lBQzNCLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQTtZQUNwQyxJQUFNLEtBQUssR0FBUSxjQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQTtZQUNsRCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQTtRQUc3QixDQUFDO1FBR0QsUUFBUSxZQUFDLEtBQWEsRUFBRSxLQUFzQjtZQUM1QyxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQTtZQUNuQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFBO1lBQ25FLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsSUFBSSxTQUFTLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNyRCxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtnQkFDakMsU0FBUyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO2dCQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQTthQUNoQztZQUNELElBQUksQ0FBQyxPQUFRLENBQUM7Z0JBQ1osU0FBUyxFQUFFLFNBQVM7YUFDckIsQ0FBQyxDQUFBO1FBQ0osQ0FBQztRQUdELGVBQWUsRUFBRSxVQUFVLENBQUM7WUFDMUIsSUFBTSxLQUFLLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7WUFDN0MsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUE7WUFFbkMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtZQUNqRSxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQTtZQUNqQyxJQUFJLENBQUMsT0FBUSxDQUFDO2dCQUNaLFNBQVMsRUFBRSxTQUFTO2FBQ3JCLENBQUMsQ0FBQTtRQUNKLENBQUM7UUFJRCxlQUFlLEVBQUUsVUFBVSxDQUFNO1lBQy9CLElBQU0sS0FBSyxHQUFHLGNBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFBO1lBRTdDLFFBQVEsS0FBSyxFQUFFO2dCQUNiLEtBQUssQ0FBQztvQkFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQUMsTUFBSztnQkFDL0IsS0FBSyxDQUFDO29CQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztvQkFBQyxNQUFLO2dCQUMvQixLQUFLLENBQUM7b0JBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUFDLE1BQUs7Z0JBQy9CLEtBQUssQ0FBQztvQkFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQUMsTUFBSztnQkFDbkMsS0FBSyxDQUFDO29CQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFBQyxNQUFLO2dCQUNsQyxLQUFLLENBQUM7b0JBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUFDLE1BQUs7Z0JBQ3ZDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTthQUMzQjtRQUNILENBQUM7UUFHRCxTQUFTO1lBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUc7Z0JBQ3BCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixlQUFlLEVBQUUsS0FBSztnQkFDdEIsZ0JBQWdCLEVBQUUsS0FBSzthQUN4QixDQUFBO1lBQ0QsSUFBSSxDQUFDLE9BQVEsQ0FBQztnQkFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTO2FBQy9CLENBQUMsQ0FBQTtRQUNKLENBQUM7UUFFRCxTQUFTO1lBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUc7Z0JBQ3BCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixpQkFBaUIsRUFBRSxLQUFLO2dCQUN4QixlQUFlLEVBQUUsSUFBSTtnQkFDckIsZ0JBQWdCLEVBQUUsS0FBSzthQUN4QixDQUFBO1lBQ0QsSUFBSSxDQUFDLE9BQVEsQ0FBQztnQkFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTO2FBQy9CLENBQUMsQ0FBQTtRQUNKLENBQUM7UUFFRCxTQUFTO1lBQ1AsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUc7Z0JBQ3BCLGVBQWUsRUFBRSxJQUFJO2dCQUNyQixpQkFBaUIsRUFBRSxLQUFLO2dCQUN4QixlQUFlLEVBQUUsS0FBSztnQkFDdEIsZ0JBQWdCLEVBQUUsSUFBSTthQUN2QixDQUFBO1lBQ0QsSUFBSSxDQUFDLE9BQVEsQ0FBQztnQkFDWixTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTO2FBQy9CLENBQUMsQ0FBQTtRQUNKLENBQUM7UUFHRCxhQUFhLEVBQUU7WUFDYixJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQTtZQUNuQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSTtnQkFDakIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUE7Z0JBQ2YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUE7Z0JBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFBO1lBQ3ZCLENBQUMsQ0FBQyxDQUFBO1lBQ0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN2QixJQUFJLENBQUMsT0FBUSxDQUFDO2dCQUNaLFNBQVMsRUFBRSxTQUFTO2FBQ3JCLENBQUMsQ0FBQTtRQUNKLENBQUM7UUFHRCxZQUFZLEVBQUU7WUFDWixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUE7WUFDcEMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFBO1lBQzNFLElBQUksQ0FBQyxPQUFRLENBQUM7Z0JBQ1osU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUzthQUMvQixDQUFDLENBQUE7UUFDSixDQUFDO1FBR0QsZ0JBQWdCLEVBQUUsVUFBVSxDQUFhO1lBQWIsa0JBQUEsRUFBQSxLQUFhO1lBRXZDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFBO1lBQ25DLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFBO1lBQ25DLElBQU0sT0FBTyxHQUFHLFFBQVEsQ0FBQTtZQUd4QixRQUFRLENBQUMsRUFBRTtnQkFDVCxLQUFLLENBQUM7b0JBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUFDLE1BQU07Z0JBQ2hDLEtBQUssQ0FBQztvQkFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7b0JBQ3RCLElBQUksQ0FBQyxDQUFDLE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxRQUFRLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTt3QkFDakYsRUFBRSxDQUFDLFNBQVMsQ0FBQzs0QkFDWCxLQUFLLEVBQUUsVUFBVTs0QkFDakIsSUFBSSxFQUFFLE1BQU07NEJBQ1osUUFBUSxFQUFFLElBQUk7eUJBQ2YsQ0FBQyxDQUFBO3FCQUNIO29CQUNELE1BQU07Z0JBQ1IsS0FBSyxDQUFDO29CQUVKLFNBQVMsR0FBRzt3QkFDVixlQUFlLEVBQUUsS0FBSzt3QkFDdEIsaUJBQWlCLEVBQUUsS0FBSzt3QkFDeEIsZUFBZSxFQUFFLEtBQUs7d0JBQ3RCLGdCQUFnQixFQUFFLEtBQUs7cUJBQ3hCLENBQUE7b0JBQ0QsSUFBSSxDQUFDLE9BQVEsQ0FBQzt3QkFDWixTQUFTLEVBQUUsU0FBUztxQkFDckIsQ0FBQyxDQUFBO29CQUNGLE1BQU07Z0JBQ1I7b0JBQ0UsSUFBRyxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUM7d0JBQzVCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtxQkFDakI7YUFDSjtRQUVILENBQUM7S0FFRjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDovabniYzloavlhpnmoYblj4rplK7nm5jlsIHoo4Xnu4Tku7ZcclxuICovXHJcbmltcG9ydCBjb21tIGZyb20gJy4uLy4uLy4uLy4uL3V0aWxzL2NvbW0nXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIENhck51bUlucHV0Qm94SXRlbSB7XHJcbiAgdmFsdWU6IHN0cmluZyB8IG51bWJlcjsvLyDovpPlhaXlgLxcclxuICBpc0FjdGl2ZTogYm9vbGVhbjsvLyDmmK/lkKblvZPliY3mv4DmtLtcclxuICBoYXNWYWx1ZTogYm9vbGVhbjsvLyDmmK/lkKbmnInlgLxcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBib2FyZFNob3dJdGVtIHtcclxuICBjYXJLZXlib2FyZFNob3c6IGJvb2xlYW5cclxuICBwcm92aW5jZUJvYXJkU2hvdzogYm9vbGVhblxyXG4gIGxldHRlckJvYXJkU2hvdzogYm9vbGVhblxyXG4gIGRpZ2l0YWxCb2FyZFNob3c6IGJvb2xlYW5cclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBrZXlib2FyZEJ0bnNJdGVtIHtcclxuICB0aXRsZTogc3RyaW5nIC8vIOaMiemUruWQjVxyXG59XHJcblxyXG5Db21wb25lbnQoe1xyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOWxnuaAp+WIl+ihqFxyXG4gICAqL1xyXG4gIHByb3BlcnRpZXM6IHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5Yid5aeL5pWw5o2uXHJcbiAgICovXHJcbiAgZGF0YToge1xyXG4gICAgLy8g6L6T5YWl5qGGXHJcbiAgICBpbnB1dEJveHM6IFt7XHJcbiAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgaXNBY3RpdmU6IGZhbHNlLFxyXG4gICAgICBoYXNWYWx1ZTogZmFsc2VcclxuICAgIH0sIHtcclxuICAgICAgdmFsdWU6ICcnLFxyXG4gICAgICBpc0FjdGl2ZTogZmFsc2UsXHJcbiAgICAgIGhhc1ZhbHVlOiBmYWxzZVxyXG4gICAgfSwge1xyXG4gICAgICB2YWx1ZTogJycsXHJcbiAgICAgIGlzQWN0aXZlOiBmYWxzZSxcclxuICAgICAgaGFzVmFsdWU6IGZhbHNlXHJcbiAgICB9LCB7XHJcbiAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgaXNBY3RpdmU6IGZhbHNlLFxyXG4gICAgICBoYXNWYWx1ZTogZmFsc2VcclxuICAgIH0sIHtcclxuICAgICAgdmFsdWU6ICcnLFxyXG4gICAgICBpc0FjdGl2ZTogZmFsc2UsXHJcbiAgICAgIGhhc1ZhbHVlOiBmYWxzZVxyXG4gICAgfSwge1xyXG4gICAgICB2YWx1ZTogJycsXHJcbiAgICAgIGlzQWN0aXZlOiBmYWxzZSxcclxuICAgICAgaGFzVmFsdWU6IGZhbHNlXHJcbiAgICB9LCB7XHJcbiAgICAgIHZhbHVlOiAnJyxcclxuICAgICAgaXNBY3RpdmU6IGZhbHNlLFxyXG4gICAgICBoYXNWYWx1ZTogZmFsc2VcclxuICAgIH1dIGFzIENhck51bUlucHV0Qm94SXRlbVtdLFxyXG5cclxuICAgIC8vIOaYr+WQpuS4uuaWsOiDvea6kOi9plxyXG4gICAgaXNOZXdFbmVyZ3k6IGZhbHNlIGFzIGJvb2xlYW4sXHJcblxyXG4gICAgcHJvdmluY2VMaXN0OiBbXHJcbiAgICAgIFwi5LqsXCIsIFwi5rKqXCIsIFwi5rWZXCIsIFwi6IuPXCIsIFwi57KkXCIsIFwi6bKBXCIsXHJcbiAgICAgIFwi5pmLXCIsIFwi5YaAXCIsIFwi6LGrXCIsIFwi5bedXCIsIFwi5ridXCIsIFwi6L69XCIsXHJcbiAgICAgIFwi5ZCJXCIsIFwi6buRXCIsIFwi55qWXCIsIFwi6YSCXCIsIFwi5rSlXCIsIFwi6LS1XCIsXHJcbiAgICAgIFwi5LqRXCIsIFwi5qGCXCIsIFwi55C8XCIsIFwi6Z2SXCIsIFwi5pawXCIsIFwi6JePXCIsXHJcbiAgICAgIFwi6JKZXCIsIFwi5a6BXCIsIFwi55SYXCIsIFwi6ZmVXCIsIFwi6Ze9XCIsIFwi6LWjXCIsXHJcbiAgICAgIFwi5rmYXCJcclxuICAgIF0sXHJcbiAgICBsZXR0ZXJMaXN0OiBbXHJcbiAgICAgIFwiQVwiLCBcIkJcIiwgXCJDXCIsIFwiRFwiLCBcIkVcIiwgXCJGXCIsXHJcbiAgICAgIFwiR1wiLCBcIkhcIiwgXCJMXCIsIFwiSlwiLCBcIktcIiwgXCJMXCIsXHJcbiAgICAgIFwiTVwiLCBcIk5cIiwgXCJPXCIsIFwiUFwiLCBcIlFcIiwgXCJSXCIsXHJcbiAgICAgIFwiU1wiLCBcIlRcIiwgXCJVXCIsIFwiVlwiLCBcIldcIiwgXCJYXCIsXHJcbiAgICAgIFwiWVwiLCBcIlpcIlxyXG4gICAgXSxcclxuICAgIGRpZ2l0YWxMaXN0OiBbXHJcbiAgICAgIFwiMVwiLCBcIjJcIiwgXCIzXCIsXHJcbiAgICAgIFwiNFwiLCBcIjVcIiwgXCI2XCIsXHJcbiAgICAgIFwiN1wiLCBcIjhcIiwgXCI5XCIsXHJcbiAgICAgIFwiMFwiXHJcbiAgICBdLFxyXG5cclxuICAgIC8vIOmUruebmOaYvuekulxyXG4gICAgYm9hcmRTaG93OiB7XHJcbiAgICAgIGNhcktleWJvYXJkU2hvdzogZmFsc2UsICAvLyDplK7nm5hCb3hcclxuICAgICAgcHJvdmluY2VCb2FyZFNob3c6IHRydWUsIC8vIOaYvuekuuecgeS7vemUruebmFxyXG4gICAgICBsZXR0ZXJCb2FyZFNob3c6IGZhbHNlLC8vIOaYvuekuuWtl+avjemUruebmFxyXG4gICAgICBkaWdpdGFsQm9hcmRTaG93OiBmYWxzZSwgLy8g5pi+56S65pWw5a2X6ZSu55uYXHJcbiAgICB9IGFzIGJvYXJkU2hvd0l0ZW0sXHJcblxyXG4gICAgLy8g6ZSu55uY5Yqf6IO95oyJ6ZSu57uEXHJcbiAgICBrZXlib2FyZEJ0bnM6IFt7XHJcbiAgICAgIHRpdGxlOiBcIuecgeS7vVwiXHJcbiAgICB9LCB7XHJcbiAgICAgIHRpdGxlOiBcIuWtl+avjVwiXHJcbiAgICB9LCB7XHJcbiAgICAgIHRpdGxlOiBcIuaVsOWtl1wiXHJcbiAgICB9LCB7XHJcbiAgICAgIHRpdGxlOiBcIua4heepulwiXHJcbiAgICB9LCB7XHJcbiAgICAgIHRpdGxlOiBcIuKGkFwiXHJcbiAgICB9LCB7XHJcbiAgICAgIHRpdGxlOiBcIuaUtui1t1wiXHJcbiAgICB9XSBhcyBrZXlib2FyZEJ0bnNJdGVtW11cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTmlrnms5XliJfooahcclxuICAgKi9cclxuICBtZXRob2RzOiB7XHJcbiAgICAvLyDmlrDog73mupDovaZzd2l0Y2jlvIDlhbNcclxuICAgIGNhclR5cGVDaGFuZ2U6IGZ1bmN0aW9uIChlOiBhbnkpIHtcclxuICAgICAgLy8g5aaC5p6c5Li65paw6IO95rqQ6L2m5re75Yqg5LiA5Liq6L6T5YWl5qGG77yM5ZCm5YiZ5Y675o6J5LiA5LiqICBcclxuICAgICAgaWYgKGUuZGV0YWlsLnZhbHVlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZ2V0U3RvcmFnZUluZGV4KCkgPT09IDYpIHRoaXMuc2V0U3RvcmFnZUluZGV4KDcpIC8vIOW9k+WJjea0u+WKqGlucHV055qEaW5kZXjlgLzkv67mraNcclxuICAgICAgICB0aGlzLmRhdGEuaW5wdXRCb3hzLnB1c2goeyB2YWx1ZTogJycsIGlzQWN0aXZlOiBmYWxzZSwgaGFzVmFsdWU6IGZhbHNlIH0pO1xyXG4gICAgICAgIHRoaXMuc2V0RGF0YSEoe1xyXG4gICAgICAgICAgaW5wdXRCb3hzOiB0aGlzLmRhdGEuaW5wdXRCb3hzXHJcbiAgICAgICAgfSlcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAodGhpcy5nZXRTdG9yYWdlSW5kZXgoKSA9PT0gNykgdGhpcy5zZXRTdG9yYWdlSW5kZXgoNikgLy8g5b2T5YmN5rS75YqoaW5wdXTnmoRpbmRleOWAvOS/ruato1xyXG4gICAgICAgIHRoaXMuZGF0YS5pbnB1dEJveHMucG9wKCk7XHJcbiAgICAgICAgdGhpcy5zZXREYXRhISh7XHJcbiAgICAgICAgICBpbnB1dEJveHM6IHRoaXMuZGF0YS5pbnB1dEJveHNcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOeCueWHu+W8ueWHuumUruebmFxyXG4gICAgc2hvd0tleWJvYXJkOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gY29tbS5nZXRUYXJnZXRWYWx1ZShlLCAnaW5kZXgnKSAvLyDlvZPliY3mtLvliqhpbnB1dOeahGluZGV4XHJcbiAgICAgIHRoaXMua2V5Ym9hcmRTaG93SGlkZShpbmRleClcclxuICAgICAgdGhpcy5zZXRTdG9yYWdlSW5kZXgoaW5kZXgpXHJcbiAgICAgIHRoaXMuZGF0YS5pbnB1dEJveHNbaW5kZXhdLmlzQWN0aXZlID0gdHJ1ZVxyXG4gICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICBpbnB1dEJveHM6IHRoaXMuZGF0YS5pbnB1dEJveHNcclxuICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy8g54K55Ye76ZqQ6JeP6ZSu55uYXHJcbiAgICBoaWRlS2V5Ym9hcmQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgdGhpcy5rZXlib2FyZFNob3dIaWRlKDkpXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOWwhmluZGV45a2Y5YWlc3RvcmFnZVxyXG4gICAgc2V0U3RvcmFnZUluZGV4KGluZGV4OiBudW1iZXIpIHtcclxuICAgICAgd3guc2V0U3RvcmFnZVN5bmMoXCJhY3RpdmVJbnB1dEluZGV4XCIsIGluZGV4KVxyXG4gICAgfSxcclxuICAgIC8vIOmAmui/h3N0b3JhZ2Xojrflj5blvZPliY3mtLvliqjnmoRpbnB1dOeahGluZGV45YC8XHJcbiAgICBnZXRTdG9yYWdlSW5kZXgoKTogbnVtYmVyIHtcclxuICAgICAgcmV0dXJuIHd4LmdldFN0b3JhZ2VTeW5jKCdhY3RpdmVJbnB1dEluZGV4JylcclxuICAgIH0sXHJcblxyXG4gICAgLy8g56m65LqL5Lu2XHJcbiAgICBibGFua0V2ZW50KCkge1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyhcIuepuuS6i+S7tuiiq+inpuWPkeS6hlwiKVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyDnlKjniannkIbplK7nm5jov5vooYzovpPlhaVcclxuICAgIGNhck51bUlucHV0OiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gY29tbS5nZXRUYXJnZXRWYWx1ZShlLCAnaW5kZXgnKSAvLyDlvZPliY3mtLvliqhpbnB1dOeahGluZGV4XHJcbiAgICAgIGNvbnN0IHZhbCA9IGUuZGV0YWlsLnZhbHVlICAvLyDovpPlhaXnmoTlgLxcclxuICAgICAgdGhpcy5zZXRWYWx1ZShpbmRleCwgdmFsKVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyDoh6rlrprkuYnplK7nm5jovpPlhaVcclxuICAgIGtleWJvYXJkVGFwOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5nZXRTdG9yYWdlSW5kZXgoKSAvLyDlvZPliY3mtLvliqhpbnB1dOeahGluZGV4XHJcbiAgICAgIGNvbnN0IHZhbHVlOiBhbnkgPSBjb21tLmdldFRhcmdldFZhbHVlKGUsICd2YWx1ZScpIC8vIOiHquWumuS5iemUruebmOi+k+WFpeWAvFxyXG4gICAgICB0aGlzLnNldFZhbHVlKGluZGV4LCB2YWx1ZSkgLy8g6ZSu55uY5YC86LWL5LqIaW5wdXRcclxuXHJcblxyXG4gICAgfSxcclxuXHJcbiAgICAvLyDovpPlhaXotYvlgLznu5HlrppcclxuICAgIHNldFZhbHVlKGluZGV4OiBudW1iZXIsIHZhbHVlOiBzdHJpbmcgfCBudW1iZXIpIHtcclxuICAgICAgbGV0IGlucHV0Ym94cyA9IHRoaXMuZGF0YS5pbnB1dEJveHNcclxuICAgICAgaW5wdXRib3hzW2luZGV4XSA9IHsgdmFsdWU6IHZhbHVlLCBpc0FjdGl2ZTogdHJ1ZSwgaGFzVmFsdWU6IHRydWUgfVxyXG4gICAgICBpZiAoaW5wdXRib3hzW2luZGV4XS5oYXNWYWx1ZSAmJiBpbnB1dGJveHNbaW5kZXggKyAxXSkge1xyXG4gICAgICAgIGlucHV0Ym94c1tpbmRleF0uaXNBY3RpdmUgPSBmYWxzZSAvLyDljrvpmaTlvZPliY3pobnmv4DmtLvnirbmgIFcclxuICAgICAgICBpbnB1dGJveHNbaW5kZXggKyAxXS5pc0FjdGl2ZSA9IHRydWUgLy8g5LiL5LiA6aG55r+A5rS7XHJcbiAgICAgICAgdGhpcy5zZXRTdG9yYWdlSW5kZXgoaW5kZXggKyAxKVxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuc2V0RGF0YSEoe1xyXG4gICAgICAgIGlucHV0Qm94czogaW5wdXRib3hzXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOi+k+WFpeahhuWkseeEplxyXG4gICAgY2FyTnVtSW5wdXRCbHVyOiBmdW5jdGlvbiAoZSkge1xyXG4gICAgICBjb25zdCBpbmRleCA9IGNvbW0uZ2V0VGFyZ2V0VmFsdWUoZSwgJ2luZGV4JykgLy8g6L2m54mM5Y+35bqP5YiX5Y+3XHJcbiAgICAgIGxldCBpbnB1dGJveHMgPSB0aGlzLmRhdGEuaW5wdXRCb3hzXHJcblxyXG4gICAgICBpbnB1dGJveHNbaW5kZXhdLmhhc1ZhbHVlID0gaW5wdXRib3hzW2luZGV4XS52YWx1ZSA/IHRydWUgOiBmYWxzZSAvLyDliKTmlq1oYXNWYWx1ZeeahOWAvFxyXG4gICAgICBpbnB1dGJveHNbaW5kZXhdLmlzQWN0aXZlID0gZmFsc2VcclxuICAgICAgdGhpcy5zZXREYXRhISh7XHJcbiAgICAgICAgaW5wdXRCb3hzOiBpbnB1dGJveHNcclxuICAgICAgfSlcclxuICAgIH0sXHJcblxyXG5cclxuICAgIC8vIOmUruebmOaMh+S7pOaTjeS9nFxyXG4gICAga2V5Ym9hcmRDb21tYW5kOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAgIGNvbnN0IGluZGV4ID0gY29tbS5nZXRUYXJnZXRWYWx1ZShlLCAnaW5kZXgnKVxyXG4gICAgICAvLyDmjIfku6TlpITnkIZcclxuICAgICAgc3dpdGNoIChpbmRleCkge1xyXG4gICAgICAgIGNhc2UgMDogdGhpcy5jaGFuZ2VUb1AoKTsgYnJlYWtcclxuICAgICAgICBjYXNlIDE6IHRoaXMuY2hhbmdlVG9MKCk7IGJyZWFrXHJcbiAgICAgICAgY2FzZSAyOiB0aGlzLmNoYW5nZVRvRCgpOyBicmVha1xyXG4gICAgICAgIGNhc2UgMzogdGhpcy5jbGVhcklucHV0Qm94KCk7IGJyZWFrXHJcbiAgICAgICAgY2FzZSA0OiB0aGlzLmJhY2tJbnB1dEJveCgpOyBicmVha1xyXG4gICAgICAgIGNhc2UgNTogdGhpcy5rZXlib2FyZFNob3dIaWRlKDkpOyBicmVha1xyXG4gICAgICAgIGRlZmF1bHQ6IHRoaXMuYmxhbmtFdmVudCgpXHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8g5pi+56S655yB5Lu96ZSu55uYXHJcbiAgICBjaGFuZ2VUb1AoKSB7XHJcbiAgICAgIHRoaXMuZGF0YS5ib2FyZFNob3cgPSB7XHJcbiAgICAgICAgY2FyS2V5Ym9hcmRTaG93OiB0cnVlLCAgLy8g6ZSu55uYQm94XHJcbiAgICAgICAgcHJvdmluY2VCb2FyZFNob3c6IHRydWUsIC8vIOecgeS7vemUruebmFxyXG4gICAgICAgIGxldHRlckJvYXJkU2hvdzogZmFsc2UsIC8vIOWtl+avjemUruebmFxyXG4gICAgICAgIGRpZ2l0YWxCb2FyZFNob3c6IGZhbHNlLCAvLyDmlbDlrZfplK7nm5hcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICBib2FyZFNob3c6IHRoaXMuZGF0YS5ib2FyZFNob3dcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICAvLyDmmL7npLrlrZfmr43plK7nm5hcclxuICAgIGNoYW5nZVRvTCgpIHtcclxuICAgICAgdGhpcy5kYXRhLmJvYXJkU2hvdyA9IHtcclxuICAgICAgICBjYXJLZXlib2FyZFNob3c6IHRydWUsXHJcbiAgICAgICAgcHJvdmluY2VCb2FyZFNob3c6IGZhbHNlLFxyXG4gICAgICAgIGxldHRlckJvYXJkU2hvdzogdHJ1ZSxcclxuICAgICAgICBkaWdpdGFsQm9hcmRTaG93OiBmYWxzZSxcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICBib2FyZFNob3c6IHRoaXMuZGF0YS5ib2FyZFNob3dcclxuICAgICAgfSlcclxuICAgIH0sXHJcbiAgICAvLyDmmL7npLrmlbDlrZfplK7nm5hcclxuICAgIGNoYW5nZVRvRCgpIHtcclxuICAgICAgdGhpcy5kYXRhLmJvYXJkU2hvdyA9IHtcclxuICAgICAgICBjYXJLZXlib2FyZFNob3c6IHRydWUsXHJcbiAgICAgICAgcHJvdmluY2VCb2FyZFNob3c6IGZhbHNlLFxyXG4gICAgICAgIGxldHRlckJvYXJkU2hvdzogZmFsc2UsXHJcbiAgICAgICAgZGlnaXRhbEJvYXJkU2hvdzogdHJ1ZSxcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICBib2FyZFNob3c6IHRoaXMuZGF0YS5ib2FyZFNob3dcclxuICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy8g5riF56m6XHJcbiAgICBjbGVhcklucHV0Qm94OiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgIGxldCBpbnB1dGJveHMgPSB0aGlzLmRhdGEuaW5wdXRCb3hzXHJcbiAgICAgIGlucHV0Ym94cy5tYXAoKGl0ZW0pID0+IHtcclxuICAgICAgICBpdGVtLnZhbHVlID0gJydcclxuICAgICAgICBpdGVtLmlzQWN0aXZlID0gZmFsc2VcclxuICAgICAgICBpdGVtLmhhc1ZhbHVlID0gZmFsc2VcclxuICAgICAgfSlcclxuICAgICAgdGhpcy5zZXRTdG9yYWdlSW5kZXgoMClcclxuICAgICAgdGhpcy5zZXREYXRhISh7XHJcbiAgICAgICAgaW5wdXRCb3hzOiBpbnB1dGJveHNcclxuICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy8g6YCA5qC8XHJcbiAgICBiYWNrSW5wdXRCb3g6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmdldFN0b3JhZ2VJbmRleCgpXHJcbiAgICAgIHRoaXMuZGF0YS5pbnB1dEJveHNbaW5kZXhdID0geyB2YWx1ZTogJycsIGlzQWN0aXZlOiB0cnVlLCBoYXNWYWx1ZTogZmFsc2UgfVxyXG4gICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICBpbnB1dEJveHM6IHRoaXMuZGF0YS5pbnB1dEJveHNcclxuICAgICAgfSlcclxuICAgIH0sXHJcblxyXG4gICAgLy/mmL7npLov6ZqQ6JeP6ZSu55uYXHJcbiAgICBrZXlib2FyZFNob3dIaWRlOiBmdW5jdGlvbiAoczogbnVtYmVyID0gMCkge1xyXG4gICAgICAvLyDovpPlhaXmoYbkurrmgKfljJborr7nva5cclxuICAgICAgbGV0IGJvYXJkc2hvdyA9IHRoaXMuZGF0YS5ib2FyZFNob3dcclxuICAgICAgbGV0IGlucHV0Ym94cyA9IHRoaXMuZGF0YS5pbnB1dEJveHNcclxuICAgICAgY29uc3QgbGV0dGVycyA9IC9eW0EtWl0vXHJcblxyXG4gICAgICAvLyDngrnlh7vnrKzkuIDkuKrovpPlhaXmoYblvLnlh7rigJznnIHku73igJ3plK7nm5hcclxuICAgICAgc3dpdGNoIChzKSB7XHJcbiAgICAgICAgY2FzZSAwOiB0aGlzLmNoYW5nZVRvUCgpOyBicmVhaztcclxuICAgICAgICBjYXNlIDE6IHRoaXMuY2hhbmdlVG9MKClcclxuICAgICAgICAgIGlmICghKHR5cGVvZiBpbnB1dGJveHNbMV0udmFsdWUgPT09IFwic3RyaW5nXCIgJiYgbGV0dGVycy50ZXN0KGlucHV0Ym94c1sxXS52YWx1ZSkpKSB7XHJcbiAgICAgICAgICAgIHd4LnNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgICAgdGl0bGU6ICfnrKzkuozkvY3lv4XpobvkuLrlrZfmr40nLFxyXG4gICAgICAgICAgICAgIGljb246ICdub25lJyxcclxuICAgICAgICAgICAgICBkdXJhdGlvbjogMjAwMFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgY2FzZSA5OlxyXG4gICAgICAgICAgLy8g6ZqQ6JeP6ZSu55uYXHJcbiAgICAgICAgICBib2FyZHNob3cgPSB7XHJcbiAgICAgICAgICAgIGNhcktleWJvYXJkU2hvdzogZmFsc2UsXHJcbiAgICAgICAgICAgIHByb3ZpbmNlQm9hcmRTaG93OiBmYWxzZSxcclxuICAgICAgICAgICAgbGV0dGVyQm9hcmRTaG93OiBmYWxzZSxcclxuICAgICAgICAgICAgZGlnaXRhbEJvYXJkU2hvdzogZmFsc2UsXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLnNldERhdGEhKHtcclxuICAgICAgICAgICAgYm9hcmRTaG93OiBib2FyZHNob3dcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgaWYoIWJvYXJkc2hvdy5jYXJLZXlib2FyZFNob3cpe1xyXG4gICAgICAgICAgICB0aGlzLmNoYW5nZVRvTCgpXHJcbiAgICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICB9LFxyXG5cclxuICB9XHJcbn0pIl19