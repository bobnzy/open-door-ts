/**
 * 车牌填写框及键盘封装组件
 */
import comm from '../../../../utils/comm'

export interface CarNumInputBoxItem {
  value: string | number;// 输入值
  isActive: boolean;// 是否当前激活
  hasValue: boolean;// 是否有值
}

export interface boardShowItem {
  carKeyboardShow: boolean
  provinceBoardShow: boolean
  letterBoardShow: boolean
  digitalBoardShow: boolean
}

export interface keyboardBtnsItem {
  title: string // 按键名
}

Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // 输入框
    inputBoxs: [{
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }, {
      value: '',
      isActive: false,
      hasValue: false
    }] as CarNumInputBoxItem[],

    // 是否为新能源车
    isNewEnergy: false as boolean,

    provinceList: [
      "京", "沪", "浙", "苏", "粤", "鲁",
      "晋", "冀", "豫", "川", "渝", "辽",
      "吉", "黑", "皖", "鄂", "津", "贵",
      "云", "桂", "琼", "青", "新", "藏",
      "蒙", "宁", "甘", "陕", "闽", "赣",
      "湘"
    ],
    letterList: [
      "A", "B", "C", "D", "E", "F",
      "G", "H", "L", "J", "K", "L",
      "M", "N", "O", "P", "Q", "R",
      "S", "T", "U", "V", "W", "X",
      "Y", "Z"
    ],
    digitalList: [
      "1", "2", "3",
      "4", "5", "6",
      "7", "8", "9",
      "0"
    ],

    // 键盘显示
    boardShow: {
      carKeyboardShow: false,  // 键盘Box
      provinceBoardShow: true, // 显示省份键盘
      letterBoardShow: false,// 显示字母键盘
      digitalBoardShow: false, // 显示数字键盘
    } as boardShowItem,

    // 键盘功能按键组
    keyboardBtns: [{
      title: "省份"
    }, {
      title: "字母"
    }, {
      title: "数字"
    }, {
      title: "清空"
    }, {
      title: "←"
    }, {
      title: "收起"
    }] as keyboardBtnsItem[]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 新能源车switch开关
    carTypeChange: function (e: any) {
      // 如果为新能源车添加一个输入框，否则去掉一个  
      if (e.detail.value) {
        if (this.getStorageIndex() === 6) this.setStorageIndex(7) // 当前活动input的index值修正
        this.data.inputBoxs.push({ value: '', isActive: false, hasValue: false });
        this.setData!({
          inputBoxs: this.data.inputBoxs
        })
      } else {
        if (this.getStorageIndex() === 7) this.setStorageIndex(6) // 当前活动input的index值修正
        this.data.inputBoxs.pop();
        this.setData!({
          inputBoxs: this.data.inputBoxs
        })
      }
    },

    // 点击弹出键盘
    showKeyboard: function (e: any) {
      const index = comm.getTargetValue(e, 'index') // 当前活动input的index
      this.keyboardShowHide(index)
      this.setStorageIndex(index)
      this.data.inputBoxs[index].isActive = true
      this.setData!({
        inputBoxs: this.data.inputBoxs
      })
    },

    // 点击隐藏键盘
    hideKeyboard: function () {
      this.keyboardShowHide(9)
    },

    // 将index存入storage
    setStorageIndex(index: number) {
      wx.setStorageSync("activeInputIndex", index)
    },
    // 通过storage获取当前活动的input的index值
    getStorageIndex(): number {
      return wx.getStorageSync('activeInputIndex')
    },

    // 空事件
    blankEvent() {
      // console.log("空事件被触发了")
    },

    // 用物理键盘进行输入
    carNumInput: function (e: any) {
      const index = comm.getTargetValue(e, 'index') // 当前活动input的index
      const val = e.detail.value  // 输入的值
      this.setValue(index, val)
    },

    // 自定义键盘输入
    keyboardTap: function (e: any) {
      const index = this.getStorageIndex() // 当前活动input的index
      const value: any = comm.getTargetValue(e, 'value') // 自定义键盘输入值
      this.setValue(index, value) // 键盘值赋予input


    },

    // 输入赋值绑定
    setValue(index: number, value: string | number) {
      let inputboxs = this.data.inputBoxs
      inputboxs[index] = { value: value, isActive: true, hasValue: true }
      if (inputboxs[index].hasValue && inputboxs[index + 1]) {
        inputboxs[index].isActive = false // 去除当前项激活状态
        inputboxs[index + 1].isActive = true // 下一项激活
        this.setStorageIndex(index + 1)
      }
      this.setData!({
        inputBoxs: inputboxs
      })
    },

    // 输入框失焦
    carNumInputBlur: function (e) {
      const index = comm.getTargetValue(e, 'index') // 车牌号序列号
      let inputboxs = this.data.inputBoxs

      inputboxs[index].hasValue = inputboxs[index].value ? true : false // 判断hasValue的值
      inputboxs[index].isActive = false
      this.setData!({
        inputBoxs: inputboxs
      })
    },


    // 键盘指令操作
    keyboardCommand: function (e: any) {
      const index = comm.getTargetValue(e, 'index')
      // 指令处理
      switch (index) {
        case 0: this.changeToP(); break
        case 1: this.changeToL(); break
        case 2: this.changeToD(); break
        case 3: this.clearInputBox(); break
        case 4: this.backInputBox(); break
        case 5: this.keyboardShowHide(9); break
        default: this.blankEvent()
      }
    },

    // 显示省份键盘
    changeToP() {
      this.data.boardShow = {
        carKeyboardShow: true,  // 键盘Box
        provinceBoardShow: true, // 省份键盘
        letterBoardShow: false, // 字母键盘
        digitalBoardShow: false, // 数字键盘
      }
      this.setData!({
        boardShow: this.data.boardShow
      })
    },
    // 显示字母键盘
    changeToL() {
      this.data.boardShow = {
        carKeyboardShow: true,
        provinceBoardShow: false,
        letterBoardShow: true,
        digitalBoardShow: false,
      }
      this.setData!({
        boardShow: this.data.boardShow
      })
    },
    // 显示数字键盘
    changeToD() {
      this.data.boardShow = {
        carKeyboardShow: true,
        provinceBoardShow: false,
        letterBoardShow: false,
        digitalBoardShow: true,
      }
      this.setData!({
        boardShow: this.data.boardShow
      })
    },

    // 清空
    clearInputBox: function () {
      let inputboxs = this.data.inputBoxs
      inputboxs.map((item) => {
        item.value = ''
        item.isActive = false
        item.hasValue = false
      })
      this.setStorageIndex(0)
      this.setData!({
        inputBoxs: inputboxs
      })
    },

    // 退格
    backInputBox: function () {
      const index = this.getStorageIndex()
      this.data.inputBoxs[index] = { value: '', isActive: true, hasValue: false }
      this.setData!({
        inputBoxs: this.data.inputBoxs
      })
    },

    //显示/隐藏键盘
    keyboardShowHide: function (s: number = 0) {
      // 输入框人性化设置
      let boardshow = this.data.boardShow
      let inputboxs = this.data.inputBoxs
      const letters = /^[A-Z]/

      // 点击第一个输入框弹出“省份”键盘
      switch (s) {
        case 0: this.changeToP(); break;
        case 1: this.changeToL()
          if (!(typeof inputboxs[1].value === "string" && letters.test(inputboxs[1].value))) {
            wx.showToast({
              title: '第二位必须为字母',
              icon: 'none',
              duration: 2000
            })
          }
          break;
        case 9:
          // 隐藏键盘
          boardshow = {
            carKeyboardShow: false,
            provinceBoardShow: false,
            letterBoardShow: false,
            digitalBoardShow: false,
          }
          this.setData!({
            boardShow: boardshow
          })
          break;
        default:
          if(!boardshow.carKeyboardShow){
            this.changeToL()
          }
      }

    },

  }
})