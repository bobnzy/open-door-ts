"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appSetting = getApp();
var navigate_1 = require("../../../utils/navigate");
Component({
    properties: {},
    data: {
        userinfo: []
    },
    methods: {
        clearUserInfo: function () {
            appSetting.globalData.userInfo = null;
            wx.removeStorage({
                key: 'isLogin'
            });
            navigate_1.default.goToPanelDirect('/pages/profile/index/index');
        }
    },
    lifetimes: {
        attached: function () {
            this.setData({
                userinfo: appSetting.globalData.userInfo
            });
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc2V0dGluZy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImxvZ2luLXNldHRpbmcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFHQSxJQUFNLFVBQVUsR0FBRyxNQUFNLEVBQUUsQ0FBQTtBQUMzQixvREFBOEM7QUFJOUMsU0FBUyxDQUFDO0lBSVIsVUFBVSxFQUFFLEVBRVg7SUFLRCxJQUFJLEVBQUU7UUFDSixRQUFRLEVBQUUsRUFBRTtLQUNiO0lBS0QsT0FBTyxFQUFFO1FBRVAsYUFBYTtZQUNYLFVBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQTtZQUVyQyxFQUFFLENBQUMsYUFBYSxDQUFDO2dCQUNmLEdBQUcsRUFBRSxTQUFTO2FBQ2YsQ0FBQyxDQUFBO1lBQ0Ysa0JBQVEsQ0FBQyxlQUFlLENBQUMsNEJBQTRCLENBQUMsQ0FBQTtRQUN4RCxDQUFDO0tBQ0Y7SUFDRCxTQUFTLEVBQUM7UUFDUixRQUFRLEVBQUU7WUFDUixJQUFJLENBQUMsT0FBUSxDQUFDO2dCQUNaLFFBQVEsRUFBRSxVQUFVLENBQUMsVUFBVSxDQUFDLFFBQVE7YUFDekMsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUNGO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIOeUqOaIt+iuvue9rumhtemdolxyXG4gKi9cclxuY29uc3QgYXBwU2V0dGluZyA9IGdldEFwcCgpXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSdcclxuLyoqXHJcbiAqIOeUqOaIt+aOqOWHulxyXG4gKi9cclxuQ29tcG9uZW50KHtcclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTlsZ7mgKfliJfooahcclxuICAgKi9cclxuICBwcm9wZXJ0aWVzOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuICAgIHVzZXJpbmZvOiBbXVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOaWueazleWIl+ihqFxyXG4gICAqL1xyXG4gIG1ldGhvZHM6IHtcclxuICAgIC8vIOa4hemZpOeUqOaIt+S/oeaBr+W5tui3s+i9rOiHs+mmlumhtVxyXG4gICAgY2xlYXJVc2VySW5mbygpe1xyXG4gICAgICBhcHBTZXR0aW5nLmdsb2JhbERhdGEudXNlckluZm8gPSBudWxsXHJcbiAgICAgIC8vIOa4hemZpOeZu+W9lee8k+WtmFxyXG4gICAgICB3eC5yZW1vdmVTdG9yYWdlKHtcclxuICAgICAgICBrZXk6ICdpc0xvZ2luJ1xyXG4gICAgICB9KVxyXG4gICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWxEaXJlY3QoJy9wYWdlcy9wcm9maWxlL2luZGV4L2luZGV4JylcclxuICAgIH1cclxuICB9LFxyXG4gIGxpZmV0aW1lczp7XHJcbiAgICBhdHRhY2hlZDogZnVuY3Rpb24oKXtcclxuICAgICAgdGhpcy5zZXREYXRhISh7XHJcbiAgICAgICAgdXNlcmluZm86IGFwcFNldHRpbmcuZ2xvYmFsRGF0YS51c2VySW5mb1xyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIH1cclxufSlcclxuIl19