/**
 * 用户设置页面
 */
const appSetting = getApp()
import navigate from '../../../utils/navigate'
/**
 * 用户推出
 */
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    userinfo: []
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 清除用户信息并跳转至首页
    clearUserInfo(){
      appSetting.globalData.userInfo = null
      // 清除登录缓存
      wx.removeStorage({
        key: 'isLogin'
      })
      navigate.goToPanelDirect('/pages/profile/index/index')
    }
  },
  lifetimes:{
    attached: function(){
      this.setData!({
        userinfo: appSetting.globalData.userInfo
      })
    }
  }
})
