/**
 * 手机登录页面
 */
import navigate from '../../../utils/navigate'
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    // 是否禁用登录按钮 默认禁用
    isCheck: true,
    // 是否显示倒计时
    isCount: true,
    // 获取用户的手机号码
    phone: '',
    // 获取用户的验证码
    code: null,
    // 倒计时
    count: 60,
    // 文字
    sendCodeText: '发送验证码'
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转回登录主页
    goToHome() {
      navigate.goToPanelDirect('/pages/profile/profile')
    },

    // 获取用户输入的手机号码(需要验证手机号的合法性)
    getPhone(e) {
      // 获取用户手机号
      this.setData({
        phone: e.detail.value
      })
    },

    // 获取验证码(需要证实验证码的合法性)
    getCode(e) {
      let code = e.detail.value
      // 验证码为6位时校验合法性
      if( code.length ===6 ){
        if(/^[0-9]\d{5}$/.test(code)){
          this.setData({
            code: code
          })
          // 调用登录按钮的方法
          this.check()
        } else {
          wx.showToast({
            title: '验证码错误',
            icon: 'none',
            duration: 1000
          })
        }
      }
    },

    // 是否发送验证码（已验证手机号码的合法性）
    sendCode() {
      // 手机号
      let phone = this.data.phone
      // 倒计时
      let count = 60
      // 是否为合法手机号
      if (!(/^1[34578]\d{9}$/.test(phone))) {
        // 非手机号
        wx.showToast({
          title: '不合法手机号',
          icon: 'none',
          duration: 1000
        })
      } else {
        this.setData({
          isCount: false
        })
        let time = setInterval(() => {
          this.setData({
            count: --count
          })
          if (count === 0) {
            this.setData({
              isCount: true,
              count: 60
            })
            clearInterval(time)
          }
        }, 1000)
      }
    },

    // 是否启用按钮
    check() {
      let isCount = this.data.isCount
      let code = this.data.code
      console.log(code)
      if (isCount === false) {
        this.setData({
          isCheck: false
        })
      }
    }
  }
})