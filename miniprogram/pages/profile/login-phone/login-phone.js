"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Component({
    properties: {},
    data: {
        isCheck: true,
        isCount: true,
        phone: '',
        code: null,
        count: 60,
        sendCodeText: '发送验证码'
    },
    methods: {
        goToHome: function () {
            navigate_1.default.goToPanelDirect('/pages/profile/profile');
        },
        getPhone: function (e) {
            this.setData({
                phone: e.detail.value
            });
        },
        getCode: function (e) {
            var code = e.detail.value;
            if (code.length === 6) {
                if (/^[0-9]\d{5}$/.test(code)) {
                    this.setData({
                        code: code
                    });
                    this.check();
                }
                else {
                    wx.showToast({
                        title: '验证码错误',
                        icon: 'none',
                        duration: 1000
                    });
                }
            }
        },
        sendCode: function () {
            var _this = this;
            var phone = this.data.phone;
            var count = 60;
            if (!(/^1[34578]\d{9}$/.test(phone))) {
                wx.showToast({
                    title: '不合法手机号',
                    icon: 'none',
                    duration: 1000
                });
            }
            else {
                this.setData({
                    isCount: false
                });
                var time_1 = setInterval(function () {
                    _this.setData({
                        count: --count
                    });
                    if (count === 0) {
                        _this.setData({
                            isCount: true,
                            count: 60
                        });
                        clearInterval(time_1);
                    }
                }, 1000);
            }
        },
        check: function () {
            var isCount = this.data.isCount;
            var code = this.data.code;
            console.log(code);
            if (isCount === false) {
                this.setData({
                    isCheck: false
                });
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tcGhvbmUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsb2dpbi1waG9uZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUdBLG9EQUE4QztBQUM5QyxTQUFTLENBQUM7SUFJUixVQUFVLEVBQUUsRUFFWDtJQUtELElBQUksRUFBRTtRQUVKLE9BQU8sRUFBRSxJQUFJO1FBRWIsT0FBTyxFQUFFLElBQUk7UUFFYixLQUFLLEVBQUUsRUFBRTtRQUVULElBQUksRUFBRSxJQUFJO1FBRVYsS0FBSyxFQUFFLEVBQUU7UUFFVCxZQUFZLEVBQUUsT0FBTztLQUN0QjtJQUtELE9BQU8sRUFBRTtRQUVQLFFBQVE7WUFDTixrQkFBUSxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO1FBQ3BELENBQUM7UUFHRCxRQUFRLFlBQUMsQ0FBQztZQUVSLElBQUksQ0FBQyxPQUFPLENBQUM7Z0JBQ1gsS0FBSyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSzthQUN0QixDQUFDLENBQUE7UUFDSixDQUFDO1FBR0QsT0FBTyxZQUFDLENBQUM7WUFDUCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQTtZQUV6QixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUksQ0FBQyxFQUFFO2dCQUNwQixJQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUM7b0JBQzNCLElBQUksQ0FBQyxPQUFPLENBQUM7d0JBQ1gsSUFBSSxFQUFFLElBQUk7cUJBQ1gsQ0FBQyxDQUFBO29CQUVGLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtpQkFDYjtxQkFBTTtvQkFDTCxFQUFFLENBQUMsU0FBUyxDQUFDO3dCQUNYLEtBQUssRUFBRSxPQUFPO3dCQUNkLElBQUksRUFBRSxNQUFNO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3FCQUNmLENBQUMsQ0FBQTtpQkFDSDthQUNGO1FBQ0gsQ0FBQztRQUdELFFBQVE7WUFBUixpQkE4QkM7WUE1QkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUE7WUFFM0IsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFBO1lBRWQsSUFBSSxDQUFDLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7Z0JBRXBDLEVBQUUsQ0FBQyxTQUFTLENBQUM7b0JBQ1gsS0FBSyxFQUFFLFFBQVE7b0JBQ2YsSUFBSSxFQUFFLE1BQU07b0JBQ1osUUFBUSxFQUFFLElBQUk7aUJBQ2YsQ0FBQyxDQUFBO2FBQ0g7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQztvQkFDWCxPQUFPLEVBQUUsS0FBSztpQkFDZixDQUFDLENBQUE7Z0JBQ0YsSUFBSSxNQUFJLEdBQUcsV0FBVyxDQUFDO29CQUNyQixLQUFJLENBQUMsT0FBTyxDQUFDO3dCQUNYLEtBQUssRUFBRSxFQUFFLEtBQUs7cUJBQ2YsQ0FBQyxDQUFBO29CQUNGLElBQUksS0FBSyxLQUFLLENBQUMsRUFBRTt3QkFDZixLQUFJLENBQUMsT0FBTyxDQUFDOzRCQUNYLE9BQU8sRUFBRSxJQUFJOzRCQUNiLEtBQUssRUFBRSxFQUFFO3lCQUNWLENBQUMsQ0FBQTt3QkFDRixhQUFhLENBQUMsTUFBSSxDQUFDLENBQUE7cUJBQ3BCO2dCQUNILENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQTthQUNUO1FBQ0gsQ0FBQztRQUdELEtBQUs7WUFDSCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQTtZQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQTtZQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQ2pCLElBQUksT0FBTyxLQUFLLEtBQUssRUFBRTtnQkFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQztvQkFDWCxPQUFPLEVBQUUsS0FBSztpQkFDZixDQUFDLENBQUE7YUFDSDtRQUNILENBQUM7S0FDRjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDmiYvmnLrnmbvlvZXpobXpnaJcclxuICovXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSdcclxuQ29tcG9uZW50KHtcclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTlsZ7mgKfliJfooahcclxuICAgKi9cclxuICBwcm9wZXJ0aWVzOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuICAgIC8vIOaYr+WQpuemgeeUqOeZu+W9leaMiemSriDpu5jorqTnpoHnlKhcclxuICAgIGlzQ2hlY2s6IHRydWUsXHJcbiAgICAvLyDmmK/lkKbmmL7npLrlgJLorqHml7ZcclxuICAgIGlzQ291bnQ6IHRydWUsXHJcbiAgICAvLyDojrflj5bnlKjmiLfnmoTmiYvmnLrlj7fnoIFcclxuICAgIHBob25lOiAnJyxcclxuICAgIC8vIOiOt+WPlueUqOaIt+eahOmqjOivgeeggVxyXG4gICAgY29kZTogbnVsbCxcclxuICAgIC8vIOWAkuiuoeaXtlxyXG4gICAgY291bnQ6IDYwLFxyXG4gICAgLy8g5paH5a2XXHJcbiAgICBzZW5kQ29kZVRleHQ6ICflj5HpgIHpqozor4HnoIEnXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog57uE5Lu255qE5pa55rOV5YiX6KGoXHJcbiAgICovXHJcbiAgbWV0aG9kczoge1xyXG4gICAgLy8g6Lez6L2s5Zue55m75b2V5Li76aG1XHJcbiAgICBnb1RvSG9tZSgpIHtcclxuICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsRGlyZWN0KCcvcGFnZXMvcHJvZmlsZS9wcm9maWxlJylcclxuICAgIH0sXHJcblxyXG4gICAgLy8g6I635Y+W55So5oi36L6T5YWl55qE5omL5py65Y+356CBKOmcgOimgemqjOivgeaJi+acuuWPt+eahOWQiOazleaApylcclxuICAgIGdldFBob25lKGUpIHtcclxuICAgICAgLy8g6I635Y+W55So5oi35omL5py65Y+3XHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgcGhvbmU6IGUuZGV0YWlsLnZhbHVlXHJcbiAgICAgIH0pXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOiOt+WPlumqjOivgeeggSjpnIDopoHor4Hlrp7pqozor4HnoIHnmoTlkIjms5XmgKcpXHJcbiAgICBnZXRDb2RlKGUpIHtcclxuICAgICAgbGV0IGNvZGUgPSBlLmRldGFpbC52YWx1ZVxyXG4gICAgICAvLyDpqozor4HnoIHkuLo25L2N5pe25qCh6aqM5ZCI5rOV5oCnXHJcbiAgICAgIGlmKCBjb2RlLmxlbmd0aCA9PT02ICl7XHJcbiAgICAgICAgaWYoL15bMC05XVxcZHs1fSQvLnRlc3QoY29kZSkpe1xyXG4gICAgICAgICAgdGhpcy5zZXREYXRhKHtcclxuICAgICAgICAgICAgY29kZTogY29kZVxyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC8vIOiwg+eUqOeZu+W9leaMiemSrueahOaWueazlVxyXG4gICAgICAgICAgdGhpcy5jaGVjaygpXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHd4LnNob3dUb2FzdCh7XHJcbiAgICAgICAgICAgIHRpdGxlOiAn6aqM6K+B56CB6ZSZ6K+vJyxcclxuICAgICAgICAgICAgaWNvbjogJ25vbmUnLFxyXG4gICAgICAgICAgICBkdXJhdGlvbjogMTAwMFxyXG4gICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0sXHJcblxyXG4gICAgLy8g5piv5ZCm5Y+R6YCB6aqM6K+B56CB77yI5bey6aqM6K+B5omL5py65Y+356CB55qE5ZCI5rOV5oCn77yJXHJcbiAgICBzZW5kQ29kZSgpIHtcclxuICAgICAgLy8g5omL5py65Y+3XHJcbiAgICAgIGxldCBwaG9uZSA9IHRoaXMuZGF0YS5waG9uZVxyXG4gICAgICAvLyDlgJLorqHml7ZcclxuICAgICAgbGV0IGNvdW50ID0gNjBcclxuICAgICAgLy8g5piv5ZCm5Li65ZCI5rOV5omL5py65Y+3XHJcbiAgICAgIGlmICghKC9eMVszNDU3OF1cXGR7OX0kLy50ZXN0KHBob25lKSkpIHtcclxuICAgICAgICAvLyDpnZ7miYvmnLrlj7dcclxuICAgICAgICB3eC5zaG93VG9hc3Qoe1xyXG4gICAgICAgICAgdGl0bGU6ICfkuI3lkIjms5XmiYvmnLrlj7cnLFxyXG4gICAgICAgICAgaWNvbjogJ25vbmUnLFxyXG4gICAgICAgICAgZHVyYXRpb246IDEwMDBcclxuICAgICAgICB9KVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgICBpc0NvdW50OiBmYWxzZVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgbGV0IHRpbWUgPSBzZXRJbnRlcnZhbCgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgICAgICBjb3VudDogLS1jb3VudFxyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIGlmIChjb3VudCA9PT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgICAgICAgIGlzQ291bnQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgY291bnQ6IDYwXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGltZSlcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LCAxMDAwKVxyXG4gICAgICB9XHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOaYr+WQpuWQr+eUqOaMiemSrlxyXG4gICAgY2hlY2soKSB7XHJcbiAgICAgIGxldCBpc0NvdW50ID0gdGhpcy5kYXRhLmlzQ291bnRcclxuICAgICAgbGV0IGNvZGUgPSB0aGlzLmRhdGEuY29kZVxyXG4gICAgICBjb25zb2xlLmxvZyhjb2RlKVxyXG4gICAgICBpZiAoaXNDb3VudCA9PT0gZmFsc2UpIHtcclxuICAgICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgICAgaXNDaGVjazogZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG59KSJdfQ==