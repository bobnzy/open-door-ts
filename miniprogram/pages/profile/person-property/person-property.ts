/**
 * 功能服务-联系物业页面
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 是否显示遮罩层 true:不显示 false:表示显示
    isShow: true
  },
  // 点击显示遮罩层
  Change() {
    this.setData({
      isShow: !this.data.isShow
    })
  },
  // 点击取消
  allChange() {
    if (this.data.isShow === false) {
      this.setData({
        isShow: !this.data.isShow
      })
    }
  },
  // 点击出现提示框
  showModel() {
    wx.showModal({
      title: '提示',
      content: '确认呼叫此号码? 13993746557',
      cancelColor: "#07C160",
      confirmColor: "#07C160",
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      },
      fail(err) {
        console.log(err)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})