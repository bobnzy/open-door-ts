"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = getApp();
var navigate_1 = require("../../../../utils/navigate");
Component({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        isLogin: true
    },
    methods: {
        goToSetting: function () {
            var url = '/pages/profile/login-setting/login-setting';
            navigate_1.default.goToPanel(url);
        },
        goToAuth: function () {
            wx.navigateTo({
                url: '/pages/profile/login-auth/login-auth'
            });
        }
    },
    lifetimes: {
        attached: function () {
            var _this = this;
            if (app.globalData.userInfo) {
                this.setData({
                    userInfo: app.globalData.userInfo,
                    hasUserInfo: true
                });
            }
            else if (this.data.canIUse) {
                app.userInfoReadyCallback = function (res) {
                    _this.setData({
                        userInfo: res.userInfo,
                        hasUserInfo: true
                    });
                };
            }
            else {
                wx.getUserInfo({
                    success: function (res) {
                        app.globalData.userInfo = res.userInfo;
                        _this.setData({
                            userInfo: res.userInfo,
                            hasUserInfo: true
                        });
                    }
                });
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4taGVhZGVyLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4taGVhZGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EsSUFBTSxHQUFHLEdBQUcsTUFBTSxFQUFFLENBQUE7QUFDcEIsdURBQWlEO0FBQ2pELFNBQVMsQ0FBQztJQUNSLElBQUksRUFBQztRQUVILFFBQVEsRUFBRSxFQUFFO1FBQ1osV0FBVyxFQUFFLEtBQUs7UUFDbEIsT0FBTyxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsOEJBQThCLENBQUM7UUFDbkQsT0FBTyxFQUFFLElBQUk7S0FDZDtJQUVELE9BQU8sRUFBRTtRQUdQLFdBQVcsRUFBRTtZQUNYLElBQUksR0FBRyxHQUFHLDRDQUE0QyxDQUFBO1lBQ3RELGtCQUFRLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFBO1FBQ3pCLENBQUM7UUFHRCxRQUFRLEVBQUU7WUFDUixFQUFFLENBQUMsVUFBVSxDQUFDO2dCQUNaLEdBQUcsRUFBRSxzQ0FBc0M7YUFDNUMsQ0FBQyxDQUFBO1FBQ0osQ0FBQztLQUNGO0lBRUQsU0FBUyxFQUFFO1FBQ1QsUUFBUSxFQUFFO1lBQUEsaUJBMkJUO1lBMUJDLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0JBQzNCLElBQUksQ0FBQyxPQUFPLENBQUM7b0JBQ1gsUUFBUSxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsUUFBUTtvQkFDakMsV0FBVyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsQ0FBQTthQUNIO2lCQUFNLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBRzVCLEdBQUcsQ0FBQyxxQkFBcUIsR0FBRyxVQUFDLEdBQVE7b0JBQ25DLEtBQUksQ0FBQyxPQUFPLENBQUM7d0JBQ1gsUUFBUSxFQUFFLEdBQUcsQ0FBQyxRQUFRO3dCQUN0QixXQUFXLEVBQUUsSUFBSTtxQkFDbEIsQ0FBQyxDQUFBO2dCQUNKLENBQUMsQ0FBQTthQUNGO2lCQUFNO2dCQUVMLEVBQUUsQ0FBQyxXQUFXLENBQUM7b0JBQ2IsT0FBTyxFQUFFLFVBQUEsR0FBRzt3QkFDVixHQUFHLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFBO3dCQUN0QyxLQUFJLENBQUMsT0FBTyxDQUFDOzRCQUNYLFFBQVEsRUFBRSxHQUFHLENBQUMsUUFBUTs0QkFDdEIsV0FBVyxFQUFFLElBQUk7eUJBQ2xCLENBQUMsQ0FBQTtvQkFDSixDQUFDO2lCQUNGLENBQUMsQ0FBQTthQUNIO1FBQ0gsQ0FBQztLQUNGO0NBQ0YsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIOatpOe7hOS7tueUqOS6juWxleekuuWktOWDj+WSjOS4quS6uuS/oeaBr1xyXG4gKi9cclxuY29uc3QgYXBwID0gZ2V0QXBwKClcclxuaW1wb3J0IG5hdmlnYXRlIGZyb20gJy4uLy4uLy4uLy4uL3V0aWxzL25hdmlnYXRlJ1xyXG5Db21wb25lbnQoe1xyXG4gIGRhdGE6e1xyXG4gICAgLy8g55So5oi35L+h5oGvXHJcbiAgICB1c2VySW5mbzoge30sXHJcbiAgICBoYXNVc2VySW5mbzogZmFsc2UsXHJcbiAgICBjYW5JVXNlOiB3eC5jYW5JVXNlKCdidXR0b24ub3Blbi10eXBlLmdldFVzZXJJbmZvJyksXHJcbiAgICBpc0xvZ2luOiB0cnVlXHJcbiAgfSxcclxuICAvLyDnu4Tku7bnmoTmlrnms5XliJfooahcclxuICBtZXRob2RzOiB7XHJcblxyXG4gICAgLy8g6Lez6L2s5Yiw6K6+572u6aG16Z2iXHJcbiAgICBnb1RvU2V0dGluZzogZnVuY3Rpb24gKCkge1xyXG4gICAgICBsZXQgdXJsID0gJy9wYWdlcy9wcm9maWxlL2xvZ2luLXNldHRpbmcvbG9naW4tc2V0dGluZydcclxuICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsKHVybClcclxuICAgIH0sXHJcblxyXG4gICAgLy8g6Lez6L2s5Yiw6YCJ5oup6aG16Z2iXHJcbiAgICBnb1RvQXV0aDogZnVuY3Rpb24oKXtcclxuICAgICAgd3gubmF2aWdhdGVUbyh7XHJcbiAgICAgICAgdXJsOiAnL3BhZ2VzL3Byb2ZpbGUvbG9naW4tYXV0aC9sb2dpbi1hdXRoJ1xyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIH0sXHJcblxyXG4gIGxpZmV0aW1lczoge1xyXG4gICAgYXR0YWNoZWQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgaWYgKGFwcC5nbG9iYWxEYXRhLnVzZXJJbmZvKSB7XHJcbiAgICAgICAgdGhpcy5zZXREYXRhKHtcclxuICAgICAgICAgIHVzZXJJbmZvOiBhcHAuZ2xvYmFsRGF0YS51c2VySW5mbyxcclxuICAgICAgICAgIGhhc1VzZXJJbmZvOiB0cnVlXHJcbiAgICAgICAgfSlcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmRhdGEuY2FuSVVzZSkge1xyXG4gICAgICAgIC8vIOeUseS6jiBnZXRVc2VySW5mbyDmmK/nvZHnu5zor7fmsYLvvIzlj6/og73kvJrlnKggUGFnZS5vbkxvYWQg5LmL5ZCO5omN6L+U5ZueXHJcbiAgICAgICAgLy8g5omA5Lul5q2k5aSE5Yqg5YWlIGNhbGxiYWNrIOS7pemYsuatoui/meenjeaDheWGtVxyXG4gICAgICAgIGFwcC51c2VySW5mb1JlYWR5Q2FsbGJhY2sgPSAocmVzOiBhbnkpID0+IHtcclxuICAgICAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgICAgIHVzZXJJbmZvOiByZXMudXNlckluZm8sXHJcbiAgICAgICAgICAgIGhhc1VzZXJJbmZvOiB0cnVlXHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyDlnKjmsqHmnIkgb3Blbi10eXBlPWdldFVzZXJJbmZvIOeJiOacrOeahOWFvOWuueWkhOeQhlxyXG4gICAgICAgIHd4LmdldFVzZXJJbmZvKHtcclxuICAgICAgICAgIHN1Y2Nlc3M6IHJlcyA9PiB7XHJcbiAgICAgICAgICAgIGFwcC5nbG9iYWxEYXRhLnVzZXJJbmZvID0gcmVzLnVzZXJJbmZvXHJcbiAgICAgICAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgICAgICAgdXNlckluZm86IHJlcy51c2VySW5mbyxcclxuICAgICAgICAgICAgICBoYXNVc2VySW5mbzogdHJ1ZVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn0pIl19