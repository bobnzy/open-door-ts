/**
 * 此组件用于显示功能服务区域
 */
import navigate from '../../../../utils/navigate'
import comm from '../../../../utils/comm'
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 接受父组件传递的数据
    funItems: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

    // 判断是否登录
    isLogin(): any {
      let isLogin = wx.getStorageSync('isLogin')
      return isLogin
    },

    // 提示框
    showModal(content: any) {
      wx.showModal({
        title: '温馨提示',
        content: content,
        confirmText: "关闭",
        showCancel: false,
        success: (res) => {
          if (res.confirm) {
            //点击确定按钮
          } else if (res.cancel) {
            //点击取消按钮
          }
        }
      })
    },

    // 跳转到各个功能页面
    goToPages(e: any): any {
      console.log(this.isLogin())
      if (this.isLogin()) {
        switch (comm.getTargetValue(e, 'id')) {
          case 0:
            navigate.goToPanel('/pages/profile/person-community/person-community')
            break;
          case 1:
            navigate.goToPanel('/pages/profile/person-record/person-record')
            break;
          case 2:
            navigate.goToPanel('/pages/profile/person-payment/person-payment')
            break;
          case 3:
            this.showModal('您没有开门权限或开门权限已过期不能邀请家人')
            break;
          case 4:
            this.showModal('您没有开门权限或您所在的小区未开启访客邀请功能\r\n不能进行访客授权操作')
            break;
          case 6:
            navigate.goToPanel('/pages/profile/person-property/person-property')
            break;
          case 7:
            navigate.goToPanel('/pages/profile/person-license-plate/person-license-plate')
            break;
          case 8:
            
            break;
          case 9:
            navigate.goToPanel('/pages/profile/person-feedback/person-feedback')
            break;
          case 10:
            // 跳转到其它小程序（需要appId）
            wx.navigateToMiniProgram({
              appId: 'wxa75efa648b60994b',
              path: 'pages/index/index?id=123',
              extraData: {
                foo: 'bar'
              },
              envVersion: 'release',
              success(res) {
                // 打开成功
                console.log(res)
              }
            })
            break;
          default:
            break;
        }
      } else {
        wx.showModal({
          title: '温馨提示',
          content: '请先登录',
          success: (res) => {
            if (res.confirm) {
              //点击确定按钮
              navigate.goToPanel('/pages/profile/login-auth/login-auth')
            } else if (res.cancel) {
              //点击取消按钮
            }
          }
        })
      }
    }

  }
})
