"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../../utils/navigate");
var comm_1 = require("../../../../utils/comm");
Component({
    properties: {
        funItems: {
            type: Array,
            value: []
        }
    },
    data: {},
    methods: {
        isLogin: function () {
            var isLogin = wx.getStorageSync('isLogin');
            return isLogin;
        },
        showModal: function (content) {
            wx.showModal({
                title: '温馨提示',
                content: content,
                confirmText: "关闭",
                showCancel: false,
                success: function (res) {
                    if (res.confirm) {
                    }
                    else if (res.cancel) {
                    }
                }
            });
        },
        goToPages: function (e) {
            console.log(this.isLogin());
            if (this.isLogin()) {
                switch (comm_1.default.getTargetValue(e, 'id')) {
                    case 0:
                        navigate_1.default.goToPanel('/pages/profile/person-community/person-community');
                        break;
                    case 1:
                        navigate_1.default.goToPanel('/pages/profile/person-record/person-record');
                        break;
                    case 2:
                        navigate_1.default.goToPanel('/pages/profile/person-payment/person-payment');
                        break;
                    case 3:
                        this.showModal('您没有开门权限或开门权限已过期不能邀请家人');
                        break;
                    case 4:
                        this.showModal('您没有开门权限或您所在的小区未开启访客邀请功能\r\n不能进行访客授权操作');
                        break;
                    case 6:
                        navigate_1.default.goToPanel('/pages/profile/person-property/person-property');
                        break;
                    case 7:
                        navigate_1.default.goToPanel('/pages/profile/person-license-plate/person-license-plate');
                        break;
                    case 8:
                        break;
                    case 9:
                        navigate_1.default.goToPanel('/pages/profile/person-feedback/person-feedback');
                        break;
                    case 10:
                        wx.navigateToMiniProgram({
                            appId: 'wxa75efa648b60994b',
                            path: 'pages/index/index?id=123',
                            extraData: {
                                foo: 'bar'
                            },
                            envVersion: 'release',
                            success: function (res) {
                                console.log(res);
                            }
                        });
                        break;
                    default:
                        break;
                }
            }
            else {
                wx.showModal({
                    title: '温馨提示',
                    content: '请先登录',
                    success: function (res) {
                        if (res.confirm) {
                            navigate_1.default.goToPanel('/pages/profile/login-auth/login-auth');
                        }
                        else if (res.cancel) {
                        }
                    }
                });
            }
        }
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tbWFpbi1tZW51LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsibG9naW4tbWFpbi1tZW51LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBR0EsdURBQWlEO0FBQ2pELCtDQUF5QztBQUN6QyxTQUFTLENBQUM7SUFJUixVQUFVLEVBQUU7UUFFVixRQUFRLEVBQUU7WUFDUixJQUFJLEVBQUUsS0FBSztZQUNYLEtBQUssRUFBRSxFQUFFO1NBQ1Y7S0FDRjtJQUtELElBQUksRUFBRSxFQUVMO0lBS0QsT0FBTyxFQUFFO1FBR1AsT0FBTyxFQUFQO1lBQ0UsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUMxQyxPQUFPLE9BQU8sQ0FBQTtRQUNoQixDQUFDO1FBR0QsU0FBUyxZQUFDLE9BQVk7WUFDcEIsRUFBRSxDQUFDLFNBQVMsQ0FBQztnQkFDWCxLQUFLLEVBQUUsTUFBTTtnQkFDYixPQUFPLEVBQUUsT0FBTztnQkFDaEIsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLFVBQVUsRUFBRSxLQUFLO2dCQUNqQixPQUFPLEVBQUUsVUFBQyxHQUFHO29CQUNYLElBQUksR0FBRyxDQUFDLE9BQU8sRUFBRTtxQkFFaEI7eUJBQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxFQUFFO3FCQUV0QjtnQkFDSCxDQUFDO2FBQ0YsQ0FBQyxDQUFBO1FBQ0osQ0FBQztRQUdELFNBQVMsRUFBVCxVQUFVLENBQU07WUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFBO1lBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNsQixRQUFRLGNBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFO29CQUNwQyxLQUFLLENBQUM7d0JBQ0osa0JBQVEsQ0FBQyxTQUFTLENBQUMsa0RBQWtELENBQUMsQ0FBQTt3QkFDdEUsTUFBTTtvQkFDUixLQUFLLENBQUM7d0JBQ0osa0JBQVEsQ0FBQyxTQUFTLENBQUMsNENBQTRDLENBQUMsQ0FBQTt3QkFDaEUsTUFBTTtvQkFDUixLQUFLLENBQUM7d0JBQ0osa0JBQVEsQ0FBQyxTQUFTLENBQUMsOENBQThDLENBQUMsQ0FBQTt3QkFDbEUsTUFBTTtvQkFDUixLQUFLLENBQUM7d0JBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO3dCQUN2QyxNQUFNO29CQUNSLEtBQUssQ0FBQzt3QkFDSixJQUFJLENBQUMsU0FBUyxDQUFDLHVDQUF1QyxDQUFDLENBQUE7d0JBQ3ZELE1BQU07b0JBQ1IsS0FBSyxDQUFDO3dCQUNKLGtCQUFRLENBQUMsU0FBUyxDQUFDLGdEQUFnRCxDQUFDLENBQUE7d0JBQ3BFLE1BQU07b0JBQ1IsS0FBSyxDQUFDO3dCQUNKLGtCQUFRLENBQUMsU0FBUyxDQUFDLDBEQUEwRCxDQUFDLENBQUE7d0JBQzlFLE1BQU07b0JBQ1IsS0FBSyxDQUFDO3dCQUVKLE1BQU07b0JBQ1IsS0FBSyxDQUFDO3dCQUNKLGtCQUFRLENBQUMsU0FBUyxDQUFDLGdEQUFnRCxDQUFDLENBQUE7d0JBQ3BFLE1BQU07b0JBQ1IsS0FBSyxFQUFFO3dCQUVMLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQzs0QkFDdkIsS0FBSyxFQUFFLG9CQUFvQjs0QkFDM0IsSUFBSSxFQUFFLDBCQUEwQjs0QkFDaEMsU0FBUyxFQUFFO2dDQUNULEdBQUcsRUFBRSxLQUFLOzZCQUNYOzRCQUNELFVBQVUsRUFBRSxTQUFTOzRCQUNyQixPQUFPLFlBQUMsR0FBRztnQ0FFVCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBOzRCQUNsQixDQUFDO3lCQUNGLENBQUMsQ0FBQTt3QkFDRixNQUFNO29CQUNSO3dCQUNFLE1BQU07aUJBQ1Q7YUFDRjtpQkFBTTtnQkFDTCxFQUFFLENBQUMsU0FBUyxDQUFDO29CQUNYLEtBQUssRUFBRSxNQUFNO29CQUNiLE9BQU8sRUFBRSxNQUFNO29CQUNmLE9BQU8sRUFBRSxVQUFDLEdBQUc7d0JBQ1gsSUFBSSxHQUFHLENBQUMsT0FBTyxFQUFFOzRCQUVmLGtCQUFRLENBQUMsU0FBUyxDQUFDLHNDQUFzQyxDQUFDLENBQUE7eUJBQzNEOzZCQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sRUFBRTt5QkFFdEI7b0JBQ0gsQ0FBQztpQkFDRixDQUFDLENBQUE7YUFDSDtRQUNILENBQUM7S0FFRjtDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDmraTnu4Tku7bnlKjkuo7mmL7npLrlip/og73mnI3liqHljLrln59cclxuICovXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSdcclxuaW1wb3J0IGNvbW0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvY29tbSdcclxuQ29tcG9uZW50KHtcclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTlsZ7mgKfliJfooahcclxuICAgKi9cclxuICBwcm9wZXJ0aWVzOiB7XHJcbiAgICAvLyDmjqXlj5fniLbnu4Tku7bkvKDpgJLnmoTmlbDmja5cclxuICAgIGZ1bkl0ZW1zOiB7XHJcbiAgICAgIHR5cGU6IEFycmF5LFxyXG4gICAgICB2YWx1ZTogW11cclxuICAgIH1cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnu4Tku7bnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOe7hOS7tueahOaWueazleWIl+ihqFxyXG4gICAqL1xyXG4gIG1ldGhvZHM6IHtcclxuXHJcbiAgICAvLyDliKTmlq3mmK/lkKbnmbvlvZVcclxuICAgIGlzTG9naW4oKTogYW55IHtcclxuICAgICAgbGV0IGlzTG9naW4gPSB3eC5nZXRTdG9yYWdlU3luYygnaXNMb2dpbicpXHJcbiAgICAgIHJldHVybiBpc0xvZ2luXHJcbiAgICB9LFxyXG5cclxuICAgIC8vIOaPkOekuuahhlxyXG4gICAgc2hvd01vZGFsKGNvbnRlbnQ6IGFueSkge1xyXG4gICAgICB3eC5zaG93TW9kYWwoe1xyXG4gICAgICAgIHRpdGxlOiAn5rip6aao5o+Q56S6JyxcclxuICAgICAgICBjb250ZW50OiBjb250ZW50LFxyXG4gICAgICAgIGNvbmZpcm1UZXh0OiBcIuWFs+mXrVwiLFxyXG4gICAgICAgIHNob3dDYW5jZWw6IGZhbHNlLFxyXG4gICAgICAgIHN1Y2Nlc3M6IChyZXMpID0+IHtcclxuICAgICAgICAgIGlmIChyZXMuY29uZmlybSkge1xyXG4gICAgICAgICAgICAvL+eCueWHu+ehruWumuaMiemSrlxyXG4gICAgICAgICAgfSBlbHNlIGlmIChyZXMuY2FuY2VsKSB7XHJcbiAgICAgICAgICAgIC8v54K55Ye75Y+W5raI5oyJ6ZKuXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSxcclxuXHJcbiAgICAvLyDot7PovazliLDlkITkuKrlip/og73pobXpnaJcclxuICAgIGdvVG9QYWdlcyhlOiBhbnkpOiBhbnkge1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmlzTG9naW4oKSlcclxuICAgICAgaWYgKHRoaXMuaXNMb2dpbigpKSB7XHJcbiAgICAgICAgc3dpdGNoIChjb21tLmdldFRhcmdldFZhbHVlKGUsICdpZCcpKSB7XHJcbiAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgIG5hdmlnYXRlLmdvVG9QYW5lbCgnL3BhZ2VzL3Byb2ZpbGUvcGVyc29uLWNvbW11bml0eS9wZXJzb24tY29tbXVuaXR5JylcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlIDE6XHJcbiAgICAgICAgICAgIG5hdmlnYXRlLmdvVG9QYW5lbCgnL3BhZ2VzL3Byb2ZpbGUvcGVyc29uLXJlY29yZC9wZXJzb24tcmVjb3JkJylcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlIDI6XHJcbiAgICAgICAgICAgIG5hdmlnYXRlLmdvVG9QYW5lbCgnL3BhZ2VzL3Byb2ZpbGUvcGVyc29uLXBheW1lbnQvcGVyc29uLXBheW1lbnQnKVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGNhc2UgMzpcclxuICAgICAgICAgICAgdGhpcy5zaG93TW9kYWwoJ+aCqOayoeacieW8gOmXqOadg+mZkOaIluW8gOmXqOadg+mZkOW3sui/h+acn+S4jeiDvemCgOivt+WutuS6uicpXHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgY2FzZSA0OlxyXG4gICAgICAgICAgICB0aGlzLnNob3dNb2RhbCgn5oKo5rKh5pyJ5byA6Zeo5p2D6ZmQ5oiW5oKo5omA5Zyo55qE5bCP5Yy65pyq5byA5ZCv6K6/5a6i6YKA6K+35Yqf6IO9XFxyXFxu5LiN6IO96L+b6KGM6K6/5a6i5o6I5p2D5pON5L2cJylcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlIDY6XHJcbiAgICAgICAgICAgIG5hdmlnYXRlLmdvVG9QYW5lbCgnL3BhZ2VzL3Byb2ZpbGUvcGVyc29uLXByb3BlcnR5L3BlcnNvbi1wcm9wZXJ0eScpXHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgY2FzZSA3OlxyXG4gICAgICAgICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoJy9wYWdlcy9wcm9maWxlL3BlcnNvbi1saWNlbnNlLXBsYXRlL3BlcnNvbi1saWNlbnNlLXBsYXRlJylcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlIDg6XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGNhc2UgOTpcclxuICAgICAgICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsKCcvcGFnZXMvcHJvZmlsZS9wZXJzb24tZmVlZGJhY2svcGVyc29uLWZlZWRiYWNrJylcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICBjYXNlIDEwOlxyXG4gICAgICAgICAgICAvLyDot7PovazliLDlhbblroPlsI/nqIvluo/vvIjpnIDopoFhcHBJZO+8iVxyXG4gICAgICAgICAgICB3eC5uYXZpZ2F0ZVRvTWluaVByb2dyYW0oe1xyXG4gICAgICAgICAgICAgIGFwcElkOiAnd3hhNzVlZmE2NDhiNjA5OTRiJyxcclxuICAgICAgICAgICAgICBwYXRoOiAncGFnZXMvaW5kZXgvaW5kZXg/aWQ9MTIzJyxcclxuICAgICAgICAgICAgICBleHRyYURhdGE6IHtcclxuICAgICAgICAgICAgICAgIGZvbzogJ2JhcidcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIGVudlZlcnNpb246ICdyZWxlYXNlJyxcclxuICAgICAgICAgICAgICBzdWNjZXNzKHJlcykge1xyXG4gICAgICAgICAgICAgICAgLy8g5omT5byA5oiQ5YqfXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB3eC5zaG93TW9kYWwoe1xyXG4gICAgICAgICAgdGl0bGU6ICfmuKnppqjmj5DnpLonLFxyXG4gICAgICAgICAgY29udGVudDogJ+ivt+WFiOeZu+W9lScsXHJcbiAgICAgICAgICBzdWNjZXNzOiAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChyZXMuY29uZmlybSkge1xyXG4gICAgICAgICAgICAgIC8v54K55Ye756Gu5a6a5oyJ6ZKuXHJcbiAgICAgICAgICAgICAgbmF2aWdhdGUuZ29Ub1BhbmVsKCcvcGFnZXMvcHJvZmlsZS9sb2dpbi1hdXRoL2xvZ2luLWF1dGgnKVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcy5jYW5jZWwpIHtcclxuICAgICAgICAgICAgICAvL+eCueWHu+WPlua2iOaMiemSrlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICB9XHJcbn0pXHJcbiJdfQ==