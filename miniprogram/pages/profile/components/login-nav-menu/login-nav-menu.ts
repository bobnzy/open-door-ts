/**
 * 此组件 功能展示
 */
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    // 接受父组件传递的参数
    imgList: {
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
