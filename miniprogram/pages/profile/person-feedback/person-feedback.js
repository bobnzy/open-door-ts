"use strict";
Page({
    data: {
        active: '',
        questions: [
            {
                value: "0",
                name: "意见建议"
            },
            {
                value: "1",
                name: "功能异常"
            },
            {
                value: "2",
                name: "使用帮助"
            },
            {
                value: "3",
                name: "其他问题"
            }
        ],
        textareaLength: 0,
        upImgNum: 0,
        questionType: "",
        description: "",
        describeImgs: [],
    },
    radioPick: function (e) {
        var index = e.currentTarget.dataset.index;
        var QT = this.data.questions[index].name;
        this.setData({
            active: index,
            questionType: QT
        });
    },
    textareaChange: function (e) {
        this.setData({
            textareaLength: e.detail.value.length
        });
    },
    textareaBlur: function (e) {
        var descriptionValue = e.detail.value;
        this.setData({
            description: descriptionValue
        });
    },
    addImg: function () {
        if (this.data.upImgNum >= 5)
            return;
        var that = this;
        wx.chooseImage({
            count: 5,
            sizeType: ['original', 'compressed'],
            sourceType: ['album', 'camera'],
            success: function (res) {
                var uploadImgList = that.data.describeImgs;
                var imgNum = uploadImgList.length;
                for (var i in res.tempFilePaths) {
                    if (imgNum < 5) {
                        uploadImgList.push({
                            imgUrl: res.tempFilePaths[i]
                        });
                        imgNum++;
                    }
                    else {
                        wx.showToast({
                            title: '最多5张！',
                            icon: 'none',
                            duration: 2000
                        });
                        break;
                    }
                }
                that.setData({
                    describeImgs: uploadImgList,
                    upImgNum: imgNum
                });
            },
            removeImg: function (e) {
                var imgId = e.currentTarget.dataset.id;
                this.data.describeImgs.splice(imgId, 1);
                this.setData({
                    describeImgs: this.data.describeImgs,
                    upImgNum: this.data.upImgNum - 1
                });
            },
            formSubmit: function () {
                var data = {
                    questionType: this.data.questionType,
                    description: this.data.description,
                    describeImgs: this.data.describeImgs,
                };
                console.log('form发生了submit事件，携带数据为：', data);
            }
        });
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '意见反馈'
        });
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLWZlZWRiYWNrLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsicGVyc29uLWZlZWRiYWNrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFHQSxJQUFJLENBQUM7SUFLSCxJQUFJLEVBQUU7UUFDSixNQUFNLEVBQUUsRUFBRTtRQUNWLFNBQVMsRUFBRTtZQUNUO2dCQUNFLEtBQUssRUFBRSxHQUFHO2dCQUNWLElBQUksRUFBRSxNQUFNO2FBQ2I7WUFDRDtnQkFDRSxLQUFLLEVBQUUsR0FBRztnQkFDVixJQUFJLEVBQUUsTUFBTTthQUNiO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLEdBQUc7Z0JBQ1YsSUFBSSxFQUFFLE1BQU07YUFDYjtZQUNEO2dCQUNFLEtBQUssRUFBRSxHQUFHO2dCQUNWLElBQUksRUFBRSxNQUFNO2FBQ2I7U0FDRjtRQUNELGNBQWMsRUFBRSxDQUFDO1FBQ2pCLFFBQVEsRUFBRSxDQUFDO1FBR1gsWUFBWSxFQUFFLEVBQUU7UUFDaEIsV0FBVyxFQUFFLEVBQUU7UUFDZixZQUFZLEVBQUUsRUFBUztLQUV4QjtJQUdELFNBQVMsWUFBQyxDQUFNO1FBRWQsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFBO1FBRzNDLElBQU0sRUFBRSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQTtRQUcxQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsTUFBTSxFQUFFLEtBQUs7WUFDYixZQUFZLEVBQUUsRUFBRTtTQUNqQixDQUFDLENBQUE7SUFDSixDQUFDO0lBR0QsY0FBYyxFQUFFLFVBQVUsQ0FBTTtRQUU5QixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsY0FBYyxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU07U0FDdEMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELFlBQVksRUFBRSxVQUFVLENBQU07UUFFNUIsSUFBTSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQTtRQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsV0FBVyxFQUFFLGdCQUFnQjtTQUM5QixDQUFDLENBQUE7SUFFSixDQUFDO0lBTUQsTUFBTSxFQUFFO1FBQ04sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxDQUFDO1lBQUUsT0FBTTtRQUNuQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsRUFBRSxDQUFDLFdBQVcsQ0FBQztZQUNiLEtBQUssRUFBRSxDQUFDO1lBQ1IsUUFBUSxFQUFFLENBQUMsVUFBVSxFQUFFLFlBQVksQ0FBQztZQUNwQyxVQUFVLEVBQUUsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDO1lBQy9CLE9BQU8sRUFBRSxVQUFVLEdBQUc7Z0JBRXBCLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFBO2dCQUMxQyxJQUFJLE1BQU0sR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFBO2dCQUNqQyxLQUFLLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxhQUFhLEVBQUU7b0JBRS9CLElBQUksTUFBTSxHQUFHLENBQUMsRUFBRTt3QkFDZCxhQUFhLENBQUMsSUFBSSxDQUFDOzRCQUNqQixNQUFNLEVBQUUsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7eUJBQzdCLENBQUMsQ0FBQTt3QkFDRixNQUFNLEVBQUUsQ0FBQTtxQkFDVDt5QkFBTTt3QkFDTCxFQUFFLENBQUMsU0FBUyxDQUFDOzRCQUNYLEtBQUssRUFBRSxPQUFPOzRCQUNkLElBQUksRUFBRSxNQUFNOzRCQUNaLFFBQVEsRUFBRSxJQUFJO3lCQUNmLENBQUMsQ0FBQTt3QkFDRixNQUFNO3FCQUNQO2lCQUNGO2dCQUlELElBQUksQ0FBQyxPQUFPLENBQUM7b0JBQ1gsWUFBWSxFQUFFLGFBQWE7b0JBQzNCLFFBQVEsRUFBRSxNQUFNO2lCQUNqQixDQUFDLENBQUE7WUFHSixDQUFDO1lBR0QsU0FBUyxZQUFDLENBQU07Z0JBQ2QsSUFBTSxLQUFLLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFBO2dCQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFBO2dCQUN2QyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUNYLFlBQVksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVk7b0JBQ3BDLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDO2lCQUNqQyxDQUFDLENBQUE7WUFDSixDQUFDO1lBR0QsVUFBVTtnQkFDUixJQUFNLElBQUksR0FBRztvQkFDWCxZQUFZLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZO29CQUNwQyxXQUFXLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXO29CQUNsQyxZQUFZLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZO2lCQUNyQyxDQUFBO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUE7WUF3QjdDLENBQUM7U0FDRixDQUFDLENBQUE7SUFDSixDQUFDO0lBT0QsTUFBTSxFQUFFLFVBQVUsUUFBUTtRQUN4QixFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDdkIsS0FBSyxFQUFFLE1BQU07U0FDZCxDQUFDLENBQUE7SUFDSixDQUFDO0lBS0QsT0FBTyxFQUFFO0lBRVQsQ0FBQztJQUtELE1BQU0sRUFBRTtJQUVSLENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsUUFBUSxFQUFFO0lBRVYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7SUFLRCxhQUFhLEVBQUU7SUFFZixDQUFDO0NBRUYsQ0FBQyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIOWKn+iDveacjeWKoS3nlKjmiLflj43ppojpobXpnaJcclxuICovXHJcblBhZ2Uoe1xyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcbiAgICBhY3RpdmU6ICcnLCAvLyDlvZPliY3ngrnlh7vnmoTpl67popjnsbvlnotpbmRleOWvueW6lOWAvO+8jOeUqOS7peaUueWPmOagt+W8j1xyXG4gICAgcXVlc3Rpb25zOiBbIC8vIOmXrumimOexu+Wei+WIl+ihqFxyXG4gICAgICB7XHJcbiAgICAgICAgdmFsdWU6IFwiMFwiLFxyXG4gICAgICAgIG5hbWU6IFwi5oSP6KeB5bu66K6uXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHZhbHVlOiBcIjFcIixcclxuICAgICAgICBuYW1lOiBcIuWKn+iDveW8guW4uFwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICB2YWx1ZTogXCIyXCIsXHJcbiAgICAgICAgbmFtZTogXCLkvb/nlKjluK7liqlcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgdmFsdWU6IFwiM1wiLFxyXG4gICAgICAgIG5hbWU6IFwi5YW25LuW6Zeu6aKYXCJcclxuICAgICAgfVxyXG4gICAgXSxcclxuICAgIHRleHRhcmVhTGVuZ3RoOiAwLCAvL+aWh+acrOWfn+i+k+WFpeaWh+acrOmVv+W6plxyXG4gICAgdXBJbWdOdW06IDAsIC8vIOW9k+WJjeS4iuS8oOWbvueJh+aVsFxyXG5cclxuICAgIC8vIOihqOWNleaVsOaNrlxyXG4gICAgcXVlc3Rpb25UeXBlOiBcIlwiLCAvL+mXrumimOexu+Wei1xyXG4gICAgZGVzY3JpcHRpb246IFwiXCIsIC8vIOmXrumimOaWh+Wtl+aPj+i/sFxyXG4gICAgZGVzY3JpYmVJbWdzOiBbXSBhcyBhbnksIC8vIOS4iuS8oOmXrumimOaIquWbvuWIl+ihqFxyXG5cclxuICB9LFxyXG5cclxuICAvLyDpgInmi6nlj43ppojpl67popjnsbvlnotcclxuICByYWRpb1BpY2soZTogYW55KSB7XHJcbiAgICAvLyDojrflj5bngrnlh7vnmoTnsbvlnovnmoRpbmRleFxyXG4gICAgY29uc3QgaW5kZXggPSBlLmN1cnJlbnRUYXJnZXQuZGF0YXNldC5pbmRleFxyXG5cclxuICAgIC8vIC8vIOihqOWNleWAvFxyXG4gICAgY29uc3QgUVQgPSB0aGlzLmRhdGEucXVlc3Rpb25zW2luZGV4XS5uYW1lXHJcblxyXG4gICAgLy8g5pu05pS55a+55bqUYWN0aXZl5YC877yM5qC35byP5Y+Y6ImyXHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICBhY3RpdmU6IGluZGV4LFxyXG4gICAgICBxdWVzdGlvblR5cGU6IFFUXHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG4gIC8vIOaWh+acrOWfn+i+k+WFpVxyXG4gIHRleHRhcmVhQ2hhbmdlOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhlLmRldGFpbC52YWx1ZS5sZW5ndGgpXHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICB0ZXh0YXJlYUxlbmd0aDogZS5kZXRhaWwudmFsdWUubGVuZ3RoXHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgLy8g5paH5pys5Z+f5aSx54SmXHJcbiAgdGV4dGFyZWFCbHVyOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhlLmRldGFpbC52YWx1ZSxcIumVv+W6pu+8mlwiLGUuZGV0YWlsLnZhbHVlLmxlbmd0aClcclxuICAgIGNvbnN0IGRlc2NyaXB0aW9uVmFsdWUgPSBlLmRldGFpbC52YWx1ZVxyXG4gICAgdGhpcy5zZXREYXRhKHtcclxuICAgICAgZGVzY3JpcHRpb246IGRlc2NyaXB0aW9uVmFsdWVcclxuICAgIH0pXHJcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLmRhdGEpXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog5LiK5Lyg5Zu+54mHXHJcbiAgICovXHJcbiAgLy8g54K55Ye75re75Yqg5Zu+54mH5LiK5LygXHJcbiAgYWRkSW1nOiBmdW5jdGlvbiAoKSB7XHJcbiAgICBpZiAodGhpcy5kYXRhLnVwSW1nTnVtID49IDUpIHJldHVybiAvL+WbvueJh+W3sua7oe+8jOS7gOS5iOmDveS4jeWBmlxyXG4gICAgdmFyIHRoYXQgPSB0aGlzO1xyXG4gICAgd3guY2hvb3NlSW1hZ2UoeyAvL+S7juacrOWcsOebuOWGjOmAieaLqeWbvueJh+aIluS9v+eUqOebuOacuuaLjeeFp1xyXG4gICAgICBjb3VudDogNSwgLy8g5LiA5qyh5pyA5aSa5Y+v5Lul6YCJ5oup55qE5Zu+54mH5pWw77yM6buY6K6kOVxyXG4gICAgICBzaXplVHlwZTogWydvcmlnaW5hbCcsICdjb21wcmVzc2VkJ10sIC8vIOWPr+S7peaMh+WumuaYr+WOn+Wbvui/mOaYr+WOi+e8qeWbvu+8jOm7mOiupOS6jOiAhemDveaciVxyXG4gICAgICBzb3VyY2VUeXBlOiBbJ2FsYnVtJywgJ2NhbWVyYSddLCAvLyDlj6/ku6XmjIflrprmnaXmupDmmK/nm7jlhozov5jmmK/nm7jmnLrvvIzpu5jorqTkuozogIXpg73mnIlcclxuICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlcykge1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHJlcylcclxuICAgICAgICBsZXQgdXBsb2FkSW1nTGlzdCA9IHRoYXQuZGF0YS5kZXNjcmliZUltZ3NcclxuICAgICAgICBsZXQgaW1nTnVtID0gdXBsb2FkSW1nTGlzdC5sZW5ndGggLy/lvZPliY3lt7LmnInlm77niYfmlbBcclxuICAgICAgICBmb3IgKGxldCBpIGluIHJlcy50ZW1wRmlsZVBhdGhzKSB7XHJcbiAgICAgICAgICAvLyAg5Yik5pat5Zu+54mH5pWw5piv5ZCm5aSn5LqO5LiK5Lyg5LiK6ZmQNe+8jOayoeacieWwseWKoOWFpeaVsOe7hOaYvuekuuWHuuadpVxyXG4gICAgICAgICAgaWYgKGltZ051bSA8IDUpIHtcclxuICAgICAgICAgICAgdXBsb2FkSW1nTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgICBpbWdVcmw6IHJlcy50ZW1wRmlsZVBhdGhzW2ldXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGltZ051bSsrXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB3eC5zaG93VG9hc3Qoe1xyXG4gICAgICAgICAgICAgIHRpdGxlOiAn5pyA5aSaNeW8oO+8gScsXHJcbiAgICAgICAgICAgICAgaWNvbjogJ25vbmUnLFxyXG4gICAgICAgICAgICAgIGR1cmF0aW9uOiAyMDAwXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBjb25zb2xlLmxvZyh1cGxvYWRJbWdMaXN0KVxyXG5cclxuICAgICAgICAvL+WJjeWPsOaYvuekulxyXG4gICAgICAgIHRoYXQuc2V0RGF0YSh7XHJcbiAgICAgICAgICBkZXNjcmliZUltZ3M6IHVwbG9hZEltZ0xpc3QsXHJcbiAgICAgICAgICB1cEltZ051bTogaW1nTnVtXHJcbiAgICAgICAgfSlcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGF0LmRhdGEuZGVzY3JpYmVJbWdzKVxyXG5cclxuICAgICAgfSxcclxuXHJcbiAgICAgIC8vIOenu+mZpOWbvueJh1xyXG4gICAgICByZW1vdmVJbWcoZTogYW55KSB7XHJcbiAgICAgICAgY29uc3QgaW1nSWQgPSBlLmN1cnJlbnRUYXJnZXQuZGF0YXNldC5pZFxyXG4gICAgICAgIHRoaXMuZGF0YS5kZXNjcmliZUltZ3Muc3BsaWNlKGltZ0lkLCAxKVxyXG4gICAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgICBkZXNjcmliZUltZ3M6IHRoaXMuZGF0YS5kZXNjcmliZUltZ3MsXHJcbiAgICAgICAgICB1cEltZ051bTogdGhpcy5kYXRhLnVwSW1nTnVtIC0gMVxyXG4gICAgICAgIH0pXHJcbiAgICAgIH0sXHJcblxyXG4gICAgICAvLyDooajljZXmj5DkuqRcclxuICAgICAgZm9ybVN1Ym1pdCgpIHtcclxuICAgICAgICBjb25zdCBkYXRhID0ge1xyXG4gICAgICAgICAgcXVlc3Rpb25UeXBlOiB0aGlzLmRhdGEucXVlc3Rpb25UeXBlLCAvL+mXrumimOexu+Wei1xyXG4gICAgICAgICAgZGVzY3JpcHRpb246IHRoaXMuZGF0YS5kZXNjcmlwdGlvbiwgLy8g6Zeu6aKY5paH5a2X5o+P6L+wXHJcbiAgICAgICAgICBkZXNjcmliZUltZ3M6IHRoaXMuZGF0YS5kZXNjcmliZUltZ3MsIC8vIOS4iuS8oOmXrumimOaIquWbvuWIl+ihqFxyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmxvZygnZm9ybeWPkeeUn+S6hnN1Ym1pdOS6i+S7tu+8jOaQuuW4puaVsOaNruS4uu+8micsIGRhdGEpXHJcblxyXG4gICAgICAgIC8vIOaVsOaNruivt+axglxyXG5cclxuXHJcbiAgICAgICAgLy8gLy8g5LiK5Lyg5Yiw5pyN5Yqh5Zmo77yaKOa1i+ivlSlcclxuICAgICAgICAvLyAvLyDov5Tlm57pgInlrprnhafniYfnmoTmnKzlnLDmlofku7bot6/lvoTliJfooajvvIx0ZW1wRmlsZVBhdGjlj6/ku6XkvZzkuLppbWfmoIfnrb7nmoRzcmPlsZ7mgKfmmL7npLrlm77niYdcclxuICAgICAgICAvLyB2YXIgdGVtcEZpbGVQYXRocyA9IHRoaXMuZGF0YS5kZXNjcmliZUltZ3NcclxuICAgICAgICAvLyB3eC51cGxvYWRGaWxlKHtcclxuICAgICAgICAvLyAgIHVybDogJ2h0dHA6Ly8xNzIuMTYuMy4yNDc6ODA4MC9hcGkvYXJlYS91cGRhdGVJbWcnLFxyXG4gICAgICAgIC8vICAgbmFtZTogJ2ZpbGUnLFxyXG4gICAgICAgIC8vICAgZmlsZVBhdGg6IHRlbXBGaWxlUGF0aHNbMF0uaW1nVXJsLFxyXG4gICAgICAgIC8vICAgaGVhZGVyOiB7XHJcbiAgICAgICAgLy8gICAgIFwiQ29udGVudC1UeXBlXCI6IFwibXVsdGlwYXJ0L2Zvcm0tZGF0YVwiLFxyXG4gICAgICAgIC8vICAgICAnYWNjZXB0JzogJ2FwcGxpY2F0aW9uL2pzb24nXHJcbiAgICAgICAgLy8gICB9LFxyXG4gICAgICAgIC8vICAgZm9ybURhdGE6IHt9LFxyXG4gICAgICAgIC8vICAgc3VjY2VzczogKHJlczogYW55KSA9PiB7XHJcbiAgICAgICAgLy8gICAgIGNvbnNvbGUubG9nKHJlcylcclxuICAgICAgICAvLyAgIH0sXHJcbiAgICAgICAgLy8gICBmYWlsOiAocmVzOiBhbnkpID0+IHtcclxuICAgICAgICAvLyAgICAgY29uc29sZS5sb2cocmVzLCB0ZW1wRmlsZVBhdGhzWzBdLmltZ1VybClcclxuICAgICAgICAvLyAgIH1cclxuICAgICAgICAvLyB9KVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLliqDovb1cclxuICAgKi9cclxuICBvbkxvYWQ6IGZ1bmN0aW9uIChfb3B0aW9ucykge1xyXG4gICAgd3guc2V0TmF2aWdhdGlvbkJhclRpdGxlKHtcclxuICAgICAgdGl0bGU6ICfmhI/op4Hlj43ppognXHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yid5qyh5riy5p+T5a6M5oiQXHJcbiAgICovXHJcbiAgb25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouaYvuekulxyXG4gICAqL1xyXG4gIG9uU2hvdzogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWNuOi9vVxyXG4gICAqL1xyXG4gIG9uVW5sb2FkOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouebuOWFs+S6i+S7tuWkhOeQhuWHveaVsC0t55uR5ZCs55So5oi35LiL5ouJ5Yqo5L2cXHJcbiAgICovXHJcbiAgb25QdWxsRG93blJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxufSkiXX0=