"use strict";
Page({
    data: {
        steps: [{
                imgUrl: "/assets/images/apply-step1.png",
                msgTitle: "1.联系物业",
                msgText1: "前往社区物业管理处",
                msgText2: "联系管理人员提供授权二维码"
            }, {
                imgUrl: "/assets/images/apply-step2.png",
                msgTitle: "2.扫描授权二维码",
                msgText1: "微信扫描授权二维码",
                msgText2: "按管理人员要求填写申请资料"
            }, {
                imgUrl: "/assets/images/apply-step3.png",
                msgTitle: "3.提交认证",
                msgText1: "提交资料，立即获取开门权限",
                msgText2: "就可以开门了哦~"
            }]
    },
    getBack: function () {
        wx.navigateBack({
            delta: 0,
        });
    },
    onload: function (_options) {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwbHktc3RlcHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhcHBseS1zdGVwcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBSUEsSUFBSSxDQUFDO0lBSUgsSUFBSSxFQUFFO1FBQ0osS0FBSyxFQUFDLENBQUM7Z0JBQ0wsTUFBTSxFQUFFLGdDQUFnQztnQkFDeEMsUUFBUSxFQUFFLFFBQVE7Z0JBQ2xCLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixRQUFRLEVBQUUsZUFBZTthQUMxQixFQUFFO2dCQUNELE1BQU0sRUFBRSxnQ0FBZ0M7Z0JBQ3hDLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixRQUFRLEVBQUUsV0FBVztnQkFDckIsUUFBUSxFQUFFLGVBQWU7YUFDMUIsRUFBRTtnQkFDRCxNQUFNLEVBQUUsZ0NBQWdDO2dCQUN4QyxRQUFRLEVBQUUsUUFBUTtnQkFDbEIsUUFBUSxFQUFFLGVBQWU7Z0JBQ3pCLFFBQVEsRUFBRSxVQUFVO2FBQ3JCLENBQWU7S0FDakI7SUFHRCxPQUFPO1FBQ0wsRUFBRSxDQUFDLFlBQVksQ0FBQztZQUNkLEtBQUssRUFBRSxDQUFDO1NBQ1QsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUVELE1BQU0sRUFBRSxVQUFVLFFBQWE7SUFFL0IsQ0FBQztDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiDnlLPor7fmnYPpmZDmraXpqqTor7TmmI5cclxuICovXHJcblxyXG5QYWdlKHtcclxuICAvKipcclxuICAgKiDpobXpnaLnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcbiAgICBzdGVwczpbe1xyXG4gICAgICBpbWdVcmw6IFwiL2Fzc2V0cy9pbWFnZXMvYXBwbHktc3RlcDEucG5nXCIsXHJcbiAgICAgIG1zZ1RpdGxlOiBcIjEu6IGU57O754mp5LiaXCIsXHJcbiAgICAgIG1zZ1RleHQxOiBcIuWJjeW+gOekvuWMuueJqeS4mueuoeeQhuWkhFwiLFxyXG4gICAgICBtc2dUZXh0MjogXCLogZTns7vnrqHnkIbkurrlkZjmj5DkvpvmjojmnYPkuoznu7TnoIFcIlxyXG4gICAgfSwge1xyXG4gICAgICBpbWdVcmw6IFwiL2Fzc2V0cy9pbWFnZXMvYXBwbHktc3RlcDIucG5nXCIsXHJcbiAgICAgIG1zZ1RpdGxlOiBcIjIu5omr5o+P5o6I5p2D5LqM57u056CBXCIsXHJcbiAgICAgIG1zZ1RleHQxOiBcIuW+ruS/oeaJq+aPj+aOiOadg+S6jOe7tOeggVwiLFxyXG4gICAgICBtc2dUZXh0MjogXCLmjInnrqHnkIbkurrlkZjopoHmsYLloavlhpnnlLPor7fotYTmlplcIlxyXG4gICAgfSwge1xyXG4gICAgICBpbWdVcmw6IFwiL2Fzc2V0cy9pbWFnZXMvYXBwbHktc3RlcDMucG5nXCIsXHJcbiAgICAgIG1zZ1RpdGxlOiBcIjMu5o+Q5Lqk6K6k6K+BXCIsXHJcbiAgICAgIG1zZ1RleHQxOiBcIuaPkOS6pOi1hOaWme+8jOeri+WNs+iOt+WPluW8gOmXqOadg+mZkFwiLFxyXG4gICAgICBtc2dUZXh0MjogXCLlsLHlj6/ku6XlvIDpl6jkuoblk6Z+XCJcclxuICAgIH1dIGFzIEFycmF5PGFueT5cclxuICB9LFxyXG5cclxuICAvLyDigJzlpb3vvIznn6XpgZPkuobigJ3mjInpkq7vvIzmjInkuIvov5Tlm57kuIrkuIDnuqfpobXpnaJcclxuICBnZXRCYWNrKCkge1xyXG4gICAgd3gubmF2aWdhdGVCYWNrKHtcclxuICAgICAgZGVsdGE6IDAsXHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG4gIG9ubG9hZDogZnVuY3Rpb24gKF9vcHRpb25zOiBhbnkpIHtcclxuICAgIFxyXG4gIH1cclxufSkiXX0=