/**
 * 申请权限步骤说明
 */

Page({
  /**
   * 页面的初始数据
   */
  data: {
    steps:[{
      imgUrl: "/assets/images/apply-step1.png",
      msgTitle: "1.联系物业",
      msgText1: "前往社区物业管理处",
      msgText2: "联系管理人员提供授权二维码"
    }, {
      imgUrl: "/assets/images/apply-step2.png",
      msgTitle: "2.扫描授权二维码",
      msgText1: "微信扫描授权二维码",
      msgText2: "按管理人员要求填写申请资料"
    }, {
      imgUrl: "/assets/images/apply-step3.png",
      msgTitle: "3.提交认证",
      msgText1: "提交资料，立即获取开门权限",
      msgText2: "就可以开门了哦~"
    }] as Array<any>
  },

  // “好，知道了”按钮，按下返回上一级页面
  getBack() {
    wx.navigateBack({
      delta: 0,
    })
  },

  onload: function (_options: any) {
    
  }
})