"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var comm_1 = require("../../../utils/comm");
var navigate_1 = require("../../../utils/navigate");
Page({
    data: {
        localCity: "",
        villageData: [{
                id: 1,
                place: "光华馨地"
            },
            {
                id: 2,
                place: "缤纷年华"
            },
            {
                id: 3,
                place: "滨河丽景"
            },
            {
                id: 4,
                place: "今日家园"
            },
            {
                id: 5,
                place: "双楠美邻"
            }
        ]
    },
    getCity: function () {
        var newCity = "成都市";
        this.setData({
            localCity: newCity
        });
        var formData = wx.getStorageSync('_placeForm');
        wx.setStorageSync('_placeForm', __assign({}, formData, { city: newCity }));
    },
    choiceReset: function () {
        wx.showToast({
            title: '正在加载',
            icon: 'loading',
            duration: 1000
        });
    },
    choiceOther: function () {
        wx.showToast({
            title: '正在切换',
            icon: 'loading',
            duration: 1000
        });
    },
    choiceVillage: function (event) {
        var village = comm_1.default.getTargetValue(event, 'village');
        var formData = wx.getStorageSync('_placeForm');
        wx.setStorageSync('_placeForm', __assign({}, formData, { village: village }));
        navigate_1.default.goToPanel('/pages/apply/choise-building/choise-building');
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '选择小区'
        });
        this.getCity();
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hvaWNlLXBsYWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2hvaWNlLXBsYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFFQSw0Q0FBdUM7QUFDdkMsb0RBQStDO0FBRS9DLElBQUksQ0FBQztJQUtILElBQUksRUFBRTtRQUVKLFNBQVMsRUFBRSxFQUFFO1FBRWIsV0FBVyxFQUFFLENBQUM7Z0JBQ1osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07YUFDZDtZQUNEO2dCQUNFLEVBQUUsRUFBRSxDQUFDO2dCQUNMLEtBQUssRUFBRSxNQUFNO2FBQ2Q7WUFDRDtnQkFDRSxFQUFFLEVBQUUsQ0FBQztnQkFDTCxLQUFLLEVBQUUsTUFBTTthQUNkO1lBQ0Q7Z0JBQ0UsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07YUFDZDtZQUNEO2dCQUNFLEVBQUUsRUFBRSxDQUFDO2dCQUNMLEtBQUssRUFBRSxNQUFNO2FBQ2Q7U0FDQTtLQUNGO0lBRUQsT0FBTztRQUVMLElBQUksT0FBTyxHQUFXLEtBQUssQ0FBQTtRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsU0FBUyxFQUFFLE9BQU87U0FDbkIsQ0FBQyxDQUFBO1FBQ0YsSUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRCxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksZUFBTyxRQUFRLEVBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUcsQ0FBQztJQUN6RSxDQUFDO0lBRUQsV0FBVztRQUNULEVBQUUsQ0FBQyxTQUFTLENBQUM7WUFDWCxLQUFLLEVBQUUsTUFBTTtZQUNiLElBQUksRUFBRSxTQUFTO1lBQ2YsUUFBUSxFQUFFLElBQUk7U0FDZixDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsV0FBVztRQUNULEVBQUUsQ0FBQyxTQUFTLENBQUM7WUFDWCxLQUFLLEVBQUUsTUFBTTtZQUNiLElBQUksRUFBRSxTQUFTO1lBQ2YsUUFBUSxFQUFFLElBQUk7U0FDZixDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsYUFBYSxZQUFDLEtBQVU7UUFDdEIsSUFBTSxPQUFPLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDdEQsSUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRCxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksZUFBTyxRQUFRLEVBQUssRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUcsQ0FBQztRQUMxRSxrQkFBUSxDQUFDLFNBQVMsQ0FDaEIsOENBQThDLENBQy9DLENBQUE7SUFDSCxDQUFDO0lBSUQsTUFBTSxFQUFFLFVBQVUsUUFBUTtRQUV4QixFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDdkIsS0FBSyxFQUFFLE1BQU07U0FDZCxDQUFDLENBQUE7UUFFRixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUE7SUFDaEIsQ0FBQztJQUtELE9BQU8sRUFBRTtJQUVULENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELFFBQVEsRUFBRTtJQUVWLENBQUM7SUFLRCxpQkFBaUIsRUFBRTtJQUVuQixDQUFDO0lBS0QsYUFBYSxFQUFFO0lBRWYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyDnlLPor7flvIDpl6jmnYPpmZAt6YCJ5oup5bCP5Yy65L+h5oGv6aG16Z2iXHJcblxyXG5pbXBvcnQgQ29tbSBmcm9tICcuLi8uLi8uLi91dGlscy9jb21tJztcclxuaW1wb3J0IG5hdmlnYXRlIGZyb20gJy4uLy4uLy4uL3V0aWxzL25hdmlnYXRlJztcclxuXHJcblBhZ2Uoe1xyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcbiAgICAvLyDlvZPliY3ln47luIJcclxuICAgIGxvY2FsQ2l0eTogXCJcIixcclxuICAgIC8vIOacrOWcsOaooeaLn2pzb27mlbDmja5cclxuICAgIHZpbGxhZ2VEYXRhOiBbe1xyXG4gICAgICBpZDogMSxcclxuICAgICAgcGxhY2U6IFwi5YWJ5Y2O6aao5ZywXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGlkOiAyLFxyXG4gICAgICBwbGFjZTogXCLnvKTnurflubTljY5cIlxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgaWQ6IDMsXHJcbiAgICAgIHBsYWNlOiBcIua7qOays+S4veaZr1wiXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBpZDogNCxcclxuICAgICAgcGxhY2U6IFwi5LuK5pel5a625ZutXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGlkOiA1LFxyXG4gICAgICBwbGFjZTogXCLlj4zmpaDnvo7pgrtcIlxyXG4gICAgfVxyXG4gICAgXVxyXG4gIH0sXHJcbiAgLy8g6I635Y+W5b2T5YmN5Z+O5biCXHJcbiAgZ2V0Q2l0eSgpIHtcclxuICAgIC8vIOWBh+iuvuiOt+WPlueahOWfjuW4guaYr+aIkOmDveW4glxyXG4gICAgbGV0IG5ld0NpdHk6IHN0cmluZyA9IFwi5oiQ6YO95biCXCJcclxuICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgIGxvY2FsQ2l0eTogbmV3Q2l0eVxyXG4gICAgfSlcclxuICAgIGNvbnN0IGZvcm1EYXRhID0gd3guZ2V0U3RvcmFnZVN5bmMoJ19wbGFjZUZvcm0nKTtcclxuICAgIHd4LnNldFN0b3JhZ2VTeW5jKCdfcGxhY2VGb3JtJywgeyAuLi5mb3JtRGF0YSwgLi4ueyBjaXR5OiBuZXdDaXR5IH0gfSk7XHJcbiAgfSxcclxuICAvLyDph43mlrDlrprkvY3ojrflj5bln47luIJcclxuICBjaG9pY2VSZXNldCgpIHtcclxuICAgIHd4LnNob3dUb2FzdCh7XHJcbiAgICAgIHRpdGxlOiAn5q2j5Zyo5Yqg6L29JyxcclxuICAgICAgaWNvbjogJ2xvYWRpbmcnLFxyXG4gICAgICBkdXJhdGlvbjogMTAwMFxyXG4gICAgfSlcclxuICB9LFxyXG4gIC8vIOWIh+aNouW9k+WJjeWfjuW4guS4uuWFtuS7luWfjuW4glxyXG4gIGNob2ljZU90aGVyKCkge1xyXG4gICAgd3guc2hvd1RvYXN0KHtcclxuICAgICAgdGl0bGU6ICfmraPlnKjliIfmjaInLFxyXG4gICAgICBpY29uOiAnbG9hZGluZycsXHJcbiAgICAgIGR1cmF0aW9uOiAxMDAwXHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgLy8g6YCJ5oup5bCP5Yy677yM5pWw5o2u5Yqg5YWl57yT5a2YXHJcbiAgY2hvaWNlVmlsbGFnZShldmVudDogYW55KSB7XHJcbiAgICBjb25zdCB2aWxsYWdlID0gQ29tbS5nZXRUYXJnZXRWYWx1ZShldmVudCwgJ3ZpbGxhZ2UnKTtcclxuICAgIGNvbnN0IGZvcm1EYXRhID0gd3guZ2V0U3RvcmFnZVN5bmMoJ19wbGFjZUZvcm0nKTtcclxuICAgIHd4LnNldFN0b3JhZ2VTeW5jKCdfcGxhY2VGb3JtJywgeyAuLi5mb3JtRGF0YSwgLi4ueyB2aWxsYWdlOiB2aWxsYWdlIH0gfSk7XHJcbiAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoXHJcbiAgICAgICcvcGFnZXMvYXBwbHkvY2hvaXNlLWJ1aWxkaW5nL2Nob2lzZS1idWlsZGluZydcclxuICAgIClcclxuICB9LFxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yqg6L29XHJcbiAgICovXHJcbiAgb25Mb2FkOiBmdW5jdGlvbiAoX29wdGlvbnMpIHtcclxuICAgIC8vIOS/ruaUuemhtemdouagh+mimFxyXG4gICAgd3guc2V0TmF2aWdhdGlvbkJhclRpdGxlKHtcclxuICAgICAgdGl0bGU6ICfpgInmi6nlsI/ljLonXHJcbiAgICB9KVxyXG4gICAgLy8g6I635Y+W5b2T5YmN5Z+O5biCXHJcbiAgICB0aGlzLmdldENpdHkoKVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yid5qyh5riy5p+T5a6M5oiQXHJcbiAgICovXHJcbiAgb25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouaYvuekulxyXG4gICAqL1xyXG4gIG9uU2hvdzogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWNuOi9vVxyXG4gICAqL1xyXG4gIG9uVW5sb2FkOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouebuOWFs+S6i+S7tuWkhOeQhuWHveaVsC0t55uR5ZCs55So5oi35LiL5ouJ5Yqo5L2cXHJcbiAgICovXHJcbiAgb25QdWxsRG93blJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlKjmiLfngrnlh7vlj7PkuIrop5LliIbkuqtcclxuICAgKi9cclxuICBvblNoYXJlQXBwTWVzc2FnZTogZnVuY3Rpb24gKCk6IGFueSB7XHJcblxyXG4gIH1cclxufSkiXX0=