// 申请开门权限-选择小区信息页面

import Comm from '../../../utils/comm';
import navigate from '../../../utils/navigate';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前城市
    localCity: "",
    // 本地模拟json数据
    villageData: [{
      id: 1,
      place: "光华馨地"
    },
    {
      id: 2,
      place: "缤纷年华"
    },
    {
      id: 3,
      place: "滨河丽景"
    },
    {
      id: 4,
      place: "今日家园"
    },
    {
      id: 5,
      place: "双楠美邻"
    }
    ]
  },
  // 获取当前城市
  getCity() {
    // 假设获取的城市是成都市
    let newCity: string = "成都市"
    this.setData({
      localCity: newCity
    })
    const formData = wx.getStorageSync('_placeForm');
    wx.setStorageSync('_placeForm', { ...formData, ...{ city: newCity } });
  },
  // 重新定位获取城市
  choiceReset() {
    wx.showToast({
      title: '正在加载',
      icon: 'loading',
      duration: 1000
    })
  },
  // 切换当前城市为其他城市
  choiceOther() {
    wx.showToast({
      title: '正在切换',
      icon: 'loading',
      duration: 1000
    })
  },
  // 选择小区，数据加入缓存
  choiceVillage(event: any) {
    const village = Comm.getTargetValue(event, 'village');
    const formData = wx.getStorageSync('_placeForm');
    wx.setStorageSync('_placeForm', { ...formData, ...{ village: village } });
    navigate.goToPanel(
      '/pages/apply/choise-building/choise-building'
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '选择小区'
    })
    // 获取当前城市
    this.getCity()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})