// 申请开门权限-选择楼栋信息页面
import Comm from '../../../utils/comm';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 当前小区
    localVillage: "",
    // 小区的楼栋数据
    buildingData: [{
      id: 1,
      building: "楼栋1"
    },
    {
      id: 2,
      building: "楼栋2"
    },
    {
      id: 3,
      building: "楼栋3"
    }
    ]
  },
  // 获取当前小区
  getVillage() {
    const formData = wx.getStorageSync('_placeForm');
    this.setData({
      localVillage: formData.village
    })
   
  },
  // 切换小区，回到选择小区页面
  choiceOther() {
    wx.navigateBack({
      delta: 1,
    })
  },
  // 选择楼栋数据，返回申请权限页面
  choiceBuilding(event: any) {
    const building = Comm.getTargetValue(event, 'building');
    const formData = wx.getStorageSync('_placeForm');
    wx.setStorageSync('_placeForm', { ...formData, ...{ building: building } });
    wx.redirectTo({
      url: '/pages/apply/apply-place/apply-place',
    })
    wx.navigateBack({
      delta: 2,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '楼栋'
    })
    this.getVillage()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})