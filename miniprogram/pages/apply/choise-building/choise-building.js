"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var comm_1 = require("../../../utils/comm");
Page({
    data: {
        localVillage: "",
        buildingData: [{
                id: 1,
                building: "楼栋1"
            },
            {
                id: 2,
                building: "楼栋2"
            },
            {
                id: 3,
                building: "楼栋3"
            }
        ]
    },
    getVillage: function () {
        var formData = wx.getStorageSync('_placeForm');
        this.setData({
            localVillage: formData.village
        });
    },
    choiceOther: function () {
        wx.navigateBack({
            delta: 1,
        });
    },
    choiceBuilding: function (event) {
        var building = comm_1.default.getTargetValue(event, 'building');
        var formData = wx.getStorageSync('_placeForm');
        wx.setStorageSync('_placeForm', __assign({}, formData, { building: building }));
        wx.redirectTo({
            url: '/pages/apply/apply-place/apply-place',
        });
        wx.navigateBack({
            delta: 2,
        });
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '楼栋'
        });
        this.getVillage();
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hvaXNlLWJ1aWxkaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiY2hvaXNlLWJ1aWxkaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFDQSw0Q0FBdUM7QUFFdkMsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBRUosWUFBWSxFQUFFLEVBQUU7UUFFaEIsWUFBWSxFQUFFLENBQUM7Z0JBQ2IsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsUUFBUSxFQUFFLEtBQUs7YUFDaEI7WUFDRDtnQkFDRSxFQUFFLEVBQUUsQ0FBQztnQkFDTCxRQUFRLEVBQUUsS0FBSzthQUNoQjtZQUNEO2dCQUNFLEVBQUUsRUFBRSxDQUFDO2dCQUNMLFFBQVEsRUFBRSxLQUFLO2FBQ2hCO1NBQ0E7S0FDRjtJQUVELFVBQVU7UUFDUixJQUFNLFFBQVEsR0FBRyxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxPQUFPLENBQUM7WUFDWCxZQUFZLEVBQUUsUUFBUSxDQUFDLE9BQU87U0FDL0IsQ0FBQyxDQUFBO0lBRUosQ0FBQztJQUVELFdBQVc7UUFDVCxFQUFFLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSyxFQUFFLENBQUM7U0FDVCxDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsY0FBYyxZQUFDLEtBQVU7UUFDdkIsSUFBTSxRQUFRLEdBQUcsY0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDeEQsSUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNqRCxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksZUFBTyxRQUFRLEVBQUssRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFLEVBQUcsQ0FBQztRQUM1RSxFQUFFLENBQUMsVUFBVSxDQUFDO1lBQ1osR0FBRyxFQUFFLHNDQUFzQztTQUM1QyxDQUFDLENBQUE7UUFDRixFQUFFLENBQUMsWUFBWSxDQUFDO1lBQ2QsS0FBSyxFQUFFLENBQUM7U0FDVCxDQUFDLENBQUE7SUFDSixDQUFDO0lBSUQsTUFBTSxFQUFFLFVBQVUsUUFBUTtRQUV4QixFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDdkIsS0FBSyxFQUFFLElBQUk7U0FDWixDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7SUFDbkIsQ0FBQztJQUtELE9BQU8sRUFBRTtJQUVULENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELFFBQVEsRUFBRTtJQUVWLENBQUM7SUFLRCxpQkFBaUIsRUFBRTtJQUVuQixDQUFDO0lBS0QsYUFBYSxFQUFFO0lBRWYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyDnlLPor7flvIDpl6jmnYPpmZAt6YCJ5oup5qW85qCL5L+h5oGv6aG16Z2iXHJcbmltcG9ydCBDb21tIGZyb20gJy4uLy4uLy4uL3V0aWxzL2NvbW0nO1xyXG5cclxuUGFnZSh7XHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdoueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuICAgIC8vIOW9k+WJjeWwj+WMulxyXG4gICAgbG9jYWxWaWxsYWdlOiBcIlwiLFxyXG4gICAgLy8g5bCP5Yy655qE5qW85qCL5pWw5o2uXHJcbiAgICBidWlsZGluZ0RhdGE6IFt7XHJcbiAgICAgIGlkOiAxLFxyXG4gICAgICBidWlsZGluZzogXCLmpbzmoIsxXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGlkOiAyLFxyXG4gICAgICBidWlsZGluZzogXCLmpbzmoIsyXCJcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGlkOiAzLFxyXG4gICAgICBidWlsZGluZzogXCLmpbzmoIszXCJcclxuICAgIH1cclxuICAgIF1cclxuICB9LFxyXG4gIC8vIOiOt+WPluW9k+WJjeWwj+WMulxyXG4gIGdldFZpbGxhZ2UoKSB7XHJcbiAgICBjb25zdCBmb3JtRGF0YSA9IHd4LmdldFN0b3JhZ2VTeW5jKCdfcGxhY2VGb3JtJyk7XHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICBsb2NhbFZpbGxhZ2U6IGZvcm1EYXRhLnZpbGxhZ2VcclxuICAgIH0pXHJcbiAgIFxyXG4gIH0sXHJcbiAgLy8g5YiH5o2i5bCP5Yy677yM5Zue5Yiw6YCJ5oup5bCP5Yy66aG16Z2iXHJcbiAgY2hvaWNlT3RoZXIoKSB7XHJcbiAgICB3eC5uYXZpZ2F0ZUJhY2soe1xyXG4gICAgICBkZWx0YTogMSxcclxuICAgIH0pXHJcbiAgfSxcclxuICAvLyDpgInmi6nmpbzmoIvmlbDmja7vvIzov5Tlm57nlLPor7fmnYPpmZDpobXpnaJcclxuICBjaG9pY2VCdWlsZGluZyhldmVudDogYW55KSB7XHJcbiAgICBjb25zdCBidWlsZGluZyA9IENvbW0uZ2V0VGFyZ2V0VmFsdWUoZXZlbnQsICdidWlsZGluZycpO1xyXG4gICAgY29uc3QgZm9ybURhdGEgPSB3eC5nZXRTdG9yYWdlU3luYygnX3BsYWNlRm9ybScpO1xyXG4gICAgd3guc2V0U3RvcmFnZVN5bmMoJ19wbGFjZUZvcm0nLCB7IC4uLmZvcm1EYXRhLCAuLi57IGJ1aWxkaW5nOiBidWlsZGluZyB9IH0pO1xyXG4gICAgd3gucmVkaXJlY3RUbyh7XHJcbiAgICAgIHVybDogJy9wYWdlcy9hcHBseS9hcHBseS1wbGFjZS9hcHBseS1wbGFjZScsXHJcbiAgICB9KVxyXG4gICAgd3gubmF2aWdhdGVCYWNrKHtcclxuICAgICAgZGVsdGE6IDIsXHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLliqDovb1cclxuICAgKi9cclxuICBvbkxvYWQ6IGZ1bmN0aW9uIChfb3B0aW9ucykge1xyXG4gICAgLy8g5L+u5pS56aG16Z2i5qCH6aKYXHJcbiAgICB3eC5zZXROYXZpZ2F0aW9uQmFyVGl0bGUoe1xyXG4gICAgICB0aXRsZTogJ+alvOagiydcclxuICAgIH0pXHJcbiAgICB0aGlzLmdldFZpbGxhZ2UoKVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yid5qyh5riy5p+T5a6M5oiQXHJcbiAgICovXHJcbiAgb25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouaYvuekulxyXG4gICAqL1xyXG4gIG9uU2hvdzogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWNuOi9vVxyXG4gICAqL1xyXG4gIG9uVW5sb2FkOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouebuOWFs+S6i+S7tuWkhOeQhuWHveaVsC0t55uR5ZCs55So5oi35LiL5ouJ5Yqo5L2cXHJcbiAgICovXHJcbiAgb25QdWxsRG93blJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlKjmiLfngrnlh7vlj7PkuIrop5LliIbkuqtcclxuICAgKi9cclxuICBvblNoYXJlQXBwTWVzc2FnZTogZnVuY3Rpb24gKCk6IGFueSB7XHJcblxyXG4gIH1cclxufSkiXX0=