//人脸录入页面
import navigate from '../../../utils/navigate';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    /*按钮*/
    btn_disabled: true,
  },
  // 接受协议单选框和按钮绑定
  bindAgreeChange: function (e: any) {
    this.setData({
      isAgree: e.detail.value.length,
    })
    if (e.detail.value.length == 1) {
      this.setData({
        btn_disabled: false,
      })
    } else {
      this.setData({
        btn_disabled: true
      })
    }
  },
  // 去人脸录入页面
  submit() {
    navigate.goToPanel(
      "/pages/apply/photograph/photograph"
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '人脸识别'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})