"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Page({
    data: {
        btn_disabled: true,
    },
    bindAgreeChange: function (e) {
        this.setData({
            isAgree: e.detail.value.length,
        });
        if (e.detail.value.length == 1) {
            this.setData({
                btn_disabled: false,
            });
        }
        else {
            this.setData({
                btn_disabled: true
            });
        }
    },
    submit: function () {
        navigate_1.default.goToPanel("/pages/apply/photograph/photograph");
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '人脸识别'
        });
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjZS1pbnB1dC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImZhY2UtaW5wdXQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxvREFBK0M7QUFDL0MsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBRUosWUFBWSxFQUFFLElBQUk7S0FDbkI7SUFFRCxlQUFlLEVBQUUsVUFBVSxDQUFNO1FBQy9CLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDWCxPQUFPLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsTUFBTTtTQUMvQixDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxZQUFZLEVBQUUsS0FBSzthQUNwQixDQUFDLENBQUE7U0FDSDthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxZQUFZLEVBQUUsSUFBSTthQUNuQixDQUFDLENBQUE7U0FDSDtJQUNILENBQUM7SUFFRCxNQUFNO1FBQ0osa0JBQVEsQ0FBQyxTQUFTLENBQ2hCLG9DQUFvQyxDQUNyQyxDQUFBO0lBQ0gsQ0FBQztJQUlELE1BQU0sRUFBRSxVQUFVLFFBQVE7UUFFeEIsRUFBRSxDQUFDLHFCQUFxQixDQUFDO1lBQ3ZCLEtBQUssRUFBRSxNQUFNO1NBQ2QsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUtELE9BQU8sRUFBRTtJQUVULENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELFFBQVEsRUFBRTtJQUVWLENBQUM7SUFLRCxpQkFBaUIsRUFBRTtJQUVuQixDQUFDO0lBS0QsYUFBYSxFQUFFO0lBRWYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvL+S6uuiEuOW9leWFpemhtemdolxyXG5pbXBvcnQgbmF2aWdhdGUgZnJvbSAnLi4vLi4vLi4vdXRpbHMvbmF2aWdhdGUnO1xyXG5QYWdlKHtcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i55qE5Yid5aeL5pWw5o2uXHJcbiAgICovXHJcbiAgZGF0YToge1xyXG4gICAgLyrmjInpkq4qL1xyXG4gICAgYnRuX2Rpc2FibGVkOiB0cnVlLFxyXG4gIH0sXHJcbiAgLy8g5o6l5Y+X5Y2P6K6u5Y2V6YCJ5qGG5ZKM5oyJ6ZKu57uR5a6aXHJcbiAgYmluZEFncmVlQ2hhbmdlOiBmdW5jdGlvbiAoZTogYW55KSB7XHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICBpc0FncmVlOiBlLmRldGFpbC52YWx1ZS5sZW5ndGgsXHJcbiAgICB9KVxyXG4gICAgaWYgKGUuZGV0YWlsLnZhbHVlLmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgYnRuX2Rpc2FibGVkOiBmYWxzZSxcclxuICAgICAgfSlcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgYnRuX2Rpc2FibGVkOiB0cnVlXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfSxcclxuICAvLyDljrvkurrohLjlvZXlhaXpobXpnaJcclxuICBzdWJtaXQoKSB7XHJcbiAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoXHJcbiAgICAgIFwiL3BhZ2VzL2FwcGx5L3Bob3RvZ3JhcGgvcGhvdG9ncmFwaFwiXHJcbiAgICApXHJcbiAgfSxcclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWKoOi9vVxyXG4gICAqL1xyXG4gIG9uTG9hZDogZnVuY3Rpb24gKF9vcHRpb25zKSB7XHJcbiAgICAvLyDkv67mlLnpobXpnaLmoIfpophcclxuICAgIHd4LnNldE5hdmlnYXRpb25CYXJUaXRsZSh7XHJcbiAgICAgIHRpdGxlOiAn5Lq66IS46K+G5YirJ1xyXG4gICAgfSlcclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWIneasoea4suafk+WujOaIkFxyXG4gICAqL1xyXG4gIG9uUmVhZHk6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLmmL7npLpcclxuICAgKi9cclxuICBvblNob3c6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLpmpDol49cclxuICAgKi9cclxuICBvbkhpZGU6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLljbjovb1cclxuICAgKi9cclxuICBvblVubG9hZDogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnm7jlhbPkuovku7blpITnkIblh73mlbAtLeebkeWQrOeUqOaIt+S4i+aLieWKqOS9nFxyXG4gICAqL1xyXG4gIG9uUHVsbERvd25SZWZyZXNoOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouS4iuaLieinpuW6leS6i+S7tueahOWkhOeQhuWHveaVsFxyXG4gICAqL1xyXG4gIG9uUmVhY2hCb3R0b206IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55So5oi354K55Ye75Y+z5LiK6KeS5YiG5LqrXHJcbiAgICovXHJcbiAgb25TaGFyZUFwcE1lc3NhZ2U6IGZ1bmN0aW9uICgpOiBhbnkge1xyXG5cclxuICB9XHJcbn0pIl19