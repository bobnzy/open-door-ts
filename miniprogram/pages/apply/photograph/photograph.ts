// 人脸识别——拍照页面
import navigate from '../../../utils/navigate';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: true,
    src: ""
  },
  takePhoto() {
    const ctx = wx.createCameraContext()
    ctx.takePhoto({
      quality: 'high',
      success: (res) => {
        console.log(res.tempImagePath)
        wx.uploadFile({
          url: 'http://172.16.3.247:8080/api/area/updateImg', 
          filePath: res.tempImagePath,
          name: 'file',
          header: {
            "Content-Type": "multipart/form-data",
            'accept': 'application/json'
          },
          formData: {  },
          success:(res:any)=> {
            console.log(res)
          },
          fail:(res:any) =>{
            console.log(res)
          }
        })
        this.setData({
          src: res.tempImagePath
        })
      }
    })
    this.setData(
      {
        isShow: !this.data.isShow
      }
    )
  },
  reTakePhoto() {
    this.setData(
      {
        isShow: !this.data.isShow,
        src: ""
      }
    )
  },
  definePhoto() {
    navigate.goToPanel(
      '/pages/apply/apply-info/apply-info'
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  }
})