"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Page({
    data: {
        isShow: true,
        src: ""
    },
    takePhoto: function () {
        var _this = this;
        var ctx = wx.createCameraContext();
        ctx.takePhoto({
            quality: 'high',
            success: function (res) {
                console.log(res.tempImagePath);
                wx.uploadFile({
                    url: 'http://172.16.3.247:8080/api/area/updateImg',
                    filePath: res.tempImagePath,
                    name: 'file',
                    header: {
                        "Content-Type": "multipart/form-data",
                        'accept': 'application/json'
                    },
                    formData: {},
                    success: function (res) {
                        console.log(res);
                    },
                    fail: function (res) {
                        console.log(res);
                    }
                });
                _this.setData({
                    src: res.tempImagePath
                });
            }
        });
        this.setData({
            isShow: !this.data.isShow
        });
    },
    reTakePhoto: function () {
        this.setData({
            isShow: !this.data.isShow,
            src: ""
        });
    },
    definePhoto: function () {
        navigate_1.default.goToPanel('/pages/apply/apply-info/apply-info');
    },
    onLoad: function (_options) {
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGhvdG9ncmFwaC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInBob3RvZ3JhcGgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxvREFBK0M7QUFDL0MsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBQ0osTUFBTSxFQUFFLElBQUk7UUFDWixHQUFHLEVBQUUsRUFBRTtLQUNSO0lBQ0QsU0FBUztRQUFULGlCQWdDQztRQS9CQyxJQUFNLEdBQUcsR0FBRyxFQUFFLENBQUMsbUJBQW1CLEVBQUUsQ0FBQTtRQUNwQyxHQUFHLENBQUMsU0FBUyxDQUFDO1lBQ1osT0FBTyxFQUFFLE1BQU07WUFDZixPQUFPLEVBQUUsVUFBQyxHQUFHO2dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFBO2dCQUM5QixFQUFFLENBQUMsVUFBVSxDQUFDO29CQUNaLEdBQUcsRUFBRSw2Q0FBNkM7b0JBQ2xELFFBQVEsRUFBRSxHQUFHLENBQUMsYUFBYTtvQkFDM0IsSUFBSSxFQUFFLE1BQU07b0JBQ1osTUFBTSxFQUFFO3dCQUNOLGNBQWMsRUFBRSxxQkFBcUI7d0JBQ3JDLFFBQVEsRUFBRSxrQkFBa0I7cUJBQzdCO29CQUNELFFBQVEsRUFBRSxFQUFJO29CQUNkLE9BQU8sRUFBQyxVQUFDLEdBQU87d0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtvQkFDbEIsQ0FBQztvQkFDRCxJQUFJLEVBQUMsVUFBQyxHQUFPO3dCQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUE7b0JBQ2xCLENBQUM7aUJBQ0YsQ0FBQyxDQUFBO2dCQUNGLEtBQUksQ0FBQyxPQUFPLENBQUM7b0JBQ1gsR0FBRyxFQUFFLEdBQUcsQ0FBQyxhQUFhO2lCQUN2QixDQUFDLENBQUE7WUFDSixDQUFDO1NBQ0YsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLE9BQU8sQ0FDVjtZQUNFLE1BQU0sRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTtTQUMxQixDQUNGLENBQUE7SUFDSCxDQUFDO0lBQ0QsV0FBVztRQUNULElBQUksQ0FBQyxPQUFPLENBQ1Y7WUFDRSxNQUFNLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07WUFDekIsR0FBRyxFQUFFLEVBQUU7U0FDUixDQUNGLENBQUE7SUFDSCxDQUFDO0lBQ0QsV0FBVztRQUNULGtCQUFRLENBQUMsU0FBUyxDQUNoQixvQ0FBb0MsQ0FDckMsQ0FBQTtJQUNILENBQUM7SUFJRCxNQUFNLEVBQUUsVUFBVSxRQUFRO0lBRTFCLENBQUM7SUFLRCxPQUFPLEVBQUU7SUFFVCxDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELE1BQU0sRUFBRTtJQUVSLENBQUM7SUFLRCxRQUFRLEVBQUU7SUFFVixDQUFDO0lBS0QsaUJBQWlCLEVBQUU7SUFFbkIsQ0FBQztJQUtELGFBQWEsRUFBRTtJQUVmLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyDkurrohLjor4bliKvigJTigJTmi43nhafpobXpnaJcclxuaW1wb3J0IG5hdmlnYXRlIGZyb20gJy4uLy4uLy4uL3V0aWxzL25hdmlnYXRlJztcclxuUGFnZSh7XHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdoueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuICAgIGlzU2hvdzogdHJ1ZSxcclxuICAgIHNyYzogXCJcIlxyXG4gIH0sXHJcbiAgdGFrZVBob3RvKCkge1xyXG4gICAgY29uc3QgY3R4ID0gd3guY3JlYXRlQ2FtZXJhQ29udGV4dCgpXHJcbiAgICBjdHgudGFrZVBob3RvKHtcclxuICAgICAgcXVhbGl0eTogJ2hpZ2gnLFxyXG4gICAgICBzdWNjZXNzOiAocmVzKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzLnRlbXBJbWFnZVBhdGgpXHJcbiAgICAgICAgd3gudXBsb2FkRmlsZSh7XHJcbiAgICAgICAgICB1cmw6ICdodHRwOi8vMTcyLjE2LjMuMjQ3OjgwODAvYXBpL2FyZWEvdXBkYXRlSW1nJywgXHJcbiAgICAgICAgICBmaWxlUGF0aDogcmVzLnRlbXBJbWFnZVBhdGgsXHJcbiAgICAgICAgICBuYW1lOiAnZmlsZScsXHJcbiAgICAgICAgICBoZWFkZXI6IHtcclxuICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJtdWx0aXBhcnQvZm9ybS1kYXRhXCIsXHJcbiAgICAgICAgICAgICdhY2NlcHQnOiAnYXBwbGljYXRpb24vanNvbidcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBmb3JtRGF0YTogeyAgfSxcclxuICAgICAgICAgIHN1Y2Nlc3M6KHJlczphbnkpPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMpXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZmFpbDoocmVzOmFueSkgPT57XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcylcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgICBzcmM6IHJlcy50ZW1wSW1hZ2VQYXRoXHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgfSlcclxuICAgIHRoaXMuc2V0RGF0YShcclxuICAgICAge1xyXG4gICAgICAgIGlzU2hvdzogIXRoaXMuZGF0YS5pc1Nob3dcclxuICAgICAgfVxyXG4gICAgKVxyXG4gIH0sXHJcbiAgcmVUYWtlUGhvdG8oKSB7XHJcbiAgICB0aGlzLnNldERhdGEoXHJcbiAgICAgIHtcclxuICAgICAgICBpc1Nob3c6ICF0aGlzLmRhdGEuaXNTaG93LFxyXG4gICAgICAgIHNyYzogXCJcIlxyXG4gICAgICB9XHJcbiAgICApXHJcbiAgfSxcclxuICBkZWZpbmVQaG90bygpIHtcclxuICAgIG5hdmlnYXRlLmdvVG9QYW5lbChcclxuICAgICAgJy9wYWdlcy9hcHBseS9hcHBseS1pbmZvL2FwcGx5LWluZm8nXHJcbiAgICApXHJcbiAgfSxcclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWKoOi9vVxyXG4gICAqL1xyXG4gIG9uTG9hZDogZnVuY3Rpb24gKF9vcHRpb25zKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yid5qyh5riy5p+T5a6M5oiQXHJcbiAgICovXHJcbiAgb25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouaYvuekulxyXG4gICAqL1xyXG4gIG9uU2hvdzogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWNuOi9vVxyXG4gICAqL1xyXG4gIG9uVW5sb2FkOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouebuOWFs+S6i+S7tuWkhOeQhuWHveaVsC0t55uR5ZCs55So5oi35LiL5ouJ5Yqo5L2cXHJcbiAgICovXHJcbiAgb25QdWxsRG93blJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9XHJcbn0pIl19