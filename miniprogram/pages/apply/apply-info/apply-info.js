"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var navigate_1 = require("../../../utils/navigate");
Page({
    data: {
        btnStatus: false,
        userTypeArray: [{
                id: 0,
                value: '业主'
            }, {
                id: 1,
                value: '家属'
            }, {
                id: 2,
                value: '租客'
            }, {
                id: 3,
                value: '访客'
            }],
        userTypeIndex: 0,
        idTypeArray: [{
                id: 1,
                value: '身份证号'
            }, {
                id: 2,
                value: '港澳台'
            }, {
                id: 3,
                value: '护照'
            }, {
                id: 4,
                value: '其他'
            }],
        idTypeIndex: 0,
    },
    bindPickerChangeUser: function (e) {
        this.setData({
            userTypeIndex: e.detail.value
        });
    },
    bindPickerChangeId: function (e) {
        this.setData({
            idTypeIndex: e.detail.value
        });
    },
    changeInput: function (e) {
        if (e.detail.value.length > 1) {
            this.setData({
                btnStatus: true
            });
        }
        else {
            this.setData({
                btnStatus: false
            });
        }
    },
    formSubmit: function (e) {
        var formData = e.detail.value;
        formData.userType = this.data.userTypeArray[formData.userType].value;
        formData.idType = this.data.idTypeArray[formData.idType].value;
        console.log(formData);
        navigate_1.default.goToPanel('/pages/apply/apply-success/apply-success');
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '申请开门权限'
        });
    },
    onReady: function () {
    },
    onShow: function () {
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwbHktaW5mby5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcGx5LWluZm8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFDQSxvREFBK0M7QUFDL0MsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBRUosU0FBUyxFQUFFLEtBQUs7UUFFaEIsYUFBYSxFQUFFLENBQUM7Z0JBQ2QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLElBQUk7YUFDWixFQUFFO2dCQUNELEVBQUUsRUFBRSxDQUFDO2dCQUNMLEtBQUssRUFBRSxJQUFJO2FBQ1osRUFBRTtnQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDTCxLQUFLLEVBQUUsSUFBSTthQUNaLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLElBQUk7YUFDWixDQUFDO1FBRUYsYUFBYSxFQUFFLENBQUM7UUFFaEIsV0FBVyxFQUFFLENBQUM7Z0JBQ1osRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLE1BQU07YUFDZCxFQUFFO2dCQUNELEVBQUUsRUFBRSxDQUFDO2dCQUNMLEtBQUssRUFBRSxLQUFLO2FBQ2IsRUFBRTtnQkFDRCxFQUFFLEVBQUUsQ0FBQztnQkFDTCxLQUFLLEVBQUUsSUFBSTthQUNaLEVBQUU7Z0JBQ0QsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsS0FBSyxFQUFFLElBQUk7YUFDWixDQUFDO1FBQ0YsV0FBVyxFQUFFLENBQUM7S0FDZjtJQUVELG9CQUFvQixFQUFFLFVBQVUsQ0FBTTtRQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1gsYUFBYSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSztTQUM5QixDQUFDLENBQUE7SUFDSixDQUFDO0lBRUQsa0JBQWtCLEVBQUUsVUFBVSxDQUFNO1FBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDWCxXQUFXLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLO1NBQzVCLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCxXQUFXLFlBQUMsQ0FBTTtRQUVoQixJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxTQUFTLEVBQUUsSUFBSTthQUNoQixDQUFDLENBQUE7U0FDSDthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxTQUFTLEVBQUUsS0FBSzthQUNqQixDQUFDLENBQUE7U0FDSDtJQUNILENBQUM7SUFFRCxVQUFVLFlBQUMsQ0FBTTtRQUNmLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFBO1FBQzdCLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQTtRQUNwRSxRQUFRLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUE7UUFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUNyQixrQkFBUSxDQUFDLFNBQVMsQ0FDaEIsMENBQTBDLENBQzNDLENBQUE7SUFDSCxDQUFDO0lBSUQsTUFBTSxFQUFFLFVBQVUsUUFBUTtRQUV4QixFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDdkIsS0FBSyxFQUFFLFFBQVE7U0FDaEIsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUlELE9BQU8sRUFBRTtJQUVULENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsTUFBTSxFQUFFO0lBRVIsQ0FBQztJQUtELFFBQVEsRUFBRTtJQUVWLENBQUM7SUFLRCxpQkFBaUIsRUFBRTtJQUVuQixDQUFDO0lBS0QsYUFBYSxFQUFFO0lBRWYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvLyDnlLPor7flvIDpl6jmnYPpmZDpobXpnaIx4oCU4oCU5Zyw54K555qE5L+h5oGvXHJcbmltcG9ydCBuYXZpZ2F0ZSBmcm9tICcuLi8uLi8uLi91dGlscy9uYXZpZ2F0ZSc7XHJcblBhZ2Uoe1xyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLnmoTliJ3lp4vmlbDmja5cclxuICAgKi9cclxuICBkYXRhOiB7XHJcbiAgICAvLyDmjqfliLbmjInpkq7mmK/lkKbnpoHnlKhcclxuICAgIGJ0blN0YXR1czogZmFsc2UsXHJcbiAgICAvLyDouqvku73nsbvlnovnmoTpgInpoblcclxuICAgIHVzZXJUeXBlQXJyYXk6IFt7XHJcbiAgICAgIGlkOiAwLFxyXG4gICAgICB2YWx1ZTogJ+S4muS4uydcclxuICAgIH0sIHtcclxuICAgICAgaWQ6IDEsXHJcbiAgICAgIHZhbHVlOiAn5a625bGeJ1xyXG4gICAgfSwge1xyXG4gICAgICBpZDogMixcclxuICAgICAgdmFsdWU6ICfnp5/lrqInXHJcbiAgICB9LCB7XHJcbiAgICAgIGlkOiAzLFxyXG4gICAgICB2YWx1ZTogJ+iuv+WuoidcclxuICAgIH1dLFxyXG4gICAgLy8g6Lqr5Lu957G756S66buY6K6k5pi+56S657Si5byV5Li6MOeahOmAiemhuVxyXG4gICAgdXNlclR5cGVJbmRleDogMCxcclxuICAgIC8vIOivgeS7tuexu+Wei+eahOmAiemhuVxyXG4gICAgaWRUeXBlQXJyYXk6IFt7XHJcbiAgICAgIGlkOiAxLFxyXG4gICAgICB2YWx1ZTogJ+i6q+S7veivgeWPtydcclxuICAgIH0sIHtcclxuICAgICAgaWQ6IDIsXHJcbiAgICAgIHZhbHVlOiAn5riv5r6z5Y+wJ1xyXG4gICAgfSwge1xyXG4gICAgICBpZDogMyxcclxuICAgICAgdmFsdWU6ICfmiqTnhacnXHJcbiAgICB9LCB7XHJcbiAgICAgIGlkOiA0LFxyXG4gICAgICB2YWx1ZTogJ+WFtuS7lidcclxuICAgIH1dLFxyXG4gICAgaWRUeXBlSW5kZXg6IDAsXHJcbiAgfSxcclxuICAvLyDouqvku73nsbvlnotQaWNrZXLlgLzmlLnlj5jkuovku7ZcclxuICBiaW5kUGlja2VyQ2hhbmdlVXNlcjogZnVuY3Rpb24gKGU6IGFueSkge1xyXG4gICAgdGhpcy5zZXREYXRhKHtcclxuICAgICAgdXNlclR5cGVJbmRleDogZS5kZXRhaWwudmFsdWVcclxuICAgIH0pXHJcbiAgfSxcclxuICAvLyDor4Hku7bnsbvlnotQaWNrZXLlgLzmlLnlj5jkuovku7ZcclxuICBiaW5kUGlja2VyQ2hhbmdlSWQ6IGZ1bmN0aW9uIChlOiBhbnkpIHtcclxuICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgIGlkVHlwZUluZGV4OiBlLmRldGFpbC52YWx1ZVxyXG4gICAgfSlcclxuICB9LFxyXG4gIC8vIOi+k+WFpeahhuWunuaXtuebkeWQrFxyXG4gIGNoYW5nZUlucHV0KGU6IGFueSkge1xyXG4gICAgLy/pgJrov4fliKTmlq3ooajljZXlkITmnaHmlbDmja7mnInlhoXlrrnvvIzmnaXmjqfliLbmjInpkq7npoHnlKjnirbmgIFcclxuICAgIGlmIChlLmRldGFpbC52YWx1ZS5sZW5ndGggPiAxKSB7XHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgYnRuU3RhdHVzOiB0cnVlXHJcbiAgICAgIH0pXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgIGJ0blN0YXR1czogZmFsc2VcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9LFxyXG4gIC8vIOaPkOS6pOiupOivgeaMiemSrlxyXG4gIGZvcm1TdWJtaXQoZTogYW55KSB7XHJcbiAgICBsZXQgZm9ybURhdGEgPSBlLmRldGFpbC52YWx1ZVxyXG4gICAgZm9ybURhdGEudXNlclR5cGUgPSB0aGlzLmRhdGEudXNlclR5cGVBcnJheVtmb3JtRGF0YS51c2VyVHlwZV0udmFsdWVcclxuICAgIGZvcm1EYXRhLmlkVHlwZSA9IHRoaXMuZGF0YS5pZFR5cGVBcnJheVtmb3JtRGF0YS5pZFR5cGVdLnZhbHVlXHJcbiAgICBjb25zb2xlLmxvZyhmb3JtRGF0YSlcclxuICAgIG5hdmlnYXRlLmdvVG9QYW5lbChcclxuICAgICAgJy9wYWdlcy9hcHBseS9hcHBseS1zdWNjZXNzL2FwcGx5LXN1Y2Nlc3MnXHJcbiAgICApXHJcbiAgfSxcclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWKoOi9vVxyXG4gICAqL1xyXG4gIG9uTG9hZDogZnVuY3Rpb24gKF9vcHRpb25zKSB7XHJcbiAgICAvLyDkv67mlLnpobXpnaLmoIfpophcclxuICAgIHd4LnNldE5hdmlnYXRpb25CYXJUaXRsZSh7XHJcbiAgICAgIHRpdGxlOiAn55Sz6K+35byA6Zeo5p2D6ZmQJ1xyXG4gICAgfSlcclxuICB9LFxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yid5qyh5riy5p+T5a6M5oiQXHJcbiAgICovXHJcbiAgb25SZWFkeTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouaYvuekulxyXG4gICAqL1xyXG4gIG9uU2hvdzogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdoumakOiXj1xyXG4gICAqL1xyXG4gIG9uSGlkZTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWNuOi9vVxyXG4gICAqL1xyXG4gIG9uVW5sb2FkOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdouebuOWFs+S6i+S7tuWkhOeQhuWHveaVsC0t55uR5ZCs55So5oi35LiL5ouJ5Yqo5L2cXHJcbiAgICovXHJcbiAgb25QdWxsRG93blJlZnJlc2g6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i5LiK5ouJ6Kem5bqV5LqL5Lu255qE5aSE55CG5Ye95pWwXHJcbiAgICovXHJcbiAgb25SZWFjaEJvdHRvbTogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDnlKjmiLfngrnlh7vlj7PkuIrop5LliIbkuqtcclxuICAgKi9cclxuICBvblNoYXJlQXBwTWVzc2FnZTogZnVuY3Rpb24gKCk6IGFueSB7XHJcblxyXG4gIH1cclxufSkiXX0=