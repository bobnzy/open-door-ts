// 申请开门权限页面1——地点的信息
import navigate from '../../../utils/navigate';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 控制按钮是否禁用
    btnStatus: false,
    // 身份类型的选项
    userTypeArray: [{
      id: 0,
      value: '业主'
    }, {
      id: 1,
      value: '家属'
    }, {
      id: 2,
      value: '租客'
    }, {
      id: 3,
      value: '访客'
    }],
    // 身份类示默认显示索引为0的选项
    userTypeIndex: 0,
    // 证件类型的选项
    idTypeArray: [{
      id: 1,
      value: '身份证号'
    }, {
      id: 2,
      value: '港澳台'
    }, {
      id: 3,
      value: '护照'
    }, {
      id: 4,
      value: '其他'
    }],
    idTypeIndex: 0,
  },
  // 身份类型Picker值改变事件
  bindPickerChangeUser: function (e: any) {
    this.setData({
      userTypeIndex: e.detail.value
    })
  },
  // 证件类型Picker值改变事件
  bindPickerChangeId: function (e: any) {
    this.setData({
      idTypeIndex: e.detail.value
    })
  },
  // 输入框实时监听
  changeInput(e: any) {
    //通过判断表单各条数据有内容，来控制按钮禁用状态
    if (e.detail.value.length > 1) {
      this.setData({
        btnStatus: true
      })
    } else {
      this.setData({
        btnStatus: false
      })
    }
  },
  // 提交认证按钮
  formSubmit(e: any) {
    let formData = e.detail.value
    formData.userType = this.data.userTypeArray[formData.userType].value
    formData.idType = this.data.idTypeArray[formData.idType].value
    console.log(formData)
    navigate.goToPanel(
      '/pages/apply/apply-success/apply-success'
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '申请开门权限'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})