"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var comm_1 = require("../../../utils/comm");
var navigate_1 = require("../../../utils/navigate");
Page({
    data: {
        btnStatus: false,
        formTpyeList: [{
                id: 1,
                name: "city",
                title: "城市",
                text: "选择城市",
            },
            {
                id: 2,
                name: "village",
                title: "小区",
                text: "选择小区",
            },
            {
                id: 3,
                name: "building",
                title: "楼栋",
                text: "选择楼栋",
            },
            {
                id: 4,
                name: "house",
                title: "门牌号",
                text: "请输入",
            },
        ],
        formData: {
            city: '',
            village: '',
            building: '',
            house: ''
        }
    },
    getStorageData: function () {
        var formData = wx.getStorageSync('_placeForm');
        console.log(formData);
        this.setData({
            formData: formData
        });
    },
    goChoice: function () {
        wx.showToast({
            title: '定位中',
            icon: 'loading',
            duration: 1000,
            complete: function () {
                navigate_1.default.goToPanel('/pages/apply/choice-place/choice-place');
            }
        });
    },
    goChoiceVillage: function () {
        if (wx.getStorageSync('_placeForm').city) {
            navigate_1.default.goToPanel('/pages/apply/choice-place/choice-place');
        }
    },
    goChoiceBuilding: function () {
        if (wx.getStorageSync('_placeForm').village) {
            navigate_1.default.goToPanel('/pages/apply/choise-building/choise-building');
        }
    },
    changeInput: function (e) {
        var _a;
        var key = comm_1.default.getTargetValue(e, 'name');
        var obj = (_a = {},
            _a[key] = e.detail.value,
            _a);
        obj = Object.assign(this.data.formData, obj);
        this.setData({
            formData: obj
        });
        if (obj.city.length && obj.village.length && obj.building.length && obj.house.length) {
            this.setData({
                btnStatus: true
            });
        }
        else {
            this.setData({
                btnStatus: false
            });
        }
    },
    formSubmit: function (e) {
        console.log(e.detail.value);
        navigate_1.default.goToPanel('/pages/apply/face-input/face-input');
    },
    onLoad: function (_options) {
        wx.setNavigationBarTitle({
            title: '申请开门权限'
        });
        this.getStorageData();
    },
    onReady: function () {
    },
    onShow: function () {
        this.getStorageData();
    },
    onHide: function () {
    },
    onUnload: function () {
    },
    onPullDownRefresh: function () {
    },
    onReachBottom: function () {
    },
    onShareAppMessage: function () {
    }
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwbHktcGxhY2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJhcHBseS1wbGFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLDRDQUF1QztBQUN2QyxvREFBK0M7QUFFL0MsSUFBSSxDQUFDO0lBS0gsSUFBSSxFQUFFO1FBRUosU0FBUyxFQUFFLEtBQUs7UUFFaEIsWUFBWSxFQUFFLENBQUM7Z0JBQ2IsRUFBRSxFQUFFLENBQUM7Z0JBQ0wsSUFBSSxFQUFFLE1BQU07Z0JBQ1osS0FBSyxFQUFFLElBQUk7Z0JBQ1gsSUFBSSxFQUFFLE1BQU07YUFDYjtZQUNEO2dCQUNFLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxJQUFJO2dCQUNYLElBQUksRUFBRSxNQUFNO2FBQ2I7WUFDRDtnQkFDRSxFQUFFLEVBQUUsQ0FBQztnQkFDTCxJQUFJLEVBQUUsVUFBVTtnQkFDaEIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsSUFBSSxFQUFFLE1BQU07YUFDYjtZQUNEO2dCQUNFLEVBQUUsRUFBRSxDQUFDO2dCQUNMLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxLQUFLO2dCQUNaLElBQUksRUFBRSxLQUFLO2FBQ1o7U0FDQTtRQUVELFFBQVEsRUFBRTtZQUNSLElBQUksRUFBRSxFQUFFO1lBQ1IsT0FBTyxFQUFFLEVBQUU7WUFDWCxRQUFRLEVBQUUsRUFBRTtZQUNaLEtBQUssRUFBRSxFQUFFO1NBQ1Y7S0FDRjtJQUVELGNBQWM7UUFDWixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUVYLFFBQVEsRUFBRSxRQUFRO1NBQ25CLENBQUMsQ0FBQTtJQUdKLENBQUM7SUFFRCxRQUFRO1FBQ04sRUFBRSxDQUFDLFNBQVMsQ0FBQztZQUNYLEtBQUssRUFBRSxLQUFLO1lBQ1osSUFBSSxFQUFFLFNBQVM7WUFDZixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVE7Z0JBQ04sa0JBQVEsQ0FBQyxTQUFTLENBQ2hCLHdDQUF3QyxDQUN6QyxDQUFBO1lBQ0gsQ0FBQztTQUNGLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxFQUFFLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUksRUFBRTtZQUN4QyxrQkFBUSxDQUFDLFNBQVMsQ0FDaEIsd0NBQXdDLENBQ3pDLENBQUE7U0FDRjtJQUNILENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLEVBQUUsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxFQUFFO1lBQzNDLGtCQUFRLENBQUMsU0FBUyxDQUNoQiw4Q0FBOEMsQ0FDL0MsQ0FBQTtTQUNGO0lBQ0gsQ0FBQztJQUVELFdBQVcsWUFBQyxDQUFNOztRQUVoQixJQUFNLEdBQUcsR0FBRyxjQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUUzQyxJQUFJLEdBQUc7WUFDTCxHQUFDLEdBQUcsSUFBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUs7ZUFDdEIsQ0FBQTtRQUVELEdBQUcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDWCxRQUFRLEVBQUUsR0FBRztTQUNkLENBQUMsQ0FBQTtRQUVGLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDcEYsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxTQUFTLEVBQUUsSUFBSTthQUNoQixDQUFDLENBQUE7U0FDSDthQUFNO1lBQ0wsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDWCxTQUFTLEVBQUUsS0FBSzthQUNqQixDQUFDLENBQUE7U0FDSDtJQUNILENBQUM7SUFFRCxVQUFVLEVBQUUsVUFBVSxDQUFNO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUMzQixrQkFBUSxDQUFDLFNBQVMsQ0FDaEIsb0NBQW9DLENBQ3JDLENBQUE7SUFDSCxDQUFDO0lBSUQsTUFBTSxFQUFFLFVBQVUsUUFBUTtRQUV4QixFQUFFLENBQUMscUJBQXFCLENBQUM7WUFDdkIsS0FBSyxFQUFFLFFBQVE7U0FDaEIsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO0lBQ3ZCLENBQUM7SUFJRCxPQUFPLEVBQUU7SUFFVCxDQUFDO0lBS0QsTUFBTSxFQUFFO1FBQ04sSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO0lBQ3ZCLENBQUM7SUFLRCxNQUFNLEVBQUU7SUFFUixDQUFDO0lBS0QsUUFBUSxFQUFFO0lBRVYsQ0FBQztJQUtELGlCQUFpQixFQUFFO0lBRW5CLENBQUM7SUFLRCxhQUFhLEVBQUU7SUFFZixDQUFDO0lBS0QsaUJBQWlCLEVBQUU7SUFFbkIsQ0FBQztDQUNGLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8vIOeUs+ivt+W8gOmXqOadg+mZkOmhtemdojLigJTigJTouqvku73nmoTkv6Hmga9cclxuXHJcbmltcG9ydCBDb21tIGZyb20gJy4uLy4uLy4uL3V0aWxzL2NvbW0nO1xyXG5pbXBvcnQgbmF2aWdhdGUgZnJvbSAnLi4vLi4vLi4vdXRpbHMvbmF2aWdhdGUnO1xyXG5cclxuUGFnZSh7XHJcblxyXG4gIC8qKlxyXG4gICAqIOmhtemdoueahOWIneWni+aVsOaNrlxyXG4gICAqL1xyXG4gIGRhdGE6IHtcclxuICAgIC8vIOaOp+WItuaMiemSruaYr+WQpuemgeeUqFxyXG4gICAgYnRuU3RhdHVzOiBmYWxzZSxcclxuICAgIC8vIOihqOWNleWGheWuueWIl+ihqFxyXG4gICAgZm9ybVRweWVMaXN0OiBbe1xyXG4gICAgICBpZDogMSxcclxuICAgICAgbmFtZTogXCJjaXR5XCIsXHJcbiAgICAgIHRpdGxlOiBcIuWfjuW4glwiLFxyXG4gICAgICB0ZXh0OiBcIumAieaLqeWfjuW4glwiLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgaWQ6IDIsXHJcbiAgICAgIG5hbWU6IFwidmlsbGFnZVwiLFxyXG4gICAgICB0aXRsZTogXCLlsI/ljLpcIixcclxuICAgICAgdGV4dDogXCLpgInmi6nlsI/ljLpcIixcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIGlkOiAzLFxyXG4gICAgICBuYW1lOiBcImJ1aWxkaW5nXCIsXHJcbiAgICAgIHRpdGxlOiBcIualvOagi1wiLFxyXG4gICAgICB0ZXh0OiBcIumAieaLqealvOagi1wiLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgaWQ6IDQsXHJcbiAgICAgIG5hbWU6IFwiaG91c2VcIixcclxuICAgICAgdGl0bGU6IFwi6Zeo54mM5Y+3XCIsXHJcbiAgICAgIHRleHQ6IFwi6K+36L6T5YWlXCIsXHJcbiAgICB9LFxyXG4gICAgXSxcclxuICAgIC8vIOihqOWNleaVsOaNrlxyXG4gICAgZm9ybURhdGE6IHtcclxuICAgICAgY2l0eTogJycsXHJcbiAgICAgIHZpbGxhZ2U6ICcnLFxyXG4gICAgICBidWlsZGluZzogJycsXHJcbiAgICAgIGhvdXNlOiAnJ1xyXG4gICAgfVxyXG4gIH0sXHJcbiAgLy8g6I635Y+W5pys5Zyw57yT5a2Y5Lit55qE5Z+O5biC44CB5bCP5Yy644CB5qW85qCL5pWw5o2uXHJcbiAgZ2V0U3RvcmFnZURhdGEoKSB7XHJcbiAgICBsZXQgZm9ybURhdGEgPSB3eC5nZXRTdG9yYWdlU3luYygnX3BsYWNlRm9ybScpO1xyXG4gICAgY29uc29sZS5sb2coZm9ybURhdGEpXHJcbiAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAvLyBmb3JtRGF0YTogZm9ybURhdGEgPyBmb3JtRGF0YSA6IHRoaXMuZGF0YS5mb3JtRGF0YVxyXG4gICAgICBmb3JtRGF0YTogZm9ybURhdGFcclxuICAgIH0pXHJcblxyXG5cclxuICB9LFxyXG4gIC8vIOeCueWHu+WfjuW4gui+k+WFpeahhu+8jOi/m+WFpemAieaLqeWcsOeCuemhtemdolxyXG4gIGdvQ2hvaWNlKCkge1xyXG4gICAgd3guc2hvd1RvYXN0KHtcclxuICAgICAgdGl0bGU6ICflrprkvY3kuK0nLFxyXG4gICAgICBpY29uOiAnbG9hZGluZycsXHJcbiAgICAgIGR1cmF0aW9uOiAxMDAwLFxyXG4gICAgICBjb21wbGV0ZSgpIHtcclxuICAgICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoXHJcbiAgICAgICAgICAnL3BhZ2VzL2FwcGx5L2Nob2ljZS1wbGFjZS9jaG9pY2UtcGxhY2UnXHJcbiAgICAgICAgKVxyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gIH0sXHJcbiAgLy8g54K55Ye75bCP5Yy66L6T5YWl5qGG77yM6L+b5YWl6YCJ5oup5bCP5Yy66aG16Z2iXHJcbiAgZ29DaG9pY2VWaWxsYWdlKCkge1xyXG4gICAgaWYgKHd4LmdldFN0b3JhZ2VTeW5jKCdfcGxhY2VGb3JtJykuY2l0eSkge1xyXG4gICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoXHJcbiAgICAgICAgJy9wYWdlcy9hcHBseS9jaG9pY2UtcGxhY2UvY2hvaWNlLXBsYWNlJ1xyXG4gICAgICApXHJcbiAgICB9XHJcbiAgfSxcclxuICAvLyDngrnlh7vmpbzmoIvovpPlhaXmoYbvvIzov5vlhaXpgInmi6nmpbzmoIvpobXpnaJcclxuICBnb0Nob2ljZUJ1aWxkaW5nKCkge1xyXG4gICAgaWYgKHd4LmdldFN0b3JhZ2VTeW5jKCdfcGxhY2VGb3JtJykudmlsbGFnZSkge1xyXG4gICAgICBuYXZpZ2F0ZS5nb1RvUGFuZWwoXHJcbiAgICAgICAgJy9wYWdlcy9hcHBseS9jaG9pc2UtYnVpbGRpbmcvY2hvaXNlLWJ1aWxkaW5nJ1xyXG4gICAgICApXHJcbiAgICB9XHJcbiAgfSxcclxuICAvLyDovpPlhaXmoYblrp7ml7bnm5HlkKxcclxuICBjaGFuZ2VJbnB1dChlOiBhbnkpIHtcclxuICAgIC8vIOiOt+WPluWIsGZvcm3nmoRpdGVt55qEbmFtZeWAvO+8jOWvueW6lOWIsGZvcm1EYXRh6YeM55qE6ZSu5YC8XHJcbiAgICBjb25zdCBrZXkgPSBDb21tLmdldFRhcmdldFZhbHVlKGUsICduYW1lJyk7XHJcbiAgICAvLyDmoLnmja7ov5nkuKrplK7lgLzvvIznlJ/miJDkuIDkuKrmlrDnmoTlr7nosaFcclxuICAgIGxldCBvYmo6IGFueSA9IHtcclxuICAgICAgW2tleV06IGUuZGV0YWlsLnZhbHVlXHJcbiAgICB9XHJcbiAgICAvLy8g5ZCI5bm25Lik5Liq5a+56LGh77yM5bCG5YC86LWL5LqI6L+b5Y67XHJcbiAgICBvYmogPSBPYmplY3QuYXNzaWduKHRoaXMuZGF0YS5mb3JtRGF0YSwgb2JqKTtcclxuICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgIGZvcm1EYXRhOiBvYmpcclxuICAgIH0pXHJcbiAgICAvL+mAmui/h+WIpOaWreihqOWNleWQhOadoeaVsOaNruacieWGheWuue+8jOadpeaOp+WItuaMiemSruemgeeUqOeKtuaAgVxyXG4gICAgaWYgKG9iai5jaXR5Lmxlbmd0aCAmJiBvYmoudmlsbGFnZS5sZW5ndGggJiYgb2JqLmJ1aWxkaW5nLmxlbmd0aCAmJiBvYmouaG91c2UubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuc2V0RGF0YSh7XHJcbiAgICAgICAgYnRuU3RhdHVzOiB0cnVlXHJcbiAgICAgIH0pXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNldERhdGEoe1xyXG4gICAgICAgIGJ0blN0YXR1czogZmFsc2VcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9LFxyXG4gIC8vIOihqOWNleaPkOS6pOaMiemSrueCueWHu1xyXG4gIGZvcm1TdWJtaXQ6IGZ1bmN0aW9uIChlOiBhbnkpIHtcclxuICAgIGNvbnNvbGUubG9nKGUuZGV0YWlsLnZhbHVlKVxyXG4gICAgbmF2aWdhdGUuZ29Ub1BhbmVsKFxyXG4gICAgICAnL3BhZ2VzL2FwcGx5L2ZhY2UtaW5wdXQvZmFjZS1pbnB1dCdcclxuICAgIClcclxuICB9LFxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Yqg6L29XHJcbiAgICovXHJcbiAgb25Mb2FkOiBmdW5jdGlvbiAoX29wdGlvbnMpIHtcclxuICAgIC8vIOS/ruaUuemhtemdouagh+mimFxyXG4gICAgd3guc2V0TmF2aWdhdGlvbkJhclRpdGxlKHtcclxuICAgICAgdGl0bGU6ICfnlLPor7flvIDpl6jmnYPpmZAnXHJcbiAgICB9KVxyXG4gICAgdGhpcy5nZXRTdG9yYWdlRGF0YSgpXHJcbiAgfSxcclxuICAvKipcclxuICAgKiDnlJ/lkb3lkajmnJ/lh73mlbAtLeebkeWQrOmhtemdouWIneasoea4suafk+WujOaIkFxyXG4gICAqL1xyXG4gIG9uUmVhZHk6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog55Sf5ZG95ZGo5pyf5Ye95pWwLS3nm5HlkKzpobXpnaLmmL7npLpcclxuICAgKi9cclxuICBvblNob3c6IGZ1bmN0aW9uICgpIHtcclxuICAgIHRoaXMuZ2V0U3RvcmFnZURhdGEoKVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i6ZqQ6JePXHJcbiAgICovXHJcbiAgb25IaWRlOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUn+WRveWRqOacn+WHveaVsC0t55uR5ZCs6aG16Z2i5Y246L29XHJcbiAgICovXHJcbiAgb25VbmxvYWQ6IGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgfSxcclxuXHJcbiAgLyoqXHJcbiAgICog6aG16Z2i55u45YWz5LqL5Lu25aSE55CG5Ye95pWwLS3nm5HlkKznlKjmiLfkuIvmi4nliqjkvZxcclxuICAgKi9cclxuICBvblB1bGxEb3duUmVmcmVzaDogZnVuY3Rpb24gKCkge1xyXG5cclxuICB9LFxyXG5cclxuICAvKipcclxuICAgKiDpobXpnaLkuIrmi4nop6blupXkuovku7bnmoTlpITnkIblh73mlbBcclxuICAgKi9cclxuICBvblJlYWNoQm90dG9tOiBmdW5jdGlvbiAoKSB7XHJcblxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOeUqOaIt+eCueWHu+WPs+S4iuinkuWIhuS6q1xyXG4gICAqL1xyXG4gIG9uU2hhcmVBcHBNZXNzYWdlOiBmdW5jdGlvbiAoKTogYW55IHtcclxuXHJcbiAgfVxyXG59KSJdfQ==