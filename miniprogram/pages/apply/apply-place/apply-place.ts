// 申请开门权限页面2——身份的信息

import Comm from '../../../utils/comm';
import navigate from '../../../utils/navigate';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 控制按钮是否禁用
    btnStatus: false,
    // 表单内容列表
    formTpyeList: [{
      id: 1,
      name: "city",
      title: "城市",
      text: "选择城市",
    },
    {
      id: 2,
      name: "village",
      title: "小区",
      text: "选择小区",
    },
    {
      id: 3,
      name: "building",
      title: "楼栋",
      text: "选择楼栋",
    },
    {
      id: 4,
      name: "house",
      title: "门牌号",
      text: "请输入",
    },
    ],
    // 表单数据
    formData: {
      city: '',
      village: '',
      building: '',
      house: ''
    }
  },
  // 获取本地缓存中的城市、小区、楼栋数据
  getStorageData() {
    let formData = wx.getStorageSync('_placeForm');
    console.log(formData)
    this.setData({
      // formData: formData ? formData : this.data.formData
      formData: formData
    })


  },
  // 点击城市输入框，进入选择地点页面
  goChoice() {
    wx.showToast({
      title: '定位中',
      icon: 'loading',
      duration: 1000,
      complete() {
        navigate.goToPanel(
          '/pages/apply/choice-place/choice-place'
        )
      }
    })
  },
  // 点击小区输入框，进入选择小区页面
  goChoiceVillage() {
    if (wx.getStorageSync('_placeForm').city) {
      navigate.goToPanel(
        '/pages/apply/choice-place/choice-place'
      )
    }
  },
  // 点击楼栋输入框，进入选择楼栋页面
  goChoiceBuilding() {
    if (wx.getStorageSync('_placeForm').village) {
      navigate.goToPanel(
        '/pages/apply/choise-building/choise-building'
      )
    }
  },
  // 输入框实时监听
  changeInput(e: any) {
    // 获取到form的item的name值，对应到formData里的键值
    const key = Comm.getTargetValue(e, 'name');
    // 根据这个键值，生成一个新的对象
    let obj: any = {
      [key]: e.detail.value
    }
    /// 合并两个对象，将值赋予进去
    obj = Object.assign(this.data.formData, obj);
    this.setData({
      formData: obj
    })
    //通过判断表单各条数据有内容，来控制按钮禁用状态
    if (obj.city.length && obj.village.length && obj.building.length && obj.house.length) {
      this.setData({
        btnStatus: true
      })
    } else {
      this.setData({
        btnStatus: false
      })
    }
  },
  // 表单提交按钮点击
  formSubmit: function (e: any) {
    console.log(e.detail.value)
    navigate.goToPanel(
      '/pages/apply/face-input/face-input'
    )
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (_options) {
    // 修改页面标题
    wx.setNavigationBarTitle({
      title: '申请开门权限'
    })
    this.getStorageData()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getStorageData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (): any {

  }
})