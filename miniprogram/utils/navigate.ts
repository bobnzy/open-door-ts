/**
 * 页面跳转封装
 */

// 将params转换为get方式的路由
const doWithParams = (params: any) => {
  let _params = "";
  if (Object.keys(params).length > 0) {
    for (const key of Object.keys(params)) {
      const _data = params[key];
      _params += `${key}=${_data}&`;
    }
  }
  return _params;
}


export default {
  /**
   * 带参跳转页面，会保留后退按钮
   * @param url 
   * @param params
   * @param callBack 
   */
  goToPanel(url: string, params = {}, callBack?: any) {
    wx.navigateTo({
      url: `${url}?${doWithParams(params)}`,
      success: callBack,
      fail: callBack,
    })
  },

  /**
   * 带参直接跳转，不会保留后退按钮
   * @param url 
   * @param params 
   * @param callBack 
   */
  goToPanelDirect(url: string, params = {}, callBack?: any) {
    wx.reLaunch({
      url: `${url}?${doWithParams(params)}`,
      success: callBack,
      fail: callBack,
    })
  }

}