/**
 * 此处主要写一些通用的方法类来做处理
 */

export default {

  /**
   * 获取某个组件的自定义属性值
   */
  getTargetValue(e: any, key: string){
    if(e.currentTarget.dataset.hasOwnProperty(key)){
      return e.currentTarget.dataset[key];
    }
    return undefined;
  },

}