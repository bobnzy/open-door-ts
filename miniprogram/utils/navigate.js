"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var doWithParams = function (params) {
    var _params = "";
    if (Object.keys(params).length > 0) {
        for (var _i = 0, _a = Object.keys(params); _i < _a.length; _i++) {
            var key = _a[_i];
            var _data = params[key];
            _params += key + "=" + _data + "&";
        }
    }
    return _params;
};
exports.default = {
    goToPanel: function (url, params, callBack) {
        if (params === void 0) { params = {}; }
        wx.navigateTo({
            url: url + "?" + doWithParams(params),
            success: callBack,
            fail: callBack,
        });
    },
    goToPanelDirect: function (url, params, callBack) {
        if (params === void 0) { params = {}; }
        wx.reLaunch({
            url: url + "?" + doWithParams(params),
            success: callBack,
            fail: callBack,
        });
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuYXZpZ2F0ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUtBLElBQU0sWUFBWSxHQUFHLFVBQUMsTUFBVztJQUMvQixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDakIsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDbEMsS0FBa0IsVUFBbUIsRUFBbkIsS0FBQSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFuQixjQUFtQixFQUFuQixJQUFtQixFQUFFO1lBQWxDLElBQU0sR0FBRyxTQUFBO1lBQ1osSUFBTSxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLE9BQU8sSUFBTyxHQUFHLFNBQUksS0FBSyxNQUFHLENBQUM7U0FDL0I7S0FDRjtJQUNELE9BQU8sT0FBTyxDQUFDO0FBQ2pCLENBQUMsQ0FBQTtBQUdELGtCQUFlO0lBT2IsU0FBUyxZQUFDLEdBQVcsRUFBRSxNQUFXLEVBQUUsUUFBYztRQUEzQix1QkFBQSxFQUFBLFdBQVc7UUFDaEMsRUFBRSxDQUFDLFVBQVUsQ0FBQztZQUNaLEdBQUcsRUFBSyxHQUFHLFNBQUksWUFBWSxDQUFDLE1BQU0sQ0FBRztZQUNyQyxPQUFPLEVBQUUsUUFBUTtZQUNqQixJQUFJLEVBQUUsUUFBUTtTQUNmLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFRRCxlQUFlLFlBQUMsR0FBVyxFQUFFLE1BQVcsRUFBRSxRQUFjO1FBQTNCLHVCQUFBLEVBQUEsV0FBVztRQUN0QyxFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ1YsR0FBRyxFQUFLLEdBQUcsU0FBSSxZQUFZLENBQUMsTUFBTSxDQUFHO1lBQ3JDLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLElBQUksRUFBRSxRQUFRO1NBQ2YsQ0FBQyxDQUFBO0lBQ0osQ0FBQztDQUVGLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICog6aG16Z2i6Lez6L2s5bCB6KOFXHJcbiAqL1xyXG5cclxuLy8g5bCGcGFyYW1z6L2s5o2i5Li6Z2V05pa55byP55qE6Lev55SxXHJcbmNvbnN0IGRvV2l0aFBhcmFtcyA9IChwYXJhbXM6IGFueSkgPT4ge1xyXG4gIGxldCBfcGFyYW1zID0gXCJcIjtcclxuICBpZiAoT2JqZWN0LmtleXMocGFyYW1zKS5sZW5ndGggPiAwKSB7XHJcbiAgICBmb3IgKGNvbnN0IGtleSBvZiBPYmplY3Qua2V5cyhwYXJhbXMpKSB7XHJcbiAgICAgIGNvbnN0IF9kYXRhID0gcGFyYW1zW2tleV07XHJcbiAgICAgIF9wYXJhbXMgKz0gYCR7a2V5fT0ke19kYXRhfSZgO1xyXG4gICAgfVxyXG4gIH1cclxuICByZXR1cm4gX3BhcmFtcztcclxufVxyXG5cclxuXHJcbmV4cG9ydCBkZWZhdWx0IHtcclxuICAvKipcclxuICAgKiDluKblj4Lot7PovazpobXpnaLvvIzkvJrkv53nlZnlkI7pgIDmjInpkq5cclxuICAgKiBAcGFyYW0gdXJsIFxyXG4gICAqIEBwYXJhbSBwYXJhbXNcclxuICAgKiBAcGFyYW0gY2FsbEJhY2sgXHJcbiAgICovXHJcbiAgZ29Ub1BhbmVsKHVybDogc3RyaW5nLCBwYXJhbXMgPSB7fSwgY2FsbEJhY2s/OiBhbnkpIHtcclxuICAgIHd4Lm5hdmlnYXRlVG8oe1xyXG4gICAgICB1cmw6IGAke3VybH0/JHtkb1dpdGhQYXJhbXMocGFyYW1zKX1gLFxyXG4gICAgICBzdWNjZXNzOiBjYWxsQmFjayxcclxuICAgICAgZmFpbDogY2FsbEJhY2ssXHJcbiAgICB9KVxyXG4gIH0sXHJcblxyXG4gIC8qKlxyXG4gICAqIOW4puWPguebtOaOpei3s+i9rO+8jOS4jeS8muS/neeVmeWQjumAgOaMiemSrlxyXG4gICAqIEBwYXJhbSB1cmwgXHJcbiAgICogQHBhcmFtIHBhcmFtcyBcclxuICAgKiBAcGFyYW0gY2FsbEJhY2sgXHJcbiAgICovXHJcbiAgZ29Ub1BhbmVsRGlyZWN0KHVybDogc3RyaW5nLCBwYXJhbXMgPSB7fSwgY2FsbEJhY2s/OiBhbnkpIHtcclxuICAgIHd4LnJlTGF1bmNoKHtcclxuICAgICAgdXJsOiBgJHt1cmx9PyR7ZG9XaXRoUGFyYW1zKHBhcmFtcyl9YCxcclxuICAgICAgc3VjY2VzczogY2FsbEJhY2ssXHJcbiAgICAgIGZhaWw6IGNhbGxCYWNrLFxyXG4gICAgfSlcclxuICB9XHJcblxyXG59Il19