"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("./config");
var navigate_1 = require("../utils/navigate");
var urls_ingore = [
    'session', 'createOrUpdateUserWxDetail',
    'login', 'register', 'getWeChatConfig'
];
var is_in_array = function (key) {
    var testStr = ',' + urls_ingore.join(",") + ",";
    return testStr.indexOf("," + key + ",") != -1;
};
var token_key = '_token';
var getToken = function () {
    var token = null;
    token = wx.getStorageSync(token_key);
    if (!token) {
        navigate_1.default.goToPanelDirect('/pages/index/index');
    }
    return token;
};
var needToken = function (url) {
    console.log(!is_in_array(url));
    return false;
};
var doWithResponse = function (res) {
    switch (res.statusCode) {
        case 404:
            navigate_1.default.goToPanelDirect('/pages/index/index');
            return null;
        case 401:
            navigate_1.default.goToPanelDirect('/pages/index/index');
            return null;
        case 500:
            navigate_1.default.goToPanelDirect('/pages/index/index');
            return null;
    }
    return res.data;
};
var makeHeader = function (url, contentType) {
    var _contentType = { 'content-type': contentType };
    if (!needToken(url)) {
        return _contentType;
    }
    var _authorization = { 'Authorization': "Bearer " + getToken(), };
    return __assign({}, _contentType, _authorization);
};
exports.default = {
    baseOptions: function (params, method) {
        var url = params.url, data = params.data;
        var contentType = 'application/x-www-form-urlencoded';
        contentType = params.contentType || contentType;
        return new Promise(function (resolve, reject) {
            wx.request({
                url: config_1.apiHost + url,
                dataType: 'json',
                header: makeHeader(url, contentType),
                method: method,
                data: data,
                success: function (res) {
                    var resp = doWithResponse(res);
                    if (resp) {
                        return resolve(resp);
                    }
                },
                fail: function (e) {
                    return reject(e);
                }
            });
        });
    },
    get: function (url, data) {
        var option = { url: url, data: data };
        return this.baseOptions(option, 'GET');
    },
    post: function (url, data, contentType) {
        if (contentType === void 0) { contentType = 'application/json'; }
        var params = { url: url, data: data, contentType: contentType };
        return this.baseOptions(params, 'POST');
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBpLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7QUFHQSxtQ0FBbUM7QUFDbkMsOENBQW9DO0FBdUJwQyxJQUFNLFdBQVcsR0FBRztJQUNsQixTQUFTLEVBQUUsNEJBQTRCO0lBQ3ZDLE9BQU8sRUFBRSxVQUFVLEVBQUUsaUJBQWlCO0NBQ3ZDLENBQUM7QUFPRixJQUFNLFdBQVcsR0FBRyxVQUFDLEdBQVc7SUFDOUIsSUFBSSxPQUFPLEdBQUcsR0FBRyxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQ2hELE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0FBQ2hELENBQUMsQ0FBQTtBQUtELElBQU0sU0FBUyxHQUFHLFFBQVEsQ0FBQztBQU0zQixJQUFNLFFBQVEsR0FBRztJQUNmLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQztJQUNqQixLQUFLLEdBQUcsRUFBRSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNyQyxJQUFJLENBQUMsS0FBSyxFQUFFO1FBQ1Ysa0JBQUcsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsQ0FBQztLQUMzQztJQUNELE9BQU8sS0FBSyxDQUFDO0FBQ2YsQ0FBQyxDQUFBO0FBT0QsSUFBTSxTQUFTLEdBQUcsVUFBQyxHQUFXO0lBRTVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUMvQixPQUFPLEtBQUssQ0FBQztBQUNmLENBQUMsQ0FBQTtBQUVELElBQU0sY0FBYyxHQUFHLFVBQUMsR0FBeUI7SUFDL0MsUUFBUSxHQUFHLENBQUMsVUFBVSxFQUFFO1FBRXRCLEtBQUssR0FBRztZQUNOLGtCQUFHLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLENBQUM7WUFDMUMsT0FBTyxJQUFJLENBQUM7UUFFZCxLQUFLLEdBQUc7WUFDTixrQkFBRyxDQUFDLGVBQWUsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sSUFBSSxDQUFDO1FBRWQsS0FBSyxHQUFHO1lBQ04sa0JBQUcsQ0FBQyxlQUFlLENBQUMsb0JBQW9CLENBQUMsQ0FBQztZQUMxQyxPQUFPLElBQUksQ0FBQztLQUNmO0lBQ0QsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDO0FBQ2xCLENBQUMsQ0FBQTtBQUdELElBQU0sVUFBVSxHQUFHLFVBQUMsR0FBVyxFQUFFLFdBQW1CO0lBQ2xELElBQU0sWUFBWSxHQUFHLEVBQUUsY0FBYyxFQUFFLFdBQVcsRUFBRSxDQUFDO0lBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7UUFDbkIsT0FBTyxZQUFZLENBQUM7S0FDckI7SUFDRCxJQUFNLGNBQWMsR0FBRyxFQUFFLGVBQWUsRUFBRSxZQUFVLFFBQVEsRUFBSSxHQUFHLENBQUM7SUFDcEUsb0JBQVksWUFBWSxFQUFLLGNBQWMsRUFBRztBQUNoRCxDQUFDLENBQUE7QUFFRCxrQkFBZTtJQUNiLFdBQVcsRUFBWCxVQUFZLE1BQXlELEVBQUUsTUFBc0I7UUFDckYsSUFBQSxnQkFBRyxFQUFFLGtCQUFJLENBQVc7UUFDMUIsSUFBSSxXQUFXLEdBQUcsbUNBQW1DLENBQUE7UUFDckQsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDO1FBQ2hELE9BQU8sSUFBSSxPQUFPLENBQUMsVUFBQyxPQUFPLEVBQUUsTUFBTTtZQUNqQyxFQUFFLENBQUMsT0FBTyxDQUFDO2dCQUNULEdBQUcsRUFBRSxnQkFBTyxHQUFHLEdBQUc7Z0JBQ2xCLFFBQVEsRUFBRSxNQUFNO2dCQUNoQixNQUFNLEVBQUUsVUFBVSxDQUFDLEdBQUcsRUFBRSxXQUFXLENBQUM7Z0JBQ3BDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLElBQUksRUFBRSxJQUFJO2dCQUNWLE9BQU8sRUFBRSxVQUFVLEdBQXlCO29CQUMxQyxJQUFNLElBQUksR0FBdUIsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNyRCxJQUFJLElBQUksRUFBRTt3QkFDUixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDdEI7Z0JBQ0gsQ0FBQztnQkFDRCxJQUFJLEVBQUUsVUFBVSxDQUFDO29CQUNmLE9BQU8sTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUNsQixDQUFDO2FBQ0YsQ0FBQyxDQUFBO1FBQ0osQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBQ0QsR0FBRyxFQUFILFVBQUksR0FBVyxFQUFFLElBQVM7UUFDeEIsSUFBSSxNQUFNLEdBQUcsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFBO1FBQzFCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELElBQUksRUFBSixVQUFLLEdBQVcsRUFBRSxJQUFTLEVBQUUsV0FBd0M7UUFBeEMsNEJBQUEsRUFBQSxnQ0FBd0M7UUFDbkUsSUFBSSxNQUFNLEdBQUcsRUFBRSxHQUFHLEtBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxXQUFXLGFBQUEsRUFBRSxDQUFBO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7SUFDekMsQ0FBQztDQUNGLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcclxuICog6L+Z6YeM5Li76KaB5piv5o+Q5L6bQVBJ6K+35rGC5pyN5YqhXHJcbiAqL1xyXG5pbXBvcnQgeyBhcGlIb3N0IH0gZnJvbSAnLi9jb25maWcnO1xyXG5pbXBvcnQgTmF2IGZyb20gJy4uL3V0aWxzL25hdmlnYXRlJztcclxuXHJcbi8qKlxyXG4gKiDov5Tlm57liLDpobXpnaLnmoTmlbDmja5cclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgSHR0cFJlc3BvbnNlVG9QYWdlIHtcclxuICBjb2RlOiBudW1iZXI7XHJcbiAgaXRlbXM6IGFueTtcclxuICBtZXNzYWdlOiBzdHJpbmc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDov5Tlm57liLDmnI3liqHnmoTmlbDmja7nu5PmnoRcclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgSHR0cFJlc3Bvc2VUb1NlcnZpY2Uge1xyXG4gIGRhdGE6IGFueSB8IEh0dHBSZXNwb25zZVRvUGFnZTtcclxuICBlcnJNc2c6IHN0cmluZztcclxuICBzdGF0dXNDb2RlOiBudW1iZXI7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDnlaXov4d0b2tlbumqjOivgeeahOivt+axguWcsOWdgFxyXG4gKi9cclxuY29uc3QgdXJsc19pbmdvcmUgPSBbXHJcbiAgJ3Nlc3Npb24nLCAnY3JlYXRlT3JVcGRhdGVVc2VyV3hEZXRhaWwnLFxyXG4gICdsb2dpbicsICdyZWdpc3RlcicsICdnZXRXZUNoYXRDb25maWcnXHJcbl07XHJcblxyXG4vKipcclxuICog5Yik5pat5b2T5YmN6K+35rGC55qE5Zyw5Z2A5piv5ZCm5Zyo55Wl6L+HdG9rZW7pqozor4HnmoTor7fmsYLlnLDlnYDph4xcclxuICogQHBhcmFtIGtleVxyXG4gKiBAcmV0dXJuIGJvb2xlYW5cclxuICovXHJcbmNvbnN0IGlzX2luX2FycmF5ID0gKGtleTogc3RyaW5nKSA9PiB7XHJcbiAgdmFyIHRlc3RTdHIgPSAnLCcgKyB1cmxzX2luZ29yZS5qb2luKFwiLFwiKSArIFwiLFwiO1xyXG4gIHJldHVybiB0ZXN0U3RyLmluZGV4T2YoXCIsXCIgKyBrZXkgKyBcIixcIikgIT0gLTE7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiDmnKzlnLDnvJPlrZh0b2tlbueahGtleeWAvFxyXG4gKi9cclxuY29uc3QgdG9rZW5fa2V5ID0gJ190b2tlbic7XHJcblxyXG4vKipcclxuICog6I635Y+WdG9rZW7vvIzlpoLmnpzmsqHmnIl0b2tlbu+8jOS4lOmcgOimgemqjOivgXRva2Vu77yM5YiZ6LCD5pW055m75b2V6aG1XHJcbiAqIEBwYXJhbSB1cmwgXHJcbiAqL1xyXG5jb25zdCBnZXRUb2tlbiA9ICgpID0+IHtcclxuICBsZXQgdG9rZW4gPSBudWxsO1xyXG4gIHRva2VuID0gd3guZ2V0U3RvcmFnZVN5bmModG9rZW5fa2V5KTtcclxuICBpZiAoIXRva2VuKSB7XHJcbiAgICBOYXYuZ29Ub1BhbmVsRGlyZWN0KCcvcGFnZXMvaW5kZXgvaW5kZXgnKTtcclxuICB9XHJcbiAgcmV0dXJuIHRva2VuO1xyXG59XHJcblxyXG4vKipcclxuICog5Yik5pat5piv5ZCm6ZyA6KaBdG9rZW5cclxuICogQHBhcmFtIHVybCBcclxuICogQHJldHVybnMgYm9vbGVhblxyXG4gKi9cclxuY29uc3QgbmVlZFRva2VuID0gKHVybDogc3RyaW5nLCkgPT4ge1xyXG4gIC8vIHJldHVybiAhaXNfaW5fYXJyYXkodXJsKTtcclxuICBjb25zb2xlLmxvZyghaXNfaW5fYXJyYXkodXJsKSk7XHJcbiAgcmV0dXJuIGZhbHNlO1xyXG59XHJcblxyXG5jb25zdCBkb1dpdGhSZXNwb25zZSA9IChyZXM6IEh0dHBSZXNwb3NlVG9TZXJ2aWNlKSA9PiB7XHJcbiAgc3dpdGNoIChyZXMuc3RhdHVzQ29kZSkge1xyXG4gICAgLy8g5rKh5om+5Yiw5a+55bqU55qE6K+35rGC5Zyw5Z2AXHJcbiAgICBjYXNlIDQwNDpcclxuICAgICAgTmF2LmdvVG9QYW5lbERpcmVjdCgnL3BhZ2VzL2luZGV4L2luZGV4Jyk7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgLy8g5rKh5pyJ55m75b2VXHJcbiAgICBjYXNlIDQwMTpcclxuICAgICAgTmF2LmdvVG9QYW5lbERpcmVjdCgnL3BhZ2VzL2luZGV4L2luZGV4Jyk7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgLy8g5pyN5Yqh5Zmo6ZSZ6K+vXHJcbiAgICBjYXNlIDUwMDpcclxuICAgICAgTmF2LmdvVG9QYW5lbERpcmVjdCgnL3BhZ2VzL2luZGV4L2luZGV4Jyk7XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gIH1cclxuICByZXR1cm4gcmVzLmRhdGE7XHJcbn1cclxuXHJcbi8vIOWIm+W7uuivt+axguWktOmDqOS/oeaBr1xyXG5jb25zdCBtYWtlSGVhZGVyID0gKHVybDogc3RyaW5nLCBjb250ZW50VHlwZTogc3RyaW5nKSA9PiB7XHJcbiAgY29uc3QgX2NvbnRlbnRUeXBlID0geyAnY29udGVudC10eXBlJzogY29udGVudFR5cGUgfTtcclxuICBpZiAoIW5lZWRUb2tlbih1cmwpKSB7XHJcbiAgICByZXR1cm4gX2NvbnRlbnRUeXBlO1xyXG4gIH1cclxuICBjb25zdCBfYXV0aG9yaXphdGlvbiA9IHsgJ0F1dGhvcml6YXRpb24nOiBgQmVhcmVyICR7Z2V0VG9rZW4oKX1gLCB9O1xyXG4gIHJldHVybiB7IC4uLl9jb250ZW50VHlwZSwgLi4uX2F1dGhvcml6YXRpb24gfTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQge1xyXG4gIGJhc2VPcHRpb25zKHBhcmFtczogeyB1cmw6IHN0cmluZywgZGF0YT86IGFueSwgY29udGVudFR5cGU/OiBzdHJpbmcgfSwgbWV0aG9kOiAnUE9TVCcgfCAnR0VUJyk6IFByb21pc2U8SHR0cFJlc3BvbnNlVG9QYWdlPiB7XHJcbiAgICBsZXQgeyB1cmwsIGRhdGEgfSA9IHBhcmFtc1xyXG4gICAgbGV0IGNvbnRlbnRUeXBlID0gJ2FwcGxpY2F0aW9uL3gtd3d3LWZvcm0tdXJsZW5jb2RlZCdcclxuICAgIGNvbnRlbnRUeXBlID0gcGFyYW1zLmNvbnRlbnRUeXBlIHx8IGNvbnRlbnRUeXBlO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgd3gucmVxdWVzdCh7XHJcbiAgICAgICAgdXJsOiBhcGlIb3N0ICsgdXJsLFxyXG4gICAgICAgIGRhdGFUeXBlOiAnanNvbicsXHJcbiAgICAgICAgaGVhZGVyOiBtYWtlSGVhZGVyKHVybCwgY29udGVudFR5cGUpLFxyXG4gICAgICAgIG1ldGhvZDogbWV0aG9kLFxyXG4gICAgICAgIGRhdGE6IGRhdGEsXHJcbiAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlczogSHR0cFJlc3Bvc2VUb1NlcnZpY2UpIHtcclxuICAgICAgICAgIGNvbnN0IHJlc3A6IEh0dHBSZXNwb25zZVRvUGFnZSA9IGRvV2l0aFJlc3BvbnNlKHJlcyk7XHJcbiAgICAgICAgICBpZiAocmVzcCkge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzb2x2ZShyZXNwKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIGZhaWw6IGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgICByZXR1cm4gcmVqZWN0KGUpXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgfSlcclxuICB9LFxyXG4gIGdldCh1cmw6IHN0cmluZywgZGF0YTogYW55KTogUHJvbWlzZTxIdHRwUmVzcG9uc2VUb1BhZ2U+IHtcclxuICAgIGxldCBvcHRpb24gPSB7IHVybCwgZGF0YSB9XHJcbiAgICByZXR1cm4gdGhpcy5iYXNlT3B0aW9ucyhvcHRpb24sICdHRVQnKTtcclxuICB9LFxyXG4gIHBvc3QodXJsOiBzdHJpbmcsIGRhdGE6IGFueSwgY29udGVudFR5cGU6IHN0cmluZyA9ICdhcHBsaWNhdGlvbi9qc29uJyk6IFByb21pc2U8SHR0cFJlc3BvbnNlVG9QYWdlPiB7XHJcbiAgICBsZXQgcGFyYW1zID0geyB1cmwsIGRhdGEsIGNvbnRlbnRUeXBlIH1cclxuICAgIHJldHVybiB0aGlzLmJhc2VPcHRpb25zKHBhcmFtcywgJ1BPU1QnKVxyXG4gIH1cclxufSJdfQ==